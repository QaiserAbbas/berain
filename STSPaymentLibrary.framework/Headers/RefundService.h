//  RefundService.h
//  STSPaymentLibrary
//  Created by eDesign on 5/1/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>
#import "RefundRequest.h"
#import "RefundResponse.h"

@interface RefundService : NSObject

- (RefundResponse *)process :(RefundRequest *)refundReq;

@end
