//  PaymentResponse.h
//  STSPaymentLibrary
//  Created by eDesign on 4/26/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>

@interface PaymentResponse : NSObject

@property (nonatomic,strong) NSMutableDictionary *responseDictionary;

- (NSString *)get :(NSString *)key;
- (void)setResponseDict :(NSString *)key withValue:(id)value;

+ (PaymentResponse *)sharedManager;

@end
