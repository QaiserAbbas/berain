//
//  Header.h
//  STSPaymentLibrary
//
//  Created by eDesign on 4/18/17.
//  Copyright © 2017 Key. All rights reserved.

#ifndef Header_h
#define Header_h
#endif /* Header_h */

#define noInternet @"No Internet Connection"
#define noInternetLogMessage @"You have no Internet Connection.Please Check your Internet Connection"
#define eZConnectValue @"EZconnect"
#define smartRouteValue @"smartRoute"
#define merchantAuthenticationTokenFetchURLValue @"MerchantAuthenticationTokenFetchURL"
#define execCode @"execCode"
#define execCodeZero @"0"
#define execCodeOne @"1"
#define execCodeTwo @"2"
#define execCodeThree @"3"
#define execCodeFour @"4"
#define execCodeFive @"5"
#define execCodeSix @"6"
#define authenticationTokenValue @"AuthenticationToken"
#define paymentURLValue @"PaymentURL"
#define inquiryURLValue @"InquiryURL"
#define refundURLValue @"RefundURL"
#define approveURLValue @"ApproveURL"
#define postMethodValue @"POST"
#define timeIntervalValue 50.0
#define gateWayErrorValue @"-1009"
#define timeOutErrorValue @"-1001"
#define ezConnectSuccessValue @"0000"
#define smartRouteSuccessValue @"00000"
#define byPassSecureHashValidationForResponseValue @"ByPassSecureHashValidationForResponse"
#define emptyString @""
#define nValue @"N"
#define yValue @"Y"
#define responseHashMatchKeyValue @"ResponseHashMatch"
#define matchedValue @"M"
#define notMatchedValue @"N"
#define abnormalErrorValue @"8011"
#define abnormalErrorValuePaymentMethodTwo @"00019"
#define unKnownErrorValue  @"9001"
#define twoTripleZeroTwo @"20002"
#define responseSecureHashValue @"Response.SecureHash"
#define ezConnectResponseStatus @"Response.Status"
#define ezConnectResponseStatus_1 @"Response.Status_1"
#define smartRouteResponseStatusCode @"Response.StatusCode"
#define inquiryAbnormalErrorValue @"Inquiry-1010"
#define smartRouteRefundAbnormalErrorValue @"00019"
#define ezConnectRefundAbnormalErrorValue @"5007"
#define smartRouteApproveAbnormalErrorValue @"00019"
#define hashStringFormateValue @"%@"
#define queryStringFormateValueIs @"%@=%@&"
#define appendSecureHashValueIs @"SecureHash=%@"
#define genericFileNameValueIs @"generic"
#define fileTypeValueIs @"txt"
#define parametersToEncryptNameValueIs @"ParametersToEncrypt"
#define ParametersToEncodeNameValueIs @"ParametersToEncode"
#define ParametersToEncodeNameForSecureHashValueIs @"ParametersToEncodeForSecureHash"
#define ExcludedParametersFromSecureHashValueIs @"ExcludedParametersFromSecureHash"
#define responseStatusMessageValueIs @"StatusMessage"
#define responseStatusDescriptionValueIs @"StatusDescription"
#define responseGatewayStatusDescriptionValueIs @"GatewayStatusDescription"
#define responseStatusMessage_1 @"StatusMessage_1"
#define responseOriginalStatusMessage @"OriginalStatusMessage"
#define nullValueIs @"null"
#define responseEZConnectRequestStatusValueIs @"Response.EZConnectRequestStatus"
#define responseStatus @"Response.Status"
#define kEncryptionErrorValue @"kEncryptionError"
#define responseValueIs @"Response."
#define documentReadingError @"some Error occurr while reading file from document %@";
#define serverCommunicationError @"Caught Server Communication Error ExecCode is 1";
#define timeoutError @"Caught Timeout Error ExecCode is 2"






