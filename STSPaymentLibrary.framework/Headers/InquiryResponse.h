//  InquiryResponse.h
//  STSPaymentLibrary
//  Created by eDesign on 5/1/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>

@interface InquiryResponse : NSObject

@property (nonatomic,strong) NSMutableDictionary *responseDictionary;

- (NSString *)get :(NSString *)key;

- (void)setResponseDict :(NSString *)key withValue:(id)value;

+ (InquiryResponse *)sharedManager;

@end
