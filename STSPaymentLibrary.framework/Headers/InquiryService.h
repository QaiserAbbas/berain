//  InquiryService.h
//  STSPaymentLibrary
//  Created by eDesign on 5/1/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>
#import "InquiryRequest.h"
#import "InquiryResponse.h"

@interface InquiryService : NSObject

- (InquiryResponse *)process :(InquiryRequest *)inquiryRequest;

@end
