//  PaymentService.h
//  STSPaymentLibrary
//  Created by eDesign on 4/20/17.
//  Copyright © 2017 Key. All rights reserved.


#import <Foundation/Foundation.h>
#import "PaymentRequest.h"
#import "PaymentResponse.h"

@interface PaymentService : NSObject

- (PaymentResponse *)process :(PaymentRequest *)payReq;

@end
