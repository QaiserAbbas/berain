//  Utility.h
//  STSPaymentLibrary
//  Created by eDesign on 4/18/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import "PaymentRequest.h"
#import "InquiryRequest.h"
#import "ApproveRequest.h"
#import "RefundRequest.h"
#import "Parameters.h"

@interface Utility : NSObject
{
 
}

+ (Utility *)sharedManger;

- (NSString *)getMerchantAuthenticationTokenFromURL :(NSString *)tokenURL;

- (NSData *)sha256 :(NSString *)merchantAuthenticationTokenString;

- (NSString *)base64forData :(NSData *)encryptedmerchantAuthenticationToken;

- (NSData *)doCipher:(NSData *)dataIn
                  iv:(NSData *)iv
                 key:(NSData *)symmetricKey
             context:(CCOperation)encryptOrDecrypt // kCCEncrypt or kCCDecrypt
               error:(NSError **)error;

- (NSString *)userEncriptedInfoIntoBase64EncodedString :(NSData *)userInfoData;

- (NSString *)getSecureHashForRequest:(NSDictionary *)requestDictionary :(NSString *)authenticationToken;

- (NSString *)getRequestMessage :(NSString *)hashValue RequestDictionary:(NSDictionary *)requestDictionary;

- (void)getURLfromTextFile :(NSMutableDictionary *)urlDictionary;

- (NSMutableArray *)getKeysForEncryption;

- (NSArray *)getKeysForEncode;

- (NSArray *)getKeysForSecureHashEncoding;

- (NSArray *)getExcludedKeysForSecureHash;

- (void) setLoggValue:(BOOL) log;
- (BOOL) getLoggValue;


- (NSString *)getHashStringForMatching :(NSString *)stringHash;
- (NSString *)getSecureHashForResponse:(NSDictionary *)responseDictionary :(NSString *)authenticationToken;



@end
