//  InquiryRequest.h
//  STSPaymentLibrary
//  Created by eDesign on 5/1/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>

@interface InquiryRequest : NSObject

@property (strong,nonatomic)NSString *userInterface;

@property (strong,nonatomic)NSMutableDictionary *authenticationToken;

@property (nonatomic,strong)NSMutableDictionary *keyValueDictionary;
@property (nonatomic)BOOL userValue;

+ (InquiryRequest *)sharedManager;

- (void)add :(NSString *)key :(NSString *)value;

- (void)setPaymentType :(NSString *)value;

- (void)setInquiryRequestAuthenticationToken :(NSString *)key valueForKey:(id)value;

- (void)isLogging :(BOOL)userValue;

@end
