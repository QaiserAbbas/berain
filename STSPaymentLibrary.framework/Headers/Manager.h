//  Manager.h
//  STSPaymentLibrary
//  Created by eDesign on 4/29/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>

@interface Manager : NSObject<NSURLSessionDelegate>

+ (NSString *)requestToServer :(NSMutableURLRequest *)request;

                            
@end
