//
//  InternetConnectivityCheck.h
//  STSPaymentLibrary
//
//  Created by Rafay on 4/17/17.
//  Copyright © 2017 Key. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface InternetConnectivityCheck : NSObject

+ (NSString *)checkInternetConnection;

@end
