//  PaymentRequest.h
//  STSPaymentLibrary
//  Created by eDesign on 4/19/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>

@interface PaymentRequest : NSObject

@property (strong,nonatomic)NSString *userInterface;

@property (strong,nonatomic)NSMutableDictionary *authenticationToken;

@property (nonatomic,strong)NSMutableDictionary *keyValueDictionary;

+ (PaymentRequest *)sharedManager;

- (void)add :(NSString *)key :(NSString *)value;

- (void)setPaymentType :(NSString *)value;

- (void)setPaymentAuthenticationToken :(NSString *)key valueForKey:(id)value;

- (void)isLogging :(BOOL)userValue;

@end
