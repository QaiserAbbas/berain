//  ApproveService.h
//  STSPaymentLibrary
//  Created by eDesign on 5/1/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>
#import "ApproveResponse.h"
#import "ApproveRequest.h"

@interface ApproveService : NSObject

- (ApproveResponse *)process :(ApproveRequest *)approveReq;

@end
