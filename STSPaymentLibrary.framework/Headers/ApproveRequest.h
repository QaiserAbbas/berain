//  ApproveRequest.h
//  STSPaymentLibrary
//  Created by eDesign on 5/1/17.
//  Copyright © 2017 Key. All rights reserved.

#import <Foundation/Foundation.h>

@interface ApproveRequest : NSObject

@property (strong,nonatomic)NSString *userInterface;

@property (strong,nonatomic)NSMutableDictionary *authenticationToken;

@property (nonatomic,strong)NSMutableDictionary *keyValueDictionary;

+ (ApproveRequest *)sharedManager;

- (void)add :(NSString *)key :(NSString *)value;

- (void)setPaymentType :(NSString *)value;

- (void)setApproveRequestAuthenticationToken :(NSString *)key valueForKey:(id)value;

- (void)isLogging :(BOOL)userValue;

@end
