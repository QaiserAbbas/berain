//
//  UIImage+Additions.h
//  VisitorBook
//
//  Created by Zykov on 09.07.15.
//  Copyright (c) 2015 Zykov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

+ (UIImage *)imageWithView:(UIView *)view;

@end
