//
//  UIImage+ImageWithView.m
//  DrOTC
//
//  Created by Alexander Anisimov on 1/20/14.
//  Copyright (c) 2014 Magora Systems. All rights reserved.
//

#import "UIImage+ImageWithView.h"

@implementation UIImage (ImageWithView)

+ (UIImage *)imageWithView:(UIView *)view {
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

@end
