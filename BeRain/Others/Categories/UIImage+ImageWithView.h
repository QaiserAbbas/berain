//
//  UIImage+ImageWithView.h
//  DrOTC
//
//  Created by Alexander Anisimov on 1/20/14.
//  Copyright (c) 2014 Magora Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageWithView)

+ (UIImage *)imageWithView:(UIView *)view;

@end
