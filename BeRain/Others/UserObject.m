//
//  UserObject.m
//  App
//
//  Created by Qaiser Abbas on 10/08/2015.
//  Copyright (c) 2015 Bir Al Sabia. All rights reserved.
//

#import "UserObject.h"

@implementation UserObject
@synthesize first_name;
@synthesize user_id;
@synthesize email;
@synthesize last_name;
@synthesize user_type;
@synthesize phone_number;
@synthesize profile_pic_path;

-(id) init
{
    self = [super init];
    
    if( self )
    {
    }
    
    return self;
}


- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.email forKey:App_User_Email];
    [aCoder encodeObject:self.user_type forKey:App_User_UserType];
    [aCoder encodeObject:self.phone_number forKey:App_User_PhoneNumber];
    [aCoder encodeObject:self.first_name forKey:App_User_FirstName];
    [aCoder encodeObject:self.last_name forKey:App_User_LastName];
    [aCoder encodeObject:self.user_id forKey:App_User_ID];
    [aCoder encodeObject:self.profile_pic_path forKey:App_User_Profile_Pic_Path];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super init]){
        self.user_id = [aDecoder decodeObjectForKey:App_User_ID];
        self.email = [aDecoder decodeObjectForKey:App_User_Email];
        self.user_type = [aDecoder decodeObjectForKey:App_User_UserType];
        
        self.phone_number = [aDecoder decodeObjectForKey:App_User_PhoneNumber];
        self.first_name = [aDecoder decodeObjectForKey:App_User_FirstName];
        self.last_name = [aDecoder decodeObjectForKey:App_User_LastName];
        self.profile_pic_path = [aDecoder decodeObjectForKey:App_User_Profile_Pic_Path];
    }
    return self;
}


@end
