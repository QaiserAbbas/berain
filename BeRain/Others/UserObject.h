//
//  UserObject.h
//  App
//
//  Created by Qaiser Abbas on 10/08/2015.
//  Copyright (c) 2015 Bir Al Sabia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppConstants.h"

@interface UserObject : NSObject
{
    NSString *first_name;
    NSString *email;
    NSString *user_id;
    NSString *last_name;
    NSString *phone_number;
    NSString *user_type;
    NSString *profile_pic_path;
}

@property (nonatomic, retain)       NSString*   first_name;
@property (nonatomic, retain)       NSString*   last_name;
@property (nonatomic, retain)       NSString*   phone_number;
@property (nonatomic, retain)       NSString*   email;
@property (nonatomic, retain)       NSString*   user_id;
@property (nonatomic, retain)       NSString*   user_type;
@property (nonatomic, retain)       NSString*   profile_pic_path;

@end


//activeStatus = 1;
//"birth_date" = "2017-12-05 13:43:00";
//"city_id" = 15;
//"client_sub_type" = 7;
//"client_type" = 1;
//"company_name" = "";
//email = "smtayyeb@gmail.com";
//"email_active" = "";
//endPointArn = "";
//"frist_name" = Berain;
//gender = "";
//"identity_num" = 12;
//image = "";
//"last_name" = Customer;
//"location_code" = "";
//"login_id" = "";
//mobile = 966590775565;
//"mobile_active" = "";
//"mobile_active_code" = "";
//notification = 1;
//pass = 123456;
//phone = "";
//random = 12;
//"register_date" = "2017-09-29 16:34:03";
//"sap_userId" = 12;
//"sms_rand" = 4349193642655924735;
//toicArn = "";
//"unique_id" = 12;
//"user_id" = 12;
