//
//  AppConstants.h
//  IWasHere
//
//  Created by Qaiser Abbas on 21/10/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h


#endif /* AppConstants_h */


#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

/************************************/
/*            APP USER          */
/************************************/

#define     App_User_ID                                 @"id"
#define     App_User_Email                              @"email"

#define     App_User_PhoneNumber                        @"phone_number"
#define     App_User_UserType                           @"user_type"
#define     App_User_FirstName                          @"first_name"
#define     App_User_LastName                           @"last_name"
#define     App_User_Profile_Pic_Path                   @"profile_url"



/************************************/
/*            Webservices           */
//**********************************

//#define     App_BaseUrl                     @"https://berain.com.sa/oms/api/index.php/"
#define     App_BaseUrl                     @"https://berain.com.sa/stagingoms2/api/index.php/"

#define Webservice_AllCities                @"get_all_cities"
#define Webservice_GetChannels              @"get_client_type_detail"
#define Webservice_GetSubChannels           @"get_sub_client_type"
#define Webservice_SignUp                   @"signup/en"
#define Webservice_SignIn                   @"checkuser"
#define Webservice_AllProducts              @"get_dishes"
#define Webservice_AllTips                  @"get_tips"
#define Webservice_AllOrders                @"getorders"
#define Webservice_OrderDetail              @"getorder_detail"
#define Webservice_GetCities                @"get_cities"
#define Webservice_GetArea                  @"getarea"
#define Webservice_SaveArea                 @"newaddress"
#define Webservice_ForgotPassword           @"checkuser"
//#define Webservice_DeleteAddress            @"tbl_address"
#define Webservice_Settings                 @"companysetting"
#define Webservice_AddOrder                 @"addordersnew"
#define Webservice_AddOrderDirectly         @"addorders"
#define Webservice_UpdateProfile            @"updateprofile"
#define Webservice_DeleteAddress            @"delete_address"
#define Webservice_TokenReferesh            @"refreshToken"

/************************************/
/*            Date Formates         */
/************************************/

#define     AppDateFormat                @"EEEE, MMMM d"
#define     AppUIDateFormat              @"MM/dd/yy"
#define     AppSignUpDateFormat          @"YYYY-MM-dd"


#define     App_firstTime_Use            @"FirstTime_Use"
#define     App_firstTime_Add            @"FirstTime_Add"


