//
//  SingletonClass.m
//  FindIt
//
//  Created by JunaidAkram on 12/27/13.
//  Copyright (c) 2013 ABC. All rights reserved.
//

#import "SingletonClass.h"
#import "CWLSynthesizeSingleton.h"
@implementation SingletonClass
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(SingletonClass);

+(id)sharedManager
{
    static SingletonClass *mySingletonClass=nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
    
        mySingletonClass=[[self alloc]init];
        });
    
    return mySingletonClass;
    
}

-(id)init
{
    if (self=[super init])
    {
        _settingsDictionary=[NSMutableDictionary dictionary];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}


@end
