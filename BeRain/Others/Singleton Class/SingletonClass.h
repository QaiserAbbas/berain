//
//  SingletonClass.h
//  FindIt
//
//  Created by JunaidAkram on 12/27/13.
//  Copyright (c) 2013 ABC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"

@interface SingletonClass : NSObject
CWL_DECLARE_SINGLETON_FOR_CLASS(SingletonClass)


@property (nonatomic, strong) NSMutableDictionary *settingsDictionary;

+(id)sharedManager;

@end
