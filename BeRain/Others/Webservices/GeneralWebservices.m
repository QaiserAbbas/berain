//
//  GeneralWebservices.m
//  IWasHere
//
//  Created by Qaiser Abbas on 18/11/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#import "GeneralWebservices.h"
#import "SettingsClass.h"


@implementation GeneralWebservices

+(void) webserviceMainSplashCall:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString * auth_token = [SettingsClass auth_key];
    if (auth_token != nil)
    {
        [manager.requestSerializer setValue:auth_token forHTTPHeaderField:@"Authorizations"];
    }
    [manager.requestSerializer setValue:@"charset=UTF-8" forHTTPHeaderField:@"Content-Type"];


    [manager POST:[NSString stringWithFormat:@"%@%@?",App_BaseUrl,webserviceName] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString* myString;
        myString = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];

        NSLog(@"%@",[responseObject class]);
        NSDictionary * emp = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"here we got it  %@",emp);

        callbackBlock( emp, nil );
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callbackBlock( nil, nil );
    }];
}

+(void) webserviceCall:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString * auth_token = [SettingsClass auth_key];
    if (auth_token != nil)
    {
        [manager.requestSerializer setValue:auth_token forHTTPHeaderField:@"Authorizations"];
    }
    [manager.requestSerializer setValue:@"charset=UTF-8" forHTTPHeaderField:@"Content-Type"];

    [manager POST:[NSString stringWithFormat:@"http://dalilalarais.com/arais_dev/upload_images/saveOfferImageDetail"] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSDictionary * emp = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                    NSLog(@"here we got it  %@",emp);
        
        callbackBlock( emp, nil );
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callbackBlock( nil, nil );
    }];
}


+(void) webserviceCallWithData:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName dataToPost:(NSData*)dataToPost imageName:(NSString*)imageName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
{
//    if ([[AFNetworkReachabilityManager sharedManager] isReachable])
//    {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString * auth_token = [SettingsClass auth_key];
    if (auth_token != nil)
    {
        [manager.requestSerializer setValue:auth_token forHTTPHeaderField:@"Authorizations"];
    }
    [manager.requestSerializer setValue:@"charset=UTF-8" forHTTPHeaderField:@"Content-Type"];

        [manager POST:[NSString stringWithFormat:@"%@%@?",App_BaseUrl,webserviceName] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            if (dataToPost)
            {
                [formData appendPartWithFileData:dataToPost name:@"image_file" fileName:imageName mimeType:@"image/jpeg"];
            }

        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary * emp = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSLog(@"here we got it  %@",emp);

            callbackBlock( emp, nil );
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            callbackBlock( nil, nil );
        }];
        
//    }
//    else
//    {
//        
//    }
}

+(void) webserviceMainJsonResponse:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
{
    NSError *error;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:parameters
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@?",App_BaseUrl,webserviceName] parameters:nil error:nil];
    
    req.timeoutInterval = 30;
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSString * auth_token = [SettingsClass auth_key];
    if (auth_token != nil)
    {
        [req setValue:auth_token forHTTPHeaderField:@"Authorizations"];
    }
    [req setValue:@"charset=UTF-8" forHTTPHeaderField:@"Content-Type"];

    [req setHTTPBody:dataFromDict];
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {

            NSString* myString;
            myString = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];

            NSDictionary * emp = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            callbackBlock( emp, nil );

            NSLog(@"%@", responseObject);
        } else {
            callbackBlock( nil, nil );
        }
    }] resume];

}
+(void) webserviceMainSplashCallGET:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString * auth_token = [SettingsClass auth_key];
    if (auth_token != nil)
    {
        [manager.requestSerializer setValue:auth_token forHTTPHeaderField:@"Authorizations"];
    }
    [manager.requestSerializer setValue:@"charset=UTF-8" forHTTPHeaderField:@"Content-Type"];

    [manager GET:[NSString stringWithFormat:@"%@%@?",App_BaseUrl,webserviceName] parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",[responseObject class]);
        NSDictionary * emp = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"here we got it  %@",emp);
        
        callbackBlock( emp, nil );
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callbackBlock( nil, nil );

    }];
}

+(void) webserviceMainDeleteCall:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString * auth_token = [SettingsClass auth_key];
    if (auth_token != nil)
    {
        [manager.requestSerializer setValue:auth_token forHTTPHeaderField:@"Authorizations"];
    }
    [manager.requestSerializer setValue:@"charset=UTF-8" forHTTPHeaderField:@"Content-Type"];

    [manager DELETE:[NSString stringWithFormat:@"%@%@?",App_BaseUrl,webserviceName] parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString* myString;
        myString = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];
        
        NSLog(@"%@",[responseObject class]);
        NSDictionary * emp = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"here we got it  %@",emp);
        
        callbackBlock( emp, nil );

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callbackBlock( nil, nil );

    }];
}

@end
