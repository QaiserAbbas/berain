//
//  GeneralWebservices.h
//  IWasHere
//
//  Created by Qaiser Abbas on 18/11/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import "AppConstants.h"

@interface GeneralWebservices : NSObject

+(void) webserviceCall:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
+(void) webserviceCallWithData:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName dataToPost:(NSData*)dataToPost imageName:(NSString*)imageName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;

+(void) webserviceMainSplashCall:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
+(void) webserviceMainJsonResponse:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;
+(void) webserviceMainSplashCallGET:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;

+(void) webserviceMainDeleteCall:(NSMutableDictionary *)parameters webserviceName:(NSString*)webserviceName OnCompletion:(void(^)(id returnDict, NSError* error))callbackBlock;

@end
