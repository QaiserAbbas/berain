//
//  Address+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/22/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Address+CoreDataProperties.h"

@implementation Address (CoreDataProperties)

+ (NSFetchRequest<Address *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Address"];
}

@dynamic add_area;
@dynamic add_block;
@dynamic add_city;
@dynamic add_country;
@dynamic add_detail;
@dynamic add_floor;
@dynamic add_google_address;
@dynamic add_id;
@dynamic add_latitude;
@dynamic add_longitude;
@dynamic add_name;
@dynamic add_street_name;
@dynamic sap_id;
@dynamic user_id;
@dynamic defaultAddress;

@end
