//
//  Area+CoreDataProperties.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Area+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Area (CoreDataProperties)

+ (NSFetchRequest<Area *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *area_active;
@property (nullable, nonatomic, copy) NSString *area_code;
@property (nullable, nonatomic, copy) NSString *area_city_id;
@property (nullable, nonatomic, copy) NSString *area_country_id;
@property (nullable, nonatomic, copy) NSString *area_id;
@property (nullable, nonatomic, copy) NSString *area_title_ar;
@property (nullable, nonatomic, copy) NSString *area_title_en;

@end

NS_ASSUME_NONNULL_END
