//
//  Tips+CoreDataProperties.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Tips+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Tips (CoreDataProperties)

+ (NSFetchRequest<Tips *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *id_row;
@property (nullable, nonatomic, copy) NSString *active;
@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *detail_ar;
@property (nullable, nonatomic, copy) NSString *detail_en;

@end

NS_ASSUME_NONNULL_END
