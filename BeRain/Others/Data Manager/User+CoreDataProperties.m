//
//  User+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/4/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"User"];
}

@dynamic city_id;
@dynamic client_sub_type;
@dynamic client_type;
@dynamic email;
@dynamic frist_name;
@dynamic last_name;
@dynamic mobile;
@dynamic email_active;
@dynamic mobile_active;
@dynamic mobile_active_code;
@dynamic notification;
@dynamic register_date;
@dynamic sap_userId;
@dynamic sms_rand;
@dynamic unique_id;
@dynamic user_id;

@end
