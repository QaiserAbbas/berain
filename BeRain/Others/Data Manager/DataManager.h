//
//  DataManager.h
//  VisitorBook
//
//  Created by Zykov on 02.12.14.
//  Copyright (c) 2014 Zykov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Tips+CoreDataClass.h"
#import "Cart+CoreDataClass.h"
#import "User+CoreDataClass.h"
#import "Address+CoreDataClass.h"
#import "Area+CoreDataClass.h"
#import "City+CoreDataClass.h"
#import "Channel+CoreDataClass.h"
#import "SubChannel+CoreDataClass.h"



typedef NS_ENUM(NSUInteger, DataManagerPathType) {
    DataManagerPathURL,
    DataManagerPathBody
};


@interface DataManager : NSObject

+ (DataManager *) sharedManager;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSString *)applicationDocumentsDirectory;
- (NSURL *)applicationURLDocumentsDirectory;
- (BOOL)dropDataStore;

#pragma mark - Tipe
- (void)saveTips:(NSMutableArray*)tipsArray;
-(Tips *) createTips;
-(NSArray *) arrayTips;
-(Tips*)TipsById:(NSString*)id_row;


#pragma mark - Cart
- (void)saveCart:(NSMutableDictionary*)cartDict quantity:(NSString*)quantity;
- (void)saveCartReOrder:(NSMutableArray*)order;

- (void)deleteCart:(NSMutableDictionary*)cartDict;
- (void)updateCartByCartObject:(Cart*)object quantity:(NSString*)quantity;
- (void)deleteCartbyCartOject:(Cart*)object;
- (void)deleteCartOjectCompletely:(Cart*)object;
- (void)deleteAllCart;
-(Cart *) createCart;
-(NSArray *) arrayCart;
-(Cart*)cartById:(NSString*)dish_id;

#pragma mark - Cart
- (void)saveUser:(NSMutableDictionary*)userDict;
-(User *) getUser;
- (void)removeUser;


#pragma mark - Address
- (void)saveAddress:(NSMutableArray*)AddressArray;
-(Address *) createAddress;
-(NSArray *) arrayAddress :(NSString*)user_id;
-(Address*)addressById:(NSString*)add_id;
- (void)setDefaultAddress:(NSString*)add_id user_id:(NSString*)user_id;
-(void)makeDefaultAddress:(NSString*)user_id;


-(NSMutableDictionary *)formattedDictionary;


#pragma mark - Area
- (void)saveArea:(NSMutableArray*)areaArray;
-(Area *) createArea;
-(NSArray *) arrayarea :(NSString*)city_id;
-(Area*)areaById:(NSString*)area_id;

#pragma mark - City
- (void)saveCity:(NSMutableArray*)cityArray;
-(City *) createCity;
-(NSArray *) arrayCity;
-(City*)cityById:(NSString*)city_id;

#pragma mark - Channel
- (void)saveChannel:(NSMutableArray*)channelArray;
-(Channel *) createChannel;
-(NSArray *) arrayChannel;
-(Channel*)channelById:(NSString*)channel_id;

#pragma mark - SubChannel
- (void)saveSubChannel:(NSMutableArray*)subChannelArray;
-(SubChannel *) createSubChannel;
-(NSArray *) arraySubChannel;
-(SubChannel*)subChannelById:(NSString*)subChannel_id;

@end
