//
//  SubChannel+CoreDataProperties.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "SubChannel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SubChannel (CoreDataProperties)

+ (NSFetchRequest<SubChannel *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *active;
@property (nullable, nonatomic, copy) NSString *subChannel_id;
@property (nullable, nonatomic, copy) NSString *parent_id;
@property (nullable, nonatomic, copy) NSString *sap_id;
@property (nullable, nonatomic, copy) NSString *title_ar;
@property (nullable, nonatomic, copy) NSString *title_en;
@property (nullable, nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
