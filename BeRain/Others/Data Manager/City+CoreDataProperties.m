//
//  City+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "City+CoreDataProperties.h"

@implementation City (CoreDataProperties)

+ (NSFetchRequest<City *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"City"];
}

@dynamic city_active;
@dynamic city_country_id;
@dynamic city_id;
@dynamic city_title_ar;
@dynamic city_title_en;
@dynamic plant;

@end
