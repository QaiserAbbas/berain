//
//  Cart+CoreDataProperties.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Cart+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Cart (CoreDataProperties)

+ (NSFetchRequest<Cart *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *dish_id;
@property (nullable, nonatomic, copy) NSString *dish_kitchen_id;
@property (nullable, nonatomic, copy) NSString *dish_title_ar;
@property (nullable, nonatomic, copy) NSString *dish_title_en;
@property (nullable, nonatomic, copy) NSString *dish_image;
@property (nullable, nonatomic, copy) NSString *dish_price;
@property (nullable, nonatomic, copy) NSString *quantity;

@end

NS_ASSUME_NONNULL_END
