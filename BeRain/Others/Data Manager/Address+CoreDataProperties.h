//
//  Address+CoreDataProperties.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/22/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Address+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Address (CoreDataProperties)

+ (NSFetchRequest<Address *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *add_area;
@property (nullable, nonatomic, copy) NSString *add_block;
@property (nullable, nonatomic, copy) NSString *add_city;
@property (nullable, nonatomic, copy) NSString *add_country;
@property (nullable, nonatomic, copy) NSString *add_detail;
@property (nullable, nonatomic, copy) NSString *add_floor;
@property (nullable, nonatomic, copy) NSString *add_google_address;
@property (nullable, nonatomic, copy) NSString *add_id;
@property (nullable, nonatomic, copy) NSString *add_latitude;
@property (nullable, nonatomic, copy) NSString *add_longitude;
@property (nullable, nonatomic, copy) NSString *add_name;
@property (nullable, nonatomic, copy) NSString *add_street_name;
@property (nullable, nonatomic, copy) NSString *sap_id;
@property (nullable, nonatomic, copy) NSString *user_id;
@property (nonatomic) BOOL defaultAddress;

@end

NS_ASSUME_NONNULL_END
