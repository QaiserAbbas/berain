//
//  City+CoreDataClass.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface City : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "City+CoreDataProperties.h"
