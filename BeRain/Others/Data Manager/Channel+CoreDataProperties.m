//
//  Channel+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Channel+CoreDataProperties.h"

@implementation Channel (CoreDataProperties)

+ (NSFetchRequest<Channel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Channel"];
}

@dynamic active;
@dynamic channel_id;
@dynamic parent_id;
@dynamic sap_id;
@dynamic title_ar;
@dynamic title_en;
@dynamic type;

@end
