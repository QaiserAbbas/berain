//
//  SubChannel+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "SubChannel+CoreDataProperties.h"

@implementation SubChannel (CoreDataProperties)

+ (NSFetchRequest<SubChannel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SubChannel"];
}

@dynamic active;
@dynamic subChannel_id;
@dynamic parent_id;
@dynamic sap_id;
@dynamic title_ar;
@dynamic title_en;
@dynamic type;

@end
