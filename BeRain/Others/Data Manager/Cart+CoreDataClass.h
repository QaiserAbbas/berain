//
//  Cart+CoreDataClass.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Cart : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Cart+CoreDataProperties.h"
