//
//  User+CoreDataProperties.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/4/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "User+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *city_id;
@property (nullable, nonatomic, copy) NSString *client_sub_type;
@property (nullable, nonatomic, copy) NSString *client_type;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *frist_name;
@property (nullable, nonatomic, copy) NSString *last_name;
@property (nullable, nonatomic, copy) NSString *mobile;
@property (nullable, nonatomic, copy) NSString *email_active;
@property (nullable, nonatomic, copy) NSString *mobile_active;
@property (nullable, nonatomic, copy) NSString *mobile_active_code;
@property (nullable, nonatomic, copy) NSString *notification;
@property (nullable, nonatomic, copy) NSString *register_date;
@property (nullable, nonatomic, copy) NSString *sap_userId;
@property (nullable, nonatomic, copy) NSString *sms_rand;
@property (nullable, nonatomic, copy) NSString *unique_id;
@property (nullable, nonatomic, copy) NSString *user_id;

@end

NS_ASSUME_NONNULL_END
