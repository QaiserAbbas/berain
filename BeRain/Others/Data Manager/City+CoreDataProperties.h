//
//  City+CoreDataProperties.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "City+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface City (CoreDataProperties)

+ (NSFetchRequest<City *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *city_active;
@property (nullable, nonatomic, copy) NSString *city_country_id;
@property (nullable, nonatomic, copy) NSString *city_id;
@property (nullable, nonatomic, copy) NSString *city_title_ar;
@property (nullable, nonatomic, copy) NSString *city_title_en;
@property (nullable, nonatomic, copy) NSString *plant;

@end

NS_ASSUME_NONNULL_END
