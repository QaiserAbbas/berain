//
//  Cart+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Cart+CoreDataProperties.h"

@implementation Cart (CoreDataProperties)

+ (NSFetchRequest<Cart *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Cart"];
}

@dynamic dish_id;
@dynamic dish_kitchen_id;
@dynamic dish_title_ar;
@dynamic dish_title_en;
@dynamic dish_image;
@dynamic dish_price;
@dynamic quantity;

@end
