//
//  DataManager.m
//  VisitorBook
//
//  Created by Zykov on 02.12.14.
//  Copyright (c) 2014 Zykov. All rights reserved.
//

#import "DataManager.h"
#import "SingletonClass.h"
#import "SettingsClass.h"

@interface DataManager ()

@end


@implementation DataManager


+ (DataManager *) sharedManager
{
    static DataManager * manager;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}


#pragma mark - Tips
- (void)saveTips:(NSMutableArray*)tipsArray;
{
    for (NSMutableDictionary *dataDict in tipsArray)
    {
        Tips *object =[[DataManager sharedManager] TipsById:dataDict[@"id"]]; // update
        
        if (!object)
        {
            object =[[DataManager sharedManager] createTips]; // insert
        }
        
        object.id_row       = [NSString stringWithFormat:@"%@",dataDict[@"id"]];
        object.active       = [NSString stringWithFormat:@"%@",dataDict[@"active"]];
        object.date         = [NSString stringWithFormat:@"%@",dataDict[@"date"]];
        NSString *trimmedStringAR = [dataDict[@"detail_ar"] stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        NSString *trimmedStringEn = [dataDict[@"detail_en"] stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];

        object.detail_ar    = [NSString stringWithFormat:@"%@",trimmedStringAR];
        object.detail_en    = [NSString stringWithFormat:@"%@",trimmedStringEn];
    }
    [[DataManager sharedManager] saveContext];
}

-(Tips *) createTips
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Tips" inManagedObjectContext:self.managedObjectContext];
    Tips * tip = [[Tips alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return tip;
}

-(NSArray *) arrayTips
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Tips" inManagedObjectContext:self.managedObjectContext]];
    
    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    return result;
}

-(Tips*)TipsById:(NSString*)id_row
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Tips" inManagedObjectContext:self.managedObjectContext]];

    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.id_row == %@", id_row]];


    NSError * error = nil;
    NSArray * result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if (error!=nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    
    NSMutableArray *arrayOfTips=[NSMutableArray arrayWithArray:result];

    if (result.count>1)
    {
        
        for (int i=0; i<result.count-1; i++)
        {
            Tips *employee  =result[i];
            
            [self.managedObjectContext deleteObject:employee];
            [arrayOfTips removeObject:employee];
        }
        
    }

    if (arrayOfTips)
    {
        return [arrayOfTips firstObject];
    }
    return nil;

}

#pragma mark - Address
- (void)saveAddress:(NSMutableArray*)AddressArray;
{
    for (int i=0; i<AddressArray.count; ++i)
    {
        NSMutableDictionary *dataDict =AddressArray[i];
        Address *object =[[DataManager sharedManager] addressById:dataDict[@"add_id"]]; // update

        if (!object)
        {
            object =[[DataManager sharedManager] createAddress]; // insert
            object.defaultAddress=NO;
        }
//        else
//        {
//            if (!object.defaultAddress)
//            {
//                if (i==0)
//                {
//                    object.defaultAddress=YES;
//                }
//            }  
//        }
        object.add_area       = [NSString stringWithFormat:@"%@",dataDict[@"add_area"]];
        object.add_block       = [NSString stringWithFormat:@"%@",dataDict[@"add_block"]];
        object.add_city         = [NSString stringWithFormat:@"%@",dataDict[@"add_city"]];
        object.add_country    = [NSString stringWithFormat:@"%@",dataDict[@"add_country"]];
        object.add_detail    = [NSString stringWithFormat:@"%@",dataDict[@"add_detail"]];
        object.add_floor    = [NSString stringWithFormat:@"%@",dataDict[@"add_floor"]];
        object.add_google_address    = [NSString stringWithFormat:@"%@",dataDict[@"add_google_address"]];
        object.add_id    = [NSString stringWithFormat:@"%@",dataDict[@"add_id"]];
        object.add_latitude    = [NSString stringWithFormat:@"%@",dataDict[@"add_latitude"]];
        object.add_longitude    = [NSString stringWithFormat:@"%@",dataDict[@"add_longitude"]];
        object.add_name    = [NSString stringWithFormat:@"%@",dataDict[@"add_name"]];
        object.add_street_name    = [NSString stringWithFormat:@"%@",dataDict[@"add_street_name"]];
        object.sap_id    = [NSString stringWithFormat:@"%@",dataDict[@"sap_id"]];
        object.user_id    = [NSString stringWithFormat:@"%@",dataDict[@"user_id"]];
    }
    [[DataManager sharedManager] saveContext];
}

-(void)makeDefaultAddress:(NSString*)user_id
{
    BOOL make=YES;
    NSMutableArray *array=[NSMutableArray arrayWithArray:[self arrayAddress:user_id]];
    
    for (Address *object in array)
    {
        if (object.defaultAddress)
        {
            make=NO;
            break;
        }
    }
    if (make)
    {
        if (array.count>0)
        {
            Address *object =array[0]; // update
            object.defaultAddress=YES;
            [[DataManager sharedManager] saveContext];
        }
    }
}
-(Address *) createAddress;
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Address" inManagedObjectContext:self.managedObjectContext];
    Address * address = [[Address alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return address;
  
}
-(NSArray *) arrayAddress :(NSString*)user_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Address" inManagedObjectContext:self.managedObjectContext]];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.user_id == %@", user_id]];

    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    return result;
}
-(Address*)addressById:(NSString*)add_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Address" inManagedObjectContext:self.managedObjectContext]];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.add_id == %@", add_id]];
    
    
    NSError * error = nil;
    NSArray * result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error!=nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    
    NSMutableArray *arrayOfTips=[NSMutableArray arrayWithArray:result];
    
    if (result.count>1)
    {
        
        for (int i=0; i<result.count-1; i++)
        {
            Address *address  =result[i];
            
            [self.managedObjectContext deleteObject:address];
            [arrayOfTips removeObject:address];
        }
        
    }
    
    if (arrayOfTips)
    {
        return [arrayOfTips firstObject];
    }
    return nil;
}
- (void)setDefaultAddress:(NSString*)add_id user_id:(NSString*)user_id;
{
    NSArray *AddressArray=[self arrayAddress:user_id];
    
    for (int i=0; i<AddressArray.count; ++i)
    {
        Address*object =AddressArray[i];
    
        
        if ([object.add_id isEqualToString:add_id])
        {
            object.defaultAddress=YES;
        }
        else
        {
            object.defaultAddress=NO;
        }
    }
    [[DataManager sharedManager] saveContext];
}
#pragma mark - Cart
- (void)saveCart:(NSMutableDictionary*)cartDict quantity:(NSString*)quantity;
{
    Cart *object =[[DataManager sharedManager] cartById:cartDict[@"dish_id"]]; // update
    
    if (!object)
    {
        object =[[DataManager sharedManager] createCart]; // insert
    }
    
    object.dish_id               = [NSString stringWithFormat:@"%@",cartDict[@"dish_id"]];
    object.dish_kitchen_id       = [NSString stringWithFormat:@"%@",cartDict[@"dish_kitchen_id"]];
    object.dish_title_ar         = [NSString stringWithFormat:@"%@",cartDict[@"dish_title_ar"]];
    object.dish_title_en         = [NSString stringWithFormat:@"%@",cartDict[@"dish_title_en"]];
    object.dish_image            = [NSString stringWithFormat:@"%@",cartDict[@"dish_image"]];
    object.dish_price            = [NSString stringWithFormat:@"%@",cartDict[@"dish_price"]];
    
    int qty = [object.quantity intValue];
    if (!qty)
    {
        qty=1;
    }
    else
    {
        qty+=1;
    }
    object.quantity              = [NSString stringWithFormat:@"%d",qty];

    [[DataManager sharedManager] saveContext];
}
- (void)saveCartReOrder:(NSMutableArray*)order;
{
    for (NSMutableDictionary *orderDict in order)
    {
        Cart *object =[[DataManager sharedManager] cartById:orderDict[@"dish_id"]]; // update
        
        if (!object)
        {
            object =[[DataManager sharedManager] createCart]; // insert
        }
        
        object.dish_id               = [NSString stringWithFormat:@"%@",orderDict[@"dish_id"]];
        object.dish_kitchen_id       = [NSString stringWithFormat:@"%@",orderDict[@"dish_kitchen_id"]];
        object.dish_title_ar         = [NSString stringWithFormat:@"%@",orderDict[@"dish_title_ar"]];
        object.dish_title_en         = [NSString stringWithFormat:@"%@",orderDict[@"dish_title_en"]];
        object.dish_image            = [NSString stringWithFormat:@"%@",orderDict[@"dish_image"]];
        object.dish_price            = [NSString stringWithFormat:@"%@",orderDict[@"dish_price"]];
        
        int qty = [object.quantity intValue];
//        if (!qty)
//        {
//            qty=1;
//        }
//        else
        {
            qty+=[orderDict[@"ord_count"] integerValue];
        }
        object.quantity              = [NSString stringWithFormat:@"%d",qty];
        
        [[DataManager sharedManager] saveContext];
    }
}
- (void)deleteCart:(NSMutableDictionary*)cartDict;
{
    Cart *object =[[DataManager sharedManager] cartById:cartDict[@"dish_id"]]; // update
    if ([object.quantity intValue]>1)
    {
        int qty = [object.quantity intValue];
        object.quantity        = [NSString stringWithFormat:@"%d",qty-1];
    }
    else
    {
        if (object)
        {
            [self.managedObjectContext deleteObject:object];
        }
    }
    [[DataManager sharedManager] saveContext];
}
- (void)updateCartByCartObject:(Cart*)object quantity:(NSString*)quantity;
{
    int qty = [object.quantity intValue];
    if (!qty)
    {
        qty=1;
    }
    else
    {
        qty+=1;
    }
    object.quantity              = [NSString stringWithFormat:@"%d",qty];
    
    [[DataManager sharedManager] saveContext];
}
- (void)deleteCartbyCartOject:(Cart*)object;
{
    if ([object.quantity intValue]>1)
    {
        int qty = [object.quantity intValue];
        object.quantity        = [NSString stringWithFormat:@"%d",qty-1];
    }
    else
    {
        if (object)
        {
            [self.managedObjectContext deleteObject:object];
        }
    }
    [[DataManager sharedManager] saveContext];
}
- (void)deleteCartOjectCompletely:(Cart*)object;
{
        if (object)
        {
            [self.managedObjectContext deleteObject:object];
        }
    [[DataManager sharedManager] saveContext];
}
- (void)deleteAllCart;
{
    NSMutableArray *cartArray=[NSMutableArray arrayWithArray:[self arrayCart]];

    for (Cart *object in cartArray)
    {
        [self.managedObjectContext deleteObject:object];
    }
    [[DataManager sharedManager] saveContext];
}
-(Cart *) createCart;
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Cart" inManagedObjectContext:self.managedObjectContext];
    Cart * cart = [[Cart alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return cart;
}
-(NSArray *) arrayCart;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Cart" inManagedObjectContext:self.managedObjectContext]];
    
    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    return result;
}
-(Cart*)cartById:(NSString*)dish_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Cart" inManagedObjectContext:self.managedObjectContext]];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.dish_id == %@", dish_id]];
    
    
    NSError * error = nil;
    NSArray * result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error!=nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    
    NSMutableArray *arrayOfCart=[NSMutableArray arrayWithArray:result];
    
    if (result.count>1)
    {
        
        for (int i=0; i<result.count-1; i++)
        {
            Cart *cart  =result[i];
            
            [self.managedObjectContext deleteObject:cart];
            [arrayOfCart removeObject:cart];
        }
        
    }
    
    if (arrayOfCart)
    {
        return [arrayOfCart firstObject];
    }
    return nil;
}

#pragma mark - Cart
- (void)saveUser:(NSMutableDictionary*)userDict;
{
    [SettingsClass setAuth_key:userDict[@"auth_key"]];
    
    User *object =[[DataManager sharedManager] getUser]; // update
    
    if (!object)
    {
        object =[[DataManager sharedManager] createUser]; // insert
    }
    
    object.city_id               = [NSString stringWithFormat:@"%@",userDict[@"city_id"]];
    object.client_sub_type               = [NSString stringWithFormat:@"%@",userDict[@"client_sub_type"]];
    object.client_type               = [NSString stringWithFormat:@"%@",userDict[@"client_type"]];
    object.email               = [NSString stringWithFormat:@"%@",userDict[@"email"]];
    object.frist_name               = [NSString stringWithFormat:@"%@",userDict[@"frist_name"]];
    object.last_name               = [NSString stringWithFormat:@"%@",userDict[@"last_name"]];
    object.mobile               = [NSString stringWithFormat:@"%@",userDict[@"mobile"]];
    object.email_active               = [NSString stringWithFormat:@"%@",userDict[@"email_active"]];
    object.mobile_active               = [NSString stringWithFormat:@"%@",userDict[@"mobile_active"]];
    object.mobile_active_code               = [NSString stringWithFormat:@"%@",userDict[@"mobile_active_code"]];
    object.notification               = [NSString stringWithFormat:@"%@",userDict[@"notification"]];
    object.register_date               = [NSString stringWithFormat:@"%@",userDict[@"register_date"]];
    object.sap_userId               = [NSString stringWithFormat:@"%@",userDict[@"sap_userId"]];
    object.sms_rand               = [NSString stringWithFormat:@"%@",userDict[@"sms_rand"]];
    object.unique_id               = [NSString stringWithFormat:@"%@",userDict[@"unique_id"]];
    object.user_id               = [NSString stringWithFormat:@"%@",userDict[@"user_id"]];
    
    [[DataManager sharedManager] saveContext];
    
}

-(User *) createUser
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:self.managedObjectContext];
    User * user = [[User alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return user;
}
-(User *) getUser;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:self.managedObjectContext]];
    
    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    if (result.count>0)
    {
        return result[0];
    }
    return nil;
}
- (void)removeUser;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:self.managedObjectContext]];
    
    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    for (User *object in result)
    {
        [self.managedObjectContext deleteObject:object];
    }
    [[DataManager sharedManager] saveContext];
}

#pragma mark - Area
- (void)saveArea:(NSMutableArray*)areaArray;
{
    for (int i=0; i<areaArray.count; ++i)
    {
        NSMutableDictionary *dataDict =areaArray[i];
        Area *object =[[DataManager sharedManager] areaById:dataDict[@"area_id"]]; // update
        
        if (!object)
        {
            object =[[DataManager sharedManager] createArea]; // insert
        }
        object.area_id       = [NSString stringWithFormat:@"%@",dataDict[@"area_id"]];
        object.area_active       = [NSString stringWithFormat:@"%@",dataDict[@"area_active"]];
        object.area_code         = [NSString stringWithFormat:@"%@",dataDict[@"area_code"]];
        object.area_city_id    = [NSString stringWithFormat:@"%@",dataDict[@"area_city_id"]];
        object.area_country_id    = [NSString stringWithFormat:@"%@",dataDict[@"area_country_id"]];
        object.area_title_ar    = [NSString stringWithFormat:@"%@",dataDict[@"area_title_ar"]];
        object.area_title_en    = [NSString stringWithFormat:@"%@",dataDict[@"area_title_en"]];
    }
    [[DataManager sharedManager] saveContext];
}
-(Area *) createArea;
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Area" inManagedObjectContext:self.managedObjectContext];
    Area * area = [[Area alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return area;
}
-(NSArray *) arrayarea :(NSString*)city_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Area" inManagedObjectContext:self.managedObjectContext]];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.area_city_id == %@", city_id]];
    if ([SettingsClass isLanguageArabic])
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"area_title_ar" ascending:YES]]];
    }
    else
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"area_title_en" ascending:YES]]];
    }

    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    return result;
}
-(Area*)areaById:(NSString*)area_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Area" inManagedObjectContext:self.managedObjectContext]];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.area_id == %@", area_id]];
    
    
    NSError * error = nil;
    NSArray * result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error!=nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    
    NSMutableArray *arrayOfArea=[NSMutableArray arrayWithArray:result];
    
    if (result.count>1)
    {
        
        for (int i=0; i<result.count-1; i++)
        {
            Area *area  =result[i];
            
            [self.managedObjectContext deleteObject:area];
            [arrayOfArea removeObject:area];
        }
        
    }
    
    if (arrayOfArea)
    {
        return [arrayOfArea firstObject];
    }
    return nil;
}

#pragma mark - City
- (void)saveCity:(NSMutableArray*)cityArray;
{
    for (int i=0; i<cityArray.count; ++i)
    {
        NSMutableDictionary *dataDict =cityArray[i];
        City *object =[[DataManager sharedManager] cityById:dataDict[@"city_id"]]; // update
        
        if (!object)
        {
            object =[[DataManager sharedManager] createCity]; // insert
        }
        object.city_active       = [NSString stringWithFormat:@"%@",dataDict[@"city_active"]];
        object.city_country_id       = [NSString stringWithFormat:@"%@",dataDict[@"city_country_id"]];
        object.city_id         = [NSString stringWithFormat:@"%@",dataDict[@"city_id"]];
        object.city_title_en    = [NSString stringWithFormat:@"%@",dataDict[@"city_title_en"]];
        object.city_title_ar    = [NSString stringWithFormat:@"%@",dataDict[@"city_title_ar"]];
        object.plant    = [NSString stringWithFormat:@"%@",dataDict[@"plant"]];
    }
    [[DataManager sharedManager] saveContext];
}
-(City *) createCity;
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"City" inManagedObjectContext:self.managedObjectContext];
    City * city = [[City alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return city;
}
-(NSArray *) arrayCity;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"City" inManagedObjectContext:self.managedObjectContext]];
    if ([SettingsClass isLanguageArabic])
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"city_title_ar" ascending:YES]]];
    }
    else
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"city_title_en" ascending:YES]]];
    }

    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    return result;
}
-(City*)cityById:(NSString*)city_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"City" inManagedObjectContext:self.managedObjectContext]];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.city_id == %@", city_id]];
    
    
    NSError * error = nil;
    NSArray * result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error!=nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    
    NSMutableArray *arrayOfcity=[NSMutableArray arrayWithArray:result];
    
    if (result.count>1)
    {
        
        for (int i=0; i<result.count-1; i++)
        {
            City *city  =result[i];
            
            [self.managedObjectContext deleteObject:city];
            [arrayOfcity removeObject:city];
        }
        
    }
    
    if (arrayOfcity)
    {
        return [arrayOfcity firstObject];
    }
    return nil;
}

#pragma mark - Channel
- (void)saveChannel:(NSMutableArray*)channelArray;
{
    for (int i=0; i<channelArray.count; ++i)
    {
        NSMutableDictionary *dataDict =channelArray[i];
        Channel *object =[[DataManager sharedManager] channelById:dataDict[@"id"]]; // update
        
        if (!object)
        {
            object =[[DataManager sharedManager] createChannel]; // insert
        }
        object.channel_id       = [NSString stringWithFormat:@"%@",dataDict[@"id"]];
        object.active       = [NSString stringWithFormat:@"%@",dataDict[@"parent_id"]];
        object.parent_id         = [NSString stringWithFormat:@"%@",dataDict[@"city_id"]];
        object.sap_id    = [NSString stringWithFormat:@"%@",dataDict[@"sap_id"]];
        object.title_ar    = [NSString stringWithFormat:@"%@",dataDict[@"title_ar"]];
        object.title_en    = [NSString stringWithFormat:@"%@",dataDict[@"title_en"]];
        object.type    = [NSString stringWithFormat:@"%@",dataDict[@"type"]];
    }
    [[DataManager sharedManager] saveContext];
}
-(Channel *) createChannel;
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Channel" inManagedObjectContext:self.managedObjectContext];
    Channel * channel = [[Channel alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return channel;
}
-(NSArray *) arrayChannel;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Channel" inManagedObjectContext:self.managedObjectContext]];
    if ([SettingsClass isLanguageArabic])
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"title_ar" ascending:YES]]];
    }
    else
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"title_en" ascending:YES]]];
    }

    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    return result;
}
-(Channel*)channelById:(NSString*)channel_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Channel" inManagedObjectContext:self.managedObjectContext]];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.channel_id == %@", channel_id]];
    
    
    NSError * error = nil;
    NSArray * result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error!=nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    
    NSMutableArray *arrayOfChannel=[NSMutableArray arrayWithArray:result];
    
    if (result.count>1)
    {
        
        for (int i=0; i<result.count-1; i++)
        {
            Channel *channel  =result[i];
            
            [self.managedObjectContext deleteObject:channel];
            [arrayOfChannel removeObject:channel];
        }
        
    }
    
    if (arrayOfChannel)
    {
        return [arrayOfChannel firstObject];
    }
    return nil;

}

#pragma mark - SubChannel
- (void)saveSubChannel:(NSMutableArray*)subChannelArray;
{
    for (int i=0; i<subChannelArray.count; ++i)
    {
        NSMutableDictionary *dataDict =subChannelArray[i];
        SubChannel *object =[[DataManager sharedManager] subChannelById:dataDict[@"id"]]; // update
        
        if (!object)
        {
            object =[[DataManager sharedManager] createSubChannel]; // insert
        }
        object.subChannel_id       = [NSString stringWithFormat:@"%@",dataDict[@"id"]];
        object.active       = [NSString stringWithFormat:@"%@",dataDict[@"parent_id"]];
        object.parent_id         = [NSString stringWithFormat:@"%@",dataDict[@"city_id"]];
        object.sap_id    = [NSString stringWithFormat:@"%@",dataDict[@"sap_id"]];
        object.title_ar    = [NSString stringWithFormat:@"%@",dataDict[@"title_ar"]];
        object.title_en    = [NSString stringWithFormat:@"%@",dataDict[@"title_en"]];
        object.type    = [NSString stringWithFormat:@"%@",dataDict[@"type"]];
    }
    [[DataManager sharedManager] saveContext];
}
-(SubChannel *) createSubChannel;
{
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"SubChannel" inManagedObjectContext:self.managedObjectContext];
    SubChannel * subChannel = [[SubChannel alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    return subChannel;
}
-(NSArray *) arraySubChannel;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"SubChannel" inManagedObjectContext:self.managedObjectContext]];
    if ([SettingsClass isLanguageArabic])
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"title_ar" ascending:YES]]];
    }
    else
    {
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"title_en" ascending:YES]]];
    }

    NSError * error = nil;
    NSArray * result=[NSArray array];
    
    result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    return result;
}
-(SubChannel*)subChannelById:(NSString*)subChannel_id;
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"SubChannel" inManagedObjectContext:self.managedObjectContext]];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.subChannel_id == %@", subChannel_id]];
    
    
    NSError * error = nil;
    NSArray * result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error!=nil) {
        NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
    }
    
    NSMutableArray *arrayOfSubChannel=[NSMutableArray arrayWithArray:result];
    
    if (result.count>1)
    {
        
        for (int i=0; i<result.count-1; i++)
        {
            SubChannel *subChannel  =result[i];
            
            [self.managedObjectContext deleteObject:subChannel];
            [arrayOfSubChannel removeObject:subChannel];
        }
        
    }
    
    if (arrayOfSubChannel)
    {
        return [arrayOfSubChannel firstObject];
    }
    return nil;

}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationURLDocumentsDirectory
{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "MS.VisitorBook" in the application's documents directory.
    return [NSURL fileURLWithPath:[self applicationDocumentsDirectory]];
}

- (NSString *)applicationDocumentsDirectory
{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "MS.VisitorBook" in the application's documents directory.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    return documentsDirectory;
}

- (NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BeRain" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationURLDocumentsDirectory] URLByAppendingPathComponent:@"BeRain.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
            if (error!=nil) {
                NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
            }
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
#ifdef DEBUG
            abort();
#endif
            
        }
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext
{
    
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    if (managedObjectContext != nil) {
        NSError *error = nil;
        [managedObjectContext save:&error];
        
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
//            #ifdef DEBUG
//            abort();
//            #endif
        }
    }
}

- (BOOL)dropDataStore
{

    NSError *error = nil;
    NSArray *stores = [self.persistentStoreCoordinator persistentStores];
    
    for(NSPersistentStore *store in stores) {
        [self.persistentStoreCoordinator removePersistentStore:store error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:&error];
        if (error!=nil) {
            NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
        }
    }
    
    _persistentStoreCoordinator = nil;
    _managedObjectContext = nil;
    _managedObjectModel = nil;
    
    return YES;
}

-(NSMutableDictionary *)formattedDictionary
{
    NSMutableArray *cartArray=[NSMutableArray arrayWithArray:[self arrayCart]];

    NSMutableDictionary *formattedDictionary=[NSMutableDictionary dictionary];
    
    //    NSMutableArray *cartArray=[NSMutableArray arrayWithArray:self.dataDict[@"cartArray"]];
    NSMutableArray *formattedCartArray=[NSMutableArray array];
    double vat=[[SingletonClass sharedSingletonClass].settingsDictionary[@"vat"] doubleValue];

    if (cartArray.count>0)
    {
        
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            NSMutableDictionary *cartDict=[NSMutableDictionary dictionary];
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            [cartDict setObject:cart.quantity forKey:@"count"];
            [cartDict setObject:cart.dish_id forKey:@"dish_id"];
            [cartDict setObject:cart.dish_price forKey:@"price"];
            [formattedCartArray addObject:cartDict];
        }
        totalPrice=totalPrice*100;
        
        [formattedDictionary setObject:formattedCartArray forKey:@"formattedArray"];
        [formattedDictionary setObject:[NSString stringWithFormat:@"%.02f",totalPrice+(totalPrice*vat)/100] forKey:@"totalPrice"];
        [formattedDictionary setObject:[NSString stringWithFormat:@"%.02f",totalPrice] forKey:@"totalPriceWithoutVAT"];
        
        return formattedDictionary;
    }
    else
    {
        return nil;
    }
}


@end
