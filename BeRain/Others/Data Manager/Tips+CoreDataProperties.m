//
//  Tips+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Tips+CoreDataProperties.h"

@implementation Tips (CoreDataProperties)

+ (NSFetchRequest<Tips *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Tips"];
}

@dynamic id_row;
@dynamic active;
@dynamic date;
@dynamic detail_ar;
@dynamic detail_en;

@end
