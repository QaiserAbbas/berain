//
//  Area+CoreDataProperties.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/27/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "Area+CoreDataProperties.h"

@implementation Area (CoreDataProperties)

+ (NSFetchRequest<Area *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Area"];
}

@dynamic area_active;
@dynamic area_code;
@dynamic area_city_id;
@dynamic area_country_id;
@dynamic area_id;
@dynamic area_title_ar;
@dynamic area_title_en;

@end
