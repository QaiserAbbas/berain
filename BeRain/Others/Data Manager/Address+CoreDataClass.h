//
//  Address+CoreDataClass.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/4/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Address : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Address+CoreDataProperties.h"
