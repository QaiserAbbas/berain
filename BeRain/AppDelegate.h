//
//  AppDelegate.h
//  BeRain
//
//  Created by Qaiser Abbas on 11/22/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, AppsFlyerTrackerDelegate>
{
    UIStoryboard* storyboard;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

-(void)changeLanguageMainStoryBoard;
@end

