//
//  AppDelegate.m
//  BeRain
//
//  Created by Qaiser Abbas on 11/22/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "AppDelegate.h"
#import "SideMenuViewController.h"
#import "HomeViewController.h"
//#import "AppConstants.h"

@import GoogleMaps;
@import GooglePlaces;

static NSString *const kHNKDemoGooglePlacesAutocompleteApiKey = @"AIzaSyCAO_vsg-9gk6dTnbC7Ahx4yaXQ3ubcuRw";

@interface AppDelegate ()<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CLLocation        *currentLocation;
    
    NSString *CURRENT_ADDRESS;

}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:kHNKDemoGooglePlacesAutocompleteApiKey];
    [GMSPlacesClient provideAPIKey:kHNKDemoGooglePlacesAutocompleteApiKey];
    [self getCurrentLocation];
    [LanguageManager setupCurrentLanguage];
    
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"n4QzPHhpEBvJhdru24Ys3h";
    [AppsFlyerTracker sharedTracker].appleAppID = @"1298014792";
    [AppsFlyerTracker sharedTracker].delegate = self;

#ifdef DEBUG
    [AppsFlyerTracker sharedTracker].isDebug = true;
#endif

    [self changeLanguageMainStoryBoard];    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"BeRain"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}
-(void)changeLanguageMainStoryBoard
{
    
    storyboard = nil;
    NSBundle *myBundle=[NSBundle mainBundle];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[LanguageManager currentLanguageString] ofType:@"lproj"];
    if (path == nil) {
        myBundle = [NSBundle mainBundle];
    } else {
        myBundle = [NSBundle bundleWithPath:path];
    }
    if (myBundle == nil) {
        myBundle = [NSBundle mainBundle];
    }

    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SideMenuViewController *leftMenu = [storyboard instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
    
    
    
    [SlideNavigationController sharedInstance].rightMenu = leftMenu;
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    [SlideNavigationController sharedInstance].portraitSlideOffset = 188;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidClose object:nil queue:nil usingBlock:^(NSNotification *note) {
        //        NSString *menu = note.userInfo[@"menu"];
        //        NSLog(@"Closed %@", menu);
        [leftMenu updateProfile];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
        //        NSString *menu = note.userInfo[@"menu"];
        //        NSLog(@"Opened %@", menu);
        [leftMenu updateProfile];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidReveal object:nil queue:nil usingBlock:^(NSNotification *note) {
        //        NSString *menu = note.userInfo[@"menu"];
        //        NSLog(@"Revealed %@", menu);
        [leftMenu updateProfile];
    }];
    
    SlideNavigationContorllerAnimatorSlide *alideAndFadeAnimator = [[SlideNavigationContorllerAnimatorSlide alloc] initWithSlideMovement:0.8];
    [SlideNavigationController sharedInstance].menuRevealAnimator = alideAndFadeAnimator;
    
    if ([LanguageManager isCurrentLanguageRTL]) {
        if ([[[UIView alloc] init] respondsToSelector:@selector(setSemanticContentAttribute:)]) {
            [[UIView appearance] setSemanticContentAttribute:
             UISemanticContentAttributeForceRightToLeft];
        }
    }else {
        if ([[[UIView alloc] init] respondsToSelector:@selector(setSemanticContentAttribute:)]) {
            [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        }
    }


}

- (void)getCurrentLocation
{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if([[[UIDevice currentDevice]systemVersion]floatValue]>=8.0) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    //NSLog(@"didUpdateToLocation: %@",[locations lastObject]);
    currentLocation = [locations lastObject];
    if (currentLocation != nil) {
//        CURRENT_LATITUDE    =   currentLocation.coordinate.latitude;
//        CURRENT_LONGITUDE   =   currentLocation.coordinate.longitude;
        [[SingletonClass sharedSingletonClass].settingsDictionary setObject:[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude] forKey:@"latitude"];
        [[SingletonClass sharedSingletonClass].settingsDictionary setObject:[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] forKey:@"longitude"];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                       {
                           [self getAddressFromLocation:currentLocation];
                       });
    }
}

- (void) getAddressFromLocation: (CLLocation*) location {
    if (CURRENT_ADDRESS) {
        return;
    }
    //NSLog(@"getting address...");
    NSString *req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&output=csv",[NSString stringWithFormat:@"%f",location.coordinate.latitude],[NSString stringWithFormat:@"%f",location.coordinate.longitude]];
    
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    
    if (result) {
        NSError *error;
        NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        NSArray *resultArr = [dictResponse objectForKey:@"results"]; //objectForKey:@"geometry"];
        
        if(resultArr.count > 0) {
            NSDictionary *dict = [resultArr objectAtIndex:0];
            if ([dict objectForKey:@"formatted_address"]) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    CURRENT_ADDRESS = [dict objectForKey:@"formatted_address"];
                    [[SingletonClass sharedSingletonClass].settingsDictionary setObject:[NSString stringWithFormat:@"%@",CURRENT_ADDRESS] forKey:@"currentAddress"];
                });
            }
        }
    }
}


@end
