//
//  AddressViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"


@interface AddressViewController : BaseViewController
{
    User *currentUser;

    IBOutlet UITableView *addressTableView;
    IBOutlet UIButton *addressButton;
    
    IBOutlet UIView *popUpView;
    NSMutableArray *addressArray;
    Address *selectedAddres;
}

@property (strong, nonatomic) NSString *viewType;
@property (strong, nonatomic) NSMutableDictionary *dataDict;

- (IBAction)addressButtonTap:(UIButton *)sender;
- (IBAction)okButtonTap:(UIButton *)sender;
- (IBAction)cancelButtonTap:(UIButton *)sender;
@end
