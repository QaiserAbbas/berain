//
//  AddressViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "AddressViewController.h"
#import "AddressTableViewCell.h"
#import "CheckOutViewController.h"

@interface AddressViewController ()

@end

@implementation AddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];

    addressButton.layer.cornerRadius=addressButton.frame.size.height/2;
    addressButton.layer.masksToBounds = YES;

}
-(void)viewWillAppear:(BOOL)animated
{
    addressArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayAddress:currentUser.user_id]];
    [addressTableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView Delegates
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}
-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return addressArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* Identifier = @"AddressTableViewCell";
    
    AddressTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if( cell == nil )
    {
        NSString* cellName = @"AddressTableViewCell";
        if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            //                cellName = @"LiveEventsTableViewCell-iPad";
        }
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.deleteButton addTarget:self action:@selector(deleteButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteButton setTag:indexPath.row];
    
    [cell setdataCell:addressArray[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Address *addr=addressArray[indexPath.row];

    if ([self.viewType isEqualToString:@"checkout"])
    {
        currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];
        
        if (currentUser)
        {
            if ([self totalNumberOfItems] < [[SingletonClass sharedSingletonClass].settingsDictionary[@"min_order"] intValue] )
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:[NSString stringWithFormat:@"%@ %@ %@/%@",NSLocalizedString(@"Minimum order quantity is", @""),[SingletonClass sharedSingletonClass].settingsDictionary[@"min_order"],NSLocalizedString(@"Carton", @""),NSLocalizedString(@"Shrink", @"")]];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
                
                return;
            }
            
            
            //        if ([self.viewType isEqualToString:@"checkout"])
            {
                CheckOutViewController* checkOutViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckOutViewController"];
                checkOutViewController.dataDict=[NSMutableDictionary dictionaryWithDictionary:self.dataDict];
                [checkOutViewController.dataDict setObject:addr forKey:@"address"];
                [[SlideNavigationController sharedInstance] pushViewController:checkOutViewController animated:YES];
            }
            
        }
        else
        {
        }
 
    }
    else
    {
        
    }
    [[DataManager sharedManager] setDefaultAddress:addr.add_id user_id:currentUser.user_id];
    
    addressArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayAddress:currentUser.user_id]];
    [addressTableView reloadData];


}
-(void)deleteButtonTap:(UIButton*)sender
{
    [popUpView setHidden:NO];

    selectedAddres=addressArray[sender.tag];
}
- (IBAction)addressButtonTap:(UIButton *)sender
{
}

- (IBAction)okButtonTap:(UIButton *)sender
{
    [popUpView setHidden:YES];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:selectedAddres.add_id forKey:@"id"];
    [paramDict setObject:currentUser.user_id forKey:@"user_id"];

    
    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_DeleteAddress OnCompletion:^(id returnDict, NSError *error) {
        if ([returnDict[@"status"] intValue]==1)
        {
            [[DataManager sharedManager].managedObjectContext deleteObject:selectedAddres];

            addressArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayAddress:currentUser.user_id]];
            [addressTableView reloadData];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            [alert setTitle:NSLocalizedString(@"Berain", @"")];
            [alert setMessage:NSLocalizedString(@"Please try again", @"")];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
            [alert show];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    }];

}

- (IBAction)cancelButtonTap:(UIButton *)sender
{
    [popUpView setHidden:YES];
}

-(int)totalNumberOfItems
{
    NSMutableArray *cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];

    if (cartArray.count>0)
    {
        int totalItems=0;
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            totalItems= totalItems+[cart.quantity intValue];
        }
        return totalItems;
    }
    else
    {
        return 0;
    }
}
@end
