//
//  AddressTableViewCell.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "AddressTableViewCell.h"
#import "Address+CoreDataClass.h"

@implementation AddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setdataCell:(id)object;
{
    Address *address=(Address*)object;
    
    [self.name setText:address.add_name];
    [self.detail setText:[NSString stringWithFormat:@"%@, %@:%@, %@:%@",address.add_detail,NSLocalizedString(@"Building number", @""),address.add_block,NSLocalizedString(@"Floor number", @""),address.add_floor]];
    if (address.defaultAddress)
    {
        [self.isDefaultImageView setHidden:NO];
    }
    else
    {
        [self.isDefaultImageView setHidden:YES];
    }
}
@end
