//
//  AddressTableViewCell.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *detail;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIImageView *isDefaultImageView;
-(void)setdataCell:(id)object;

@end
