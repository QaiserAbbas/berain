//
//  StaticWebViewViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface StaticWebViewViewController : BaseViewController
{
    IBOutlet UIWebView *staticWebView;
    IBOutlet UILabel *viewTitleLabel;
    
}
@property (strong, nonatomic) NSString *viewName;

@end
