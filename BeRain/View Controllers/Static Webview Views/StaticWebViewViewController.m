//
//  StaticWebViewViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "StaticWebViewViewController.h"

@interface StaticWebViewViewController ()<UIWebViewDelegate>

@end

@implementation StaticWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *urlAddress = @"";
    NSString *lang=@"/?lang=en";
    if ([SettingsClass isLanguageArabic])
    {
        lang=@"";
    }
    
    [viewTitleLabel setText:NSLocalizedString(self.viewName, @"")];
    
    if ([self.viewName isEqualToString:@"About Berain"])
    {
        urlAddress=[NSString stringWithFormat:@"http://berain.com.sa/%%D8%%A7%%D9%%84%%D8%%B1%%D8%%A6%%D9%%8A%%D8%%B3%%D9%%8A%%D8%%A91/%@",lang];
    }
    else if ([self.viewName isEqualToString:@"Why Berain?"])
    {
        urlAddress=[NSString stringWithFormat:@"http://berain.com.sa/our-team/%@",lang];
    }
    else if ([self.viewName isEqualToString:@"Career"])
    {
        urlAddress=[NSString stringWithFormat:@"http://berain.com.sa/%%D8%%A7%%D9%%84%%D8%%AA%%D9%%88%%D8%%B8%%D9%%8A%%D9%%81/%@",lang];
    }
    else if ([self.viewName isEqualToString:@"Contact Us"])
    {
        urlAddress=[NSString stringWithFormat:@"http://berain.com.sa/contact/%@",lang];
    }
    else if ([self.viewName isEqualToString:@"News"])
    {
        urlAddress=[NSString stringWithFormat:@"http://berain.com.sa/%%D8%%A7%%D9%%84%%D8%%A3%%D8%%AE%%D8%%A8%%D8%%A7%%D8%%B1/%@",lang];
    }
    else if ([self.viewName isEqualToString:@"Social Media"])
    {
        urlAddress=[NSString stringWithFormat:@"http://berain.com.sa/%%D8%%AD%%D8%%A7%%D8%%A6%%D8%%B7-%%D8%%A7%%D9%%84%%D8%%B3%%D9%%88%%D8%%B4%%D9%%8A%%D8%%A7%%D9%%84/%@",lang];
    }
    else if ([self.viewName isEqualToString:@"FAQ"])
    {
        urlAddress=[NSString stringWithFormat:@"http://berain.com.sa/%%D8%%A7%%D9%%84%%D8%%A7%%D8%%B3%%D8%%A6%%D9%%84%%D8%%A9-%%D8%%A7%%D9%%84%%D8%%B4%%D8%%A7%%D8%%A6%%D8%%B9%%D8%%A9/%@",lang];
    }
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [staticWebView loadRequest:requestObj];
    staticWebView.delegate=self;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (webView.isLoading)
        return;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
