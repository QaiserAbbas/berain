//
//  ThanksViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ThanksViewController.h"

@interface ThanksViewController ()

@end

@implementation ThanksViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    homeButton.layer.cornerRadius=homeButton.frame.size.height/2;
    homeButton.layer.masksToBounds = YES;

    historyButton.layer.cornerRadius=historyButton.frame.size.height/2;
    historyButton.layer.masksToBounds = YES;

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *today=[dateFormat stringFromDate:[NSDate date]];
    
    [dateLabel setText:today];
    [orderNumberLabel setText:[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Order number", @""),self.orderID]];
    if (self.reeferenceNumber)
    {
        [referenceLabel setText:[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Payment Reference", @""),self.reeferenceNumber]];
    }
    else
    {
        [referenceLabel setHidden:YES];
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeButtonTapped:(UIButton *)sender
{
    [self homeButtonTap:sender];
}

- (IBAction)historyButtonTap:(UIButton *)sender
{
    [self myOrdersHistoryButtonTap:sender];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
