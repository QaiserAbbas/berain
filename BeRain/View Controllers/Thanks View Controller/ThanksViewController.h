//
//  ThanksViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface ThanksViewController : BaseViewController
{
    IBOutlet UIView *thankYouView;
    IBOutlet UILabel *orderNumberLabel;
    IBOutlet UILabel *dateLabel;
    IBOutlet UIButton *homeButton;
    IBOutlet UIButton *historyButton;
    IBOutlet UILabel *referenceLabel;
}

@property (strong, nonatomic) NSString *orderID;
@property (strong, nonatomic) NSString *reeferenceNumber;

- (IBAction)homeButtonTapped:(UIButton *)sender;
- (IBAction)historyButtonTap:(UIButton *)sender;

@end
