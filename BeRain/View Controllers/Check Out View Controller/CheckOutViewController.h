//
//  CheckOutViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/7/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface CheckOutViewController : BaseViewController
{
    User *currentUser;

    IBOutletCollection(UIImageView) NSArray *bgImageViews;
    IBOutletCollection(UIImageView) NSArray *bgWhiteImageViews;
    IBOutlet UIButton *dateButton;
    IBOutlet UIButton *hoursButton;
    IBOutlet UITextView *notesTextView;
    IBOutlet UIImageView *radioCash;
    IBOutlet UIImageView *radioCredit;
    IBOutlet UIImageView *radioSadat;
    IBOutlet UIButton *placeOrderButton;

    UIImage *radioImage;
    UIImage *radioCheckImage;
    UIDatePicker *datePicker;
    
    NSMutableArray*hoursArray;
    NSInteger slectedindex;
    
    IBOutlet UITableView *cartTableView;
    NSMutableArray *cartArray;
    IBOutlet UILabel *amountLabel;
    IBOutlet UILabel *vatLabel;
    IBOutlet UILabel *totalAmountLabe;
    IBOutlet UILabel *vatQuanitity;
    IBOutlet UIView *sadadView;
    IBOutlet UIView *creditCardView;
    IBOutlet UIView *homeDevliveryView;
    IBOutlet NSLayoutConstraint *homeDeliveryUpperConstraint;
    IBOutlet NSLayoutConstraint *sadadUpperConstraint;
    IBOutlet NSLayoutConstraint *creditCardUpperConstraint;
    IBOutlet UILabel *totalAmountLabel;
}
@property (strong, nonatomic) NSMutableDictionary *dataDict;

- (IBAction)dateButtonTap:(UIButton *)sender;
- (IBAction)hoursButtonTap:(UIButton *)sender;
- (IBAction)cashOnDeliveryButtonTa:(UIButton *)sender;
- (IBAction)creditCardButtontap:(UIButton *)sender;
- (IBAction)placeOrderButtonTap:(UIButton *)sender;
- (IBAction)sadatButtonTap:(UIButton *)sender;
@end
