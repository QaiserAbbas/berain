//
//  CardInfoViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/21/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "CardInfoViewController.h"
#import "ThanksViewController.h"

@interface CardInfoViewController ()<LGAlertViewDelegate,UIWebViewDelegate>

@end

@implementation CardInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    proceedButton.layer.cornerRadius=proceedButton.frame.size.height/2;
    proceedButton.layer.masksToBounds = YES;
    
    proceedSadatButton.layer.cornerRadius=proceedSadatButton.frame.size.height/2;
    proceedSadatButton.layer.masksToBounds = YES;

    for (UIImageView*imageView in textFieldBG)
    {
        imageView.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
        imageView.layer.cornerRadius=5;
        imageView.layer.borderWidth=1.0;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderColor=[[UIColor purpleColor] CGColor];
    }
    
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];


    if ([self.dataDict[@"PaymentMethod"] intValue]==1)
    {
        [creditCardView setHidden:NO];
        [sadatView setHidden:YES];
        [navigationTitleLabel setText:NSLocalizedString(@"Payment detail", @"")];
//        navigationTitleLabel.text
    }
    else if ([self.dataDict[@"PaymentMethod"] intValue]==2)
    {
        [creditCardView setHidden:YES];
        [sadatView setHidden:NO];
        [navigationTitleLabel setText:NSLocalizedString(@"Payment detail", @"")];
    }
    
    picker = [[NTMonthYearPicker alloc] init];
    [picker addTarget:self action:@selector(onDatePicked:) forControlEvents:UIControlEventValueChanged];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    // Set mode to month + year
    // This is optional; default is month + year
    picker.datePickerMode = NTMonthYearPickerModeMonthAndYear;
    
    // Set minimum date to January 2000
    // This is optional; default is no min date
    [comps setDay:1];
    [comps setMonth:1];
    [comps setYear:2000];
    picker.minimumDate = [NSDate date];
    
    // Set maximum date to next month
    // This is optional; default is no max date
    [comps setDay:0];
    [comps setMonth:1];
    [comps setYear:2099];
    picker.maximumDate = [cal dateByAddingComponents:comps toDate:[NSDate date] options:0];
    
    // Set initial date to last month
    // This is optional; default is current month/year
    [comps setDay:0];
    [comps setMonth:0];
    [comps setYear:0];
    picker.date = [cal dateByAddingComponents:comps toDate:[NSDate date] options:0];
    NSLog(@"%@",self.dataDict);
    
    NSMutableDictionary *formattedDict=[[DataManager sharedManager] formattedDictionary];
    NSString *str=[NSString stringWithFormat:@"Total Payable amount : "];
    double amount = [formattedDict[@"totalPrice"] doubleValue]/100;
    
    NSString *localStr=[NSString stringWithFormat:@"%@%.02f",NSLocalizedString(str, @""),amount];
    totalAmountLabel.text=localStr;

    if ([SettingsClass isLanguageArabic])
    {
        [cardHolderNameTextfield setTextAlignment:NSTextAlignmentRight];
        [cvvTextfield setTextAlignment:NSTextAlignmentRight];
        [cardNumberTextfield setTextAlignment:NSTextAlignmentRight];
        [sadatAccountIDTextField setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [cardHolderNameTextfield setTextAlignment:NSTextAlignmentLeft];
        [cvvTextfield setTextAlignment:NSTextAlignmentLeft];
        [cardNumberTextfield setTextAlignment:NSTextAlignmentLeft];
        [sadatAccountIDTextField setTextAlignment:NSTextAlignmentLeft];
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)proceedButtonTap:(UIButton *)sender
{
//    NSMutableArray* invalidEntries = [[NSMutableArray alloc] init];
//    if ([creditCardView isHidden]==NO)
//    {
//        if(cardNumberTextfield.text.length<15)
//        {
//            [invalidEntries addObject:@"card number"];
//        }
//        
//        if(cvvTextfield.text.length<2)
//        {
//            [invalidEntries addObject:@"CVV number"];
//        }
//        
//        if(cardHolderNameTextfield.text.length==0)
//        {
//            [invalidEntries addObject:@"card holder name"];
//        }
//    }
//    else
//    {
//        if( sadatAccountIDTextField.text.length<14 )
//        {
//            [invalidEntries addObject:@"Sadad Account ID"];
//        }
//    }
    
    
//    {
//        // Atleast one has invalid data
//        UIAlertView* alert = [[UIAlertView alloc] init];
//        [alert setTitle:@"Insufficient input"];
//        NSMutableString* alertMsg = [[NSMutableString alloc] initWithString:@"Please enter valid input for - "];
//        {
//            for( int i = 0; i < invalidEntries.count; ++i )
//            {
//                if( i == invalidEntries.count - 1 )
//                {
//                    [alertMsg appendFormat:@"%@.", invalidEntries[i]];
//                }
//                else
//                {
//                    [alertMsg appendFormat:@"%@, ", invalidEntries[i]];
//                }
//            }
//        }
//        [alert setMessage:alertMsg];
//        [alert addButtonWithTitle:@"OK"];
//        [alert show];
//    }
//    else
    if( [self checkValidInfo] )
    {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });

    
    NSMutableDictionary *formattedDict=[[DataManager sharedManager] formattedDictionary];

    PaymentRequest *request = [[PaymentRequest alloc]init];
    [request setPaymentType:@"smartRoute"]; // smartRoute or ezConnect based on the wanted interface
    [request isLogging:YES]; // To print Log Statements or not in the console
    [request setPaymentAuthenticationToken:@"AuthenticationToken" valueForKey:@"OWE1Njc0YzQyMDRhODQwNmMyM2IxMmY0"]; //Either set
    
    [request add:@"Amount" :[NSString stringWithFormat:@"%d",[formattedDict[@"totalPrice"] intValue]]];
    [request add:@"Channel" :@"0"];
    [request add:@"ClientIPaddress" :@"79.183.118.121"];
    
    [request add:@"CurrencyISOCode" :@"682"];
    [request add:@"FrameworkInfo" :@"10.0"];
    [request add:@"ItemID" :@"88"];
    [request add:@"MerchantID" :@"0001000002"];
    [request add:@"MessageID" :@"9"];
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSInteger TransactionIDOld = timeInSeconds;

    [request add:@"TransactionID" :[NSString stringWithFormat:@"%ld000",(long)TransactionIDOld]];
    [request add:@"PaymentDescription" :[NSString stringWithFormat:@"ios App Order"]];
    
    if ([self.dataDict[@"PaymentMethod"] intValue]==2)
    {
        [request add:@"SadadOlpId" :[NSString stringWithFormat:@"%@",sadatAccountIDTextField.text]];
        [request add:@"mfu" :@"https://berain.com.sa/oms/order/payment_response"];
        [request add:@"PaymentMethod" :@"2"];

    }
    else
    {
        [request add:@"PaymentMethod" :@"1"];

        NSLog(@"%@",[NSString stringWithFormat:@"%ld",(long)selectedExpireyMonth]);
        NSLog(@"%@",[NSString stringWithFormat:@"%ld",(long)selectedExpireyYear]);
        
        [request add:@"CardHolderName" :[NSString stringWithFormat:@"%@",cardNumberTextfield.text]];
        [request add:@"CardNumber" :[NSString stringWithFormat:@"%@",cardNumberTextfield.text]];
        [request add:@"ExpiryDateMonth" :[NSString stringWithFormat:@"%@",selectedExpireyMonth]];
        [request add:@"ExpiryDateYear" :[NSString stringWithFormat:@"%@",[selectedExpireyYear substringFromIndex: [selectedExpireyYear length] - 2]]];
        [request add:@"SecurityCode" :[NSString stringWithFormat:@"%@",cvvTextfield.text]];

    }
    
    PaymentService *services = [[PaymentService alloc]init];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        PaymentResponse *response = [services process:request];
        NSLog(@"%@",response);
        
        NSString *responseCode = [response get:@"Response.StatusCode"];
        NSString *execCode1 = [response get:@"execCode"];
        NSString *responseHashMatch = [response get:@"ResponseHashMatch"];
        TransactionID=[NSString stringWithFormat:@"%@",[response get:@"Response.TransactionID"]];
        //    && [responseCode intValue]==0
        if ([execCode1 intValue]==0 && responseHashMatch.length>0)
        {
            if ([self.dataDict[@"PaymentMethod"] intValue]==2)
            {
                if ([responseCode intValue]==20002)
                {
                    [sadadWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
                    [sadadViewForWeb setHidden:NO];
                    [self openWebForSadad:response];
                }
                else
                {
                    [self showErrorMessage:[response get:@"Response.StatusDescription"]];
                }

            }
            else
            {
                if ([responseCode intValue]==0)
                {
                    NSMutableDictionary *responseDict=[self createDictFromResponse:response];
                    [self placeOrderUsingCreditCard:formattedDict response:responseDict];
                }
                else
                {
                    [self showErrorMessage:[response get:@"Response.StatusDescription"]];
                }
            }
        }
        else
        {
            [self showErrorMessage:[response get:@"Response.StatusDescription"]];
        }
    });
    }
}
-(void)showErrorMessage :(NSString*)message
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    UIAlertView* alert = [[UIAlertView alloc] init];
    [alert setTitle:NSLocalizedString(@"Berain", @"")];
    [alert setMessage:message];
    [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
    [alert show];

}
-(void)openWebForSadad:(PaymentResponse*)response
{
    authenticationURL = [response get:@"Response.AuthenticationURL"];
    NSString *estn = [response get:@"Response.estn"];
    mfu = [response get:@"Response.mfu"];
    NSString *punNumber2 = [response get:@"Response.TransactionID"];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:punNumber2 forKey:@"PunNum2"];
    [standardUserDefaults synchronize];
    
    sadadWebView.delegate=self;
    NSMutableURLRequest*request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?estn=%@&mfu=%@",authenticationURL,estn,mfu]]];
    
    [sadadWebView loadRequest:request];
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    NSString *webViewResult=sadadWebView.request.URL.absoluteString;
    if (![webViewResult containsString:@"mfu"] && ![webViewResult containsString:@"execution"] && ![webViewResult containsString:@"about:blank"])
    {
        NSArray *compsArray =[webViewResult componentsSeparatedByString:@"="];
        if (compsArray.count>=1)
        {
            NSString*estn=[NSString stringWithFormat:@"%@=",compsArray[1]];
            ApproveRequest *approveRequest=[[ApproveRequest alloc]init];
            [approveRequest setPaymentType:@"smartRoute"]; // smartRoute or ezConnect based on the wanted interface
            [approveRequest isLogging:YES]; // To print Log Statements or not in the console
            [approveRequest setApproveRequestAuthenticationToken:@"AuthenticationToken" valueForKey:@"OWE1Njc0YzQyMDRhODQwNmMyM2IxMmY0"];//Either set
            
            [approveRequest add:@"MerchantID" :@"0001000002"];
            [approveRequest add:@"MessageID" :@"10"];
            [approveRequest add:@"PaymentMethod" :@"2"];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            [approveRequest add:@"TransactionID" :[standardUserDefaults objectForKey:@"PunNum2"]];
            [approveRequest add:@"estn" :estn];
            
            ApproveService *services = [[ApproveService alloc]init];
            ApproveResponse *response = [services process:approveRequest];
            
            NSLog(@"%@",response);
            NSMutableDictionary *formattedDict=[[DataManager sharedManager] formattedDictionary];

            NSString *responseCode = [response get:@"Response.StatusCode"];
            NSString *execCode1 = [response get:@"execCode"];
            NSString *responseHashMatch = [response get:@"ResponseHashMatch"];

            
            if ([execCode1 intValue]==0 && responseHashMatch.length>0 && [responseCode intValue]==0)
            {
                    if ([responseCode intValue]==0)
                    {
                        NSMutableDictionary*respOnseDict= [self createDictFromApproveResponse:response];
                        [self placeOrderUsingCreditCard:formattedDict response:respOnseDict];
                    }
                    else
                    {
                        [self showErrorMessage:[response get:@"Response.StatusDescription"]];
                    }
            }
            else
            {
                [self showErrorMessage:[response get:@"Response.StatusDescription"]];
                [sadadViewForWeb setHidden:YES];
            }

        }

    }
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (IBAction)monthButtonTap:(UIButton *)sender
{
    picker.frame = CGRectMake(0.0, 0.0, picker.frame.size.width, 160.0);
    
    LGAlertView *dateAlertView=[[LGAlertView alloc] initWithViewAndTitle:NSLocalizedString(@"Please Choose Date", @"") message:@"" style:LGAlertViewStyleAlert view:picker buttonTitles:@[NSLocalizedString(@"Done", @"")] cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil delegate:self];
    dateAlertView.tag=1;
    [dateAlertView showAnimated];

}
-(void)placeOrderUsingCreditCard:(NSMutableDictionary*)formattedDict response:(NSDictionary*)responseDict
{
    Address *selectedAddress=self.dataDict[@"address"];
    NSMutableDictionary *addresDict=[NSMutableDictionary dictionary];
    [addresDict setObject:selectedAddress.add_id forKey:@"ord_address_id"];

    float totalPrice = [formattedDict[@"totalPriceWithoutVAT"] floatValue]/100;
    double vat=[[SingletonClass sharedSingletonClass].settingsDictionary[@"vat"] doubleValue];
    float vatOnTotalPrice=(totalPrice*vat)/100;
    
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:currentUser.user_id forKey:@"user_id"];
    [paramDict setObject:self.dataDict[@"PaymentMethod"] forKey:@"payment_type"];
    [paramDict setObject:[NSString stringWithFormat:@"%.02f",vatOnTotalPrice] forKey:@"amount_vat"];
    [paramDict setObject:[NSString stringWithFormat:@"%.02f",totalPrice] forKey:@"price_without_vat"];
    [paramDict setObject:formattedDict[@"formattedArray"] forKey:@"cart"];
    [paramDict setObject:addresDict forKey:@"addressData"];
    [paramDict setObject:responseDict forKey:@"response"];
    
    
    
    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_AddOrder OnCompletion:^(id returnDict, NSError *error) {
        
        if (returnDict[@"id"])
        {
            [[DataManager sharedManager] deleteAllCart];
            
            ThanksViewController* orderDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ThanksViewController"];
            orderDetailViewController.orderID=returnDict[@"id"];
            orderDetailViewController.reeferenceNumber=responseDict[@"TransactionID"];
            [[SlideNavigationController sharedInstance] pushViewController:orderDetailViewController animated:YES];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            [alert setTitle:NSLocalizedString(@"Berain", @"")];
            [alert setMessage:NSLocalizedString(@"Please try again", @"")];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
            [alert show];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
    

}

- (IBAction)proceedSadatButtonTap:(UIButton *)sender
{
    [self proceedButtonTap:sender];
    
//    PaymentRequest *request = [[PaymentRequest alloc]init];
//    [request setPaymentType:@"smartRoute"]; // smartRoute or ezConnect based on the wanted interface
//    [request isLogging:YES]; // To print Log Statements or not in the console
//    [request setPaymentAuthenticationToken:@"AuthenticationToken" valueForKey:@"OWE1Njc0YzQyMDRhODQwNmMyM2IxMmY0"]; //Either set
//    
//    [request add:@"Amount" :@"5000"];
//    [request add:@"Channel" :@"0"];
//    [request add:@"ClientIPaddress" :@"39.51.221.228"];
//    
//    [request add:@"CurrencyISOCode" :@"682"];
//    [request add:@"ExpiryDateMonth" :@"12"];
//    [request add:@"ExpiryDateYear" :@"22"];
//    [request add:@"FrameworkInfo" :@"10.0"];
//    [request add:@"ItemID" :@"88"];
//    [request add:@"MerchantID" :@"0001000002"];
//    [request add:@"MessageID" :@"9"];
//    [request add:@"PaymentMethod" :@"2"];
//    [request add:@"SecurityCode" :@"567"];
//    [request add:@"TransactionID" :@"12345"];
//    [request add:@"PaymentDescription" :@"QA Test"];
//    [request add:@"SadadOlpId" :@"arun123"];
//    [request add:@"mfu" :@"123"];
//    PaymentService *services = [[PaymentService alloc]init];
//    PaymentResponse *response = [services process:request];
//    NSLog(@"%@",response);
//    
//    NSString *responseCode = [response get:@"Response.StatusCode"];
//    NSString *execCode1 = [response get:@"execCode"];
//    NSString *responseHashMatch = [response get:@"ResponseHashMatch"];
}

- (IBAction)cancelSadadView:(UIButton *)sender
{
    [sadadViewForWeb setHidden:YES];
}

-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    if (alertView.tag==1)
    {
        if (index==0)
        {
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            
            if( picker.datePickerMode == NTMonthYearPickerModeMonthAndYear ) {
                [df setDateFormat:@"MMMM yyyy"];
            } else {
                [df setDateFormat:@"yyyy"];
            }
            
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:picker.date];
            selectedExpireyYear = [NSString stringWithFormat:@"%ld",(long)[components year]];
            selectedExpireyMonth = [NSString stringWithFormat:@"%ld",(long)[components month]];
            if (selectedExpireyMonth.length==1)
            {
            selectedExpireyMonth =  [NSString stringWithFormat:@"0%@",selectedExpireyMonth];
            }
            NSString *dateStr = [df stringFromDate:picker.date];
            [monthButton setTitle:dateStr forState:UIControlStateNormal];
            [expiryDateErrorLabel setHidden:YES];
        }
    }
    else
    {
    }
}
- (void)onDatePicked:(UITapGestureRecognizer *)gestureRecognizer
{

}

-(NSMutableDictionary*)createDictFromResponse:(PaymentResponse*)response
{
    NSMutableDictionary*dataDict=[NSMutableDictionary dictionary];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.TransactionID"]] forKey:@"TransactionID"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.MessageID"]] forKey:@"MessageID"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.SecureHash"]] forKey:@"SecureHash"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.CardNumber"]] forKey:@"CardNumber"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.Amount"]] forKey:@"Amount"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.CurrencyISOCode"]] forKey:@"CurrencyISOCode"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.StatusCode"]] forKey:@"StatusCode"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.StatusDescription"]] forKey:@"StatusDescription"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.GatewayStatusCode"]] forKey:@"GatewayStatusCode"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.GatewayStatusDescription"]] forKey:@"GatewayStatusDescription"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.RRN"]] forKey:@"RRN"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.GatewayName"]] forKey:@"GatewayName"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.MerchantID"]] forKey:@"MerchantID"];
    
    return dataDict;
}

-(NSMutableDictionary*)createDictFromApproveResponse:(ApproveResponse*)response
{
    NSMutableDictionary*dataDict=[NSMutableDictionary dictionary];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.TransactionID"]] forKey:@"TransactionID"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.MessageID"]] forKey:@"MessageID"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.SecureHash"]] forKey:@"SecureHash"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.Amount"]] forKey:@"Amount"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.CurrencyISOCode"]] forKey:@"CurrencyISOCode"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.StatusCode"]] forKey:@"StatusCode"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.StatusDescription"]] forKey:@"StatusDescription"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.GatewayStatusCode"]] forKey:@"GatewayStatusCode"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.GatewayStatusDescription"]] forKey:@"GatewayStatusDescription"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.RRN"]] forKey:@"RRN"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.GatewayName"]] forKey:@"GatewayName"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",[response get:@"Response.MerchantID"]] forKey:@"MerchantID"];
    
    return dataDict;
}
-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
    for (int i = 0; i < [string length]; i++)
    {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c])
        {
            return NO;
        }
        if ([string rangeOfCharacterFromSet:myCharSet].location == NSNotFound)
        {
            // valid
        } else {
            // invalid
            return NO;

        }

    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    int limit=0;
    if (textField==cardNumberTextfield)
    {
        limit=16;
    }
    if (textField==cvvTextfield)
    {
        limit=3;
    }
    if (textField==cardHolderNameTextfield)
    {
        limit=0;
    }


    if (newLength>0)
    {
        [self changeTextFields:textField hidden:YES];
    }
    else
    {
//        [self changeTextFields:textField hidden:NO];
    }
    if (newLength>limit)
    {
        if (textField==cardHolderNameTextfield || textField==sadatAccountIDTextField)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return YES;
}

-(void)changeTextFields:(UITextField *)textField hidden:(BOOL)hide
{
    if (textField == cardHolderNameTextfield)
    {
        [cardholderNameErrorLabel setHidden:hide];
//        [cardHolderNameTextfield setPlaceholder:@"Card holder name"];
    }
    else if (textField == cvvTextfield)
    {
        [cvvErrorLabel setHidden:hide];
//        [cvvTextfield setPlaceholder:@"CVV number"];
    }
    else if (textField == cardNumberTextfield)
    {
        [cardNumberErrorLabel setHidden:hide];
//        [cardNumberTextfield setPlaceholder:@"card number"];
    }
    else if (textField == sadatAccountIDTextField)
    {
        [sadadErrorLabel setHidden:hide];
//        [sadatAccountIDTextField setPlaceholder:@"Sadad Account ID"];
    }

}
-(BOOL)checkValidInfo
{
    BOOL allOK=YES;
    
    if ([cardHolderNameTextfield.text length]==0)
    {
        [cardholderNameErrorLabel setHidden:NO];
//        [cardHolderNameTextfield setPlaceholder:@"Card holder name"];
        allOK=NO;
    }
    if ([cvvTextfield.text length]<2)
    {
        [cvvErrorLabel setHidden:NO];
//        [cvvTextfield setPlaceholder:@"CVV number"];
        allOK=NO;
    }
    if ([cardNumberTextfield.text length]<15)
    {
        [cardNumberErrorLabel setHidden:NO];
//        [cardNumberTextfield setPlaceholder:@"card number"];
        allOK=NO;
    }
    if (sadatView.hidden==NO)
    {
        if ([sadatAccountIDTextField.text length]==0)
        {
            [sadadErrorLabel setHidden:NO];
//            [sadatAccountIDTextField setPlaceholder:@"SADAD Account ID"];
            allOK=NO;
        }
        else
        {
            allOK=YES;
        }
    }
    else
    {
        if ([monthButton.titleLabel.text isEqualToString:@"Date"])
        {
            [expiryDateErrorLabel setHidden:NO];
            allOK=NO;

        }
        if (selectedExpireyYear.length>0 && selectedExpireyMonth.length>0)
        {
            
        }
        else
        {
            allOK=NO;

            [expiryDateErrorLabel setHidden:NO];
        }
    }
    return allOK;
}


@end
