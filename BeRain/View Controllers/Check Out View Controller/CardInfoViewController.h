//
//  CardInfoViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/21/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"
#import "NTMonthYearPicker.h"
#import <STSPaymentLibrary/STSPaymentLibrary.h>

@interface CardInfoViewController : BaseViewController
{
    User *currentUser;

    IBOutlet UIView *creditCardView;
    IBOutlet UIView *sadatView;
    IBOutlet UILabel *navigationTitleLabel;
    NTMonthYearPicker *picker;

    IBOutlet UITextField *cardHolderNameTextfield;
    IBOutlet UITextField *cvvTextfield;
    IBOutlet UITextField *cardNumberTextfield;
    IBOutlet UITextField *sadatAccountIDTextField;
    IBOutlet UIButton *monthButton;
    IBOutlet UIButton *proceedButton;
    IBOutlet UIButton *proceedSadatButton;
    
    IBOutletCollection(UIImageView) NSArray *textFieldBG;

    NSString *selectedExpireyMonth;
    NSString * selectedExpireyYear;
    
    IBOutlet UIView *sadadViewForWeb;
    IBOutlet UIWebView *sadadWebView;
    NSString *authenticationURL;
    NSString *mfu;
    NSString *TransactionID;
    
    IBOutlet UILabel *cardholderNameErrorLabel;
    IBOutlet UILabel *cvvErrorLabel;
    IBOutlet UILabel *cardNumberErrorLabel;
    IBOutlet UILabel *expiryDateErrorLabel;
    IBOutlet UILabel *sadadErrorLabel;
    
    IBOutlet UILabel *totalAmountLabel;

}
@property (strong, nonatomic) NSMutableDictionary *dataDict;

- (IBAction)proceedButtonTap:(UIButton *)sender;
- (IBAction)monthButtonTap:(UIButton *)sender;
- (IBAction)proceedSadatButtonTap:(UIButton *)sender;
- (IBAction)cancelSadadView:(UIButton *)sender;


@end
