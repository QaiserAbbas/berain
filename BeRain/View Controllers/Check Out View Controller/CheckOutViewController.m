//
//  CheckOutViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/7/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "CheckOutViewController.h"
#import "CardInfoViewController.h"
#import "TotalPriceTableViewCell.h"
#import "CheckOutTableViewCell.h"
#import "ThanksViewController.h"

@interface CheckOutViewController ()<LGAlertViewDelegate,UITextViewDelegate>

@end

@implementation CheckOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];

    for (UIImageView*imageView in bgImageViews)
    {
        imageView.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
        imageView.layer.cornerRadius=5;
        imageView.layer.borderWidth=1.0;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    }
    
    for (UIImageView*imageView in bgWhiteImageViews)
    {
        imageView.layer.backgroundColor=[[UIColor whiteColor] CGColor];
        imageView.layer.cornerRadius=5;
        imageView.layer.borderWidth=1.0;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    }

    placeOrderButton.layer.cornerRadius=placeOrderButton.frame.size.height/2;
    placeOrderButton.layer.masksToBounds = YES;

    radioImage=[UIImage imageNamed:@"radio"];
    radioCheckImage=[UIImage imageNamed:@"radio-check"];

    hoursArray=[NSMutableArray arrayWithObjects:@"9-12",@"12-3",@"3-6",@"6-9", nil];

//    [self sadatButtonTap:nil];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *today=[dateFormat stringFromDate:[NSDate date]];
    [dateButton setTitle:today forState:UIControlStateNormal];
    [hoursButton setTitle:hoursArray[0] forState:UIControlStateNormal];

    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
//    [self setPriceInfo];
    [self checkVisibilityOfPaymentMethods];
    
    NSMutableDictionary *formattedDict=[[DataManager sharedManager] formattedDictionary];
    NSString *str=[NSString stringWithFormat:@"Total Payable amount : "];
    double amount = [formattedDict[@"totalPrice"] doubleValue]/100;
    
    NSString *localStr=[NSString stringWithFormat:@"%@%.02f",NSLocalizedString(str, @""),amount];
    totalAmountLabel.text=localStr;
}

-(void)checkVisibilityOfPaymentMethods
{
    int creditCard=8;
    int homdeDelivery=8;
    BOOL sadat=NO;
    BOOL credit =NO;
    BOOL cash =NO;
    
    if ([[SingletonClass sharedSingletonClass].settingsDictionary[@"is_sadad_enable"] boolValue])
    {
        [sadadView setHidden:NO];
        sadat=YES;
    }
    else
    {
        [sadadView setHidden:YES];
        creditCard=-42;
//        homdeDelivery=-58;
    }
    if ([[SingletonClass sharedSingletonClass].settingsDictionary[@"is_credit_card_enable"] boolValue])
    {
        credit=YES;
        [creditCardView setHidden:NO];
    }
    else
    {
        [creditCardView setHidden:YES];
        homdeDelivery=-42;
    }

    if ([[SingletonClass sharedSingletonClass].settingsDictionary[@"is_cash_delivery_enable"] boolValue])
    {
        [homeDevliveryView setHidden:NO];
        cash=YES;
    }
    else
    {
        [homeDevliveryView setHidden:YES];
    }
    if (sadat)
    {
        [self sadatButtonTap:nil];
    }
    else if (!sadat && cash)
    {
        [self cashOnDeliveryButtonTa:nil];
    }
    else if (!sadat && credit && !cash)
    {
        [self creditCardButtontap:nil];
    }
    homeDeliveryUpperConstraint.constant=creditCard;
    creditCardUpperConstraint.constant=homdeDelivery;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dateButtonTap:(UIButton *)sender
{
    datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.frame = CGRectMake(0.0, 0.0, datePicker.frame.size.width, 160.0);
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setMonth:+2];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:minDate];
    [datePicker setMinimumDate:currentDate];

    LGAlertView *dateAlertView=[[LGAlertView alloc] initWithViewAndTitle:NSLocalizedString(@"Please Choose Date", @"") message:@"" style:LGAlertViewStyleAlert view:datePicker buttonTitles:@[NSLocalizedString(@"Done", @"")] cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil delegate:self];
    dateAlertView.tag=1;
    [dateAlertView showAnimated];
}

- (IBAction)hoursButtonTap:(UIButton *)sender
{
    NSMutableArray *imagesArray=[NSMutableArray array];
    
    for (int i=0; i<hoursArray.count; i++)
    {
        if (i==slectedindex)
        {
            [imagesArray addObject:radioCheckImage];
        }
        else
        {
            [imagesArray addObject:radioImage];
        }
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Select City", @"")
                                                        message:@""
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:hoursArray
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    alertView.buttonsIconImages = imagesArray;
    alertView.buttonsIconImagesHighlighted = imagesArray;
    alertView.buttonsIconPosition = LGAlertViewButtonIconPositionRight;
    if ([SettingsClass isLanguageArabic])
    {
        alertView.buttonsTextAlignment = NSTextAlignmentRight;
    }
    else
    {
        alertView.buttonsTextAlignment = NSTextAlignmentLeft;
    }
    
    alertView.cancelButtonTextAlignment = NSTextAlignmentCenter;
    alertView.tag=2;
    [alertView showAnimated:YES completionHandler:nil];

}

- (IBAction)cashOnDeliveryButtonTa:(UIButton *)sender
{
    [radioCash setImage:radioCheckImage];
    [radioCredit setImage:radioImage];
    [radioSadat setImage:radioImage];
    [placeOrderButton setTitle:NSLocalizedString(@"Place Order", @"") forState:UIControlStateNormal];
    [self.dataDict setObject:@"0" forKey:@"PaymentMethod"];

}

- (IBAction)creditCardButtontap:(UIButton *)sender
{
    [radioCredit setImage:radioCheckImage];
    [radioCash setImage:radioImage];
    [radioSadat setImage:radioImage];
    [placeOrderButton setTitle:NSLocalizedString(@"Proceed", @"") forState:UIControlStateNormal];
    [self.dataDict setObject:@"1" forKey:@"PaymentMethod"];



}
- (IBAction)sadatButtonTap:(UIButton *)sender
{
    [radioCredit setImage:radioImage];
    [radioCash setImage:radioImage];
    [radioSadat setImage:radioCheckImage];
    [placeOrderButton setTitle:NSLocalizedString(@"Proceed", @"") forState:UIControlStateNormal];
    [self.dataDict setObject:@"2" forKey:@"PaymentMethod"];

    
}

- (IBAction)placeOrderButtonTap:(UIButton *)sender
{
    if ([self.dataDict[@"PaymentMethod"] intValue]==0)
    {
        [self placeOrderForHomeDelivery];
        return;
    }
    else if ([self.dataDict[@"PaymentMethod"] intValue]==1)
    {
        
    }
    else if ([self.dataDict[@"PaymentMethod"] intValue]==2)
    {
        
    }
    NSLog(@"%@",self.dataDict[@"PaymentMethod"]);
    
    CardInfoViewController* cardInfoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CardInfoViewController"];
    cardInfoViewController.dataDict=[NSMutableDictionary dictionaryWithDictionary:self.dataDict];
    
    [[SlideNavigationController sharedInstance] pushViewController:cardInfoViewController animated:YES];
}

-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    if (alertView.tag==1)
    {
        if (index==0)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSString *today=[dateFormat stringFromDate:datePicker.date];
            [dateButton setTitle:today forState:UIControlStateNormal];
        }
    }
    else
    {
        slectedindex=index;
        [hoursButton setTitle:hoursArray[slectedindex] forState:UIControlStateNormal];
    }
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==cartArray.count)
    {
        return 180;
    }
    else
    {
        return UITableViewAutomaticDimension;
    }
    return UITableViewAutomaticDimension;
}

#pragma mark - UITableView Delegates
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.0;
}

-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    if (cartArray.count>0)
    {
        return cartArray.count+1;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.row==cartArray.count)
    {
        static NSString* Identifier = @"TotalPriceTableViewCell";
        
        TotalPriceTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if( cell == nil )
        {
            NSString* cellName = @"TotalPriceTableViewCell";
            if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                //                cellName = @"LiveEventsTableViewCell-iPad";
            }
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell setdataCell:cartArray];
        return cell;
    }
    else
    {
        static NSString* Identifier = @"CheckOutTableViewCell";
        
        CheckOutTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if( cell == nil )
        {
            NSString* cellName = @"CheckOutTableViewCell";
            if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                //                cellName = @"LiveEventsTableViewCell-iPad";
            }
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell setdataCell:cartArray[indexPath.row]];
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)placeOrderForHomeDelivery
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    Address *selectedAddress=self.dataDict[@"address"];
    NSMutableDictionary *addresDict=[NSMutableDictionary dictionary];
    [addresDict setObject:selectedAddress.add_id forKey:@"ord_address_id"];
    NSMutableDictionary *formattedDict=[[DataManager sharedManager] formattedDictionary];
    
    
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        [paramDict setObject:currentUser.user_id forKey:@"user_id"];
//        [paramDict setObject:self.dataDict[@"PaymentMethod"] forKey:@"payment_type"];
        [paramDict setObject:formattedDict[@"formattedArray"] forKey:@"cart"];
        [paramDict setObject:addresDict forKey:@"addressData"];
    
        
        
        [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_AddOrderDirectly OnCompletion:^(id returnDict, NSError *error) {
            
            if (returnDict[@"id"])
            {
                [[DataManager sharedManager] deleteAllCart];
                
                ThanksViewController* orderDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ThanksViewController"];
                orderDetailViewController.orderID=returnDict[@"id"];
                [[SlideNavigationController sharedInstance] pushViewController:orderDetailViewController animated:YES];
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }];
    
}

-(void)setPriceInfo
{
    double vat=[[SingletonClass sharedSingletonClass].settingsDictionary[@"vat"] doubleValue];
    
    if (cartArray.count>0)
    {
        int totalItems=0;
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            totalItems= totalItems+[cart.quantity intValue];
        }
        [amountLabel setText:[NSString stringWithFormat:@"%.02f %@",totalPrice,NSLocalizedString(@"SR", @"")]];
        [totalAmountLabe setText:[NSString stringWithFormat:@"%.02f %@",totalPrice+(totalPrice*vat)/100,NSLocalizedString(@"SR", @"")]];
        [vatLabel setText:[NSString stringWithFormat:@"%.02f %@",(totalPrice*vat)/100,NSLocalizedString(@"SR", @"")]];

        if ([SettingsClass isLanguageArabic])
        {
            [vatQuanitity setText:[NSString stringWithFormat:@"%@ %% %.02f ",NSLocalizedString(@"VAT", @""),vat]];
        }
        else
        {
            [vatQuanitity setText:[NSString stringWithFormat:@"%@ %.02f %%",NSLocalizedString(@"VAT", @""),vat]];
        }
    }
    else
    {
        [totalAmountLabe setText:@"0.0 SR"];
        [amountLabel setText:@"0.0 SR"];
        [vatLabel setText:@"0.0 SR"];
    }
}




@end
