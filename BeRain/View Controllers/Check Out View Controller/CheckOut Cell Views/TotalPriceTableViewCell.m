
//
//  TotalPriceTableViewCell.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "TotalPriceTableViewCell.h"
#import "Cart+CoreDataClass.h"

@implementation TotalPriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void)setdataCell:(NSMutableArray*)cartArray;
{
    if (cartArray.count>0)
    {
        int totalItems=0;
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            totalItems= totalItems+[cart.quantity intValue];
        }
        [self.totalLabel setText:[NSString stringWithFormat:@"%.02f %@",totalPrice,NSLocalizedString(@"SR", @"")]];
    }
    else
    {
    }
    [self.cashOndeliveryLabel setText:[NSString stringWithFormat:@"00 SR"]];
    [self.homeDeliveryLabel setText:[NSString stringWithFormat:@"00 SR"]];
}


@end
