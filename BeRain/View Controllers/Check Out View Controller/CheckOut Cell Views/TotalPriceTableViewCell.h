//
//  TotalPriceTableViewCell.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalPriceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *cashOndeliveryLabel;
@property (strong, nonatomic) IBOutlet UILabel *homeDeliveryLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;

-(void)setdataCell:(NSMutableArray*)cartArray;

@end
