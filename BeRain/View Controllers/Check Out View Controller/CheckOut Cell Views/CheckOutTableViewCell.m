//
//  CheckOutTableViewCell.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "CheckOutTableViewCell.h"
#import "Cart+CoreDataClass.h"

@implementation CheckOutTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setdataCell:(id)object;
{
    Cart *cart=(Cart*)object;
    
    [self.productName setText:cart.dish_title_en];
    double total = [cart.quantity doubleValue]*[cart.dish_price doubleValue];
    
    [self.productPrice setText:[NSString stringWithFormat:@"%@X%@  %.02f %@",cart.quantity ,cart.dish_price,total,NSLocalizedString(@"SR", @"")]];
    [self.productImageView setShowActivityIndicatorView:YES];
    [self.productImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [self.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://berain.com.sa/oms/mob_app/resources/uploads/files/small/%@",cart.dish_image]] placeholderImage:[UIImage imageNamed:@"usericon"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.productImageView setShowActivityIndicatorView:NO];
    }];
}

@end
