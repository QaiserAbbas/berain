//
//  SideMenuViewController.h
//  TrueLie
//
//  Created by Qaiser Abbas on 26/11/2015.
//  Copyright © 2015 Bir Al Sabia. All rights reserved.
//

#import "BaseViewController.h"
#import "SingletonClass.h"


@interface SideMenuViewController : BaseViewController
{
    User *currentUser;
     IBOutlet UIImageView *ProfileImage;
     IBOutlet UILabel *userName;
    IBOutlet UIView *mediaCenterView;
    IBOutlet UIView *addressView;
    IBOutlet NSLayoutConstraint *shareButtononstraint;
    IBOutlet NSLayoutConstraint *aboutUpperContraint;
    IBOutlet UIButton *signInButton;
    IBOutlet UIButton *languageButton;
    IBOutlet UIButton *editButton;
    IBOutlet UILabel *userNameLabel;
    IBOutlet UIScrollView *sideMenuScroll;
    
}
-(void)updateProfile;
- (IBAction)homeButtonTap:(UIButton *)sender;
- (IBAction)aboutBerainButtonTap:(UIButton *)sender;
- (IBAction)whyBerainButtonTap:(UIButton *)sender;
- (IBAction)careerButtonTap:(UIButton *)sender;
- (IBAction)contactUsButtonTap:(UIButton *)sender;
- (IBAction)mediaCenterButtonTap:(UIButton *)sender;
- (IBAction)shareButtonTap:(UIButton *)sender;
- (IBAction)addressButtonTap:(UIButton *)sender;
- (IBAction)myOrdersButtonTap:(UIButton *)sender;

- (IBAction)logOutButtonTap:(UIButton *)sender;
- (IBAction)languageButtonTap:(UIButton *)sender;
- (IBAction)editButtonTap:(UIButton *)sender;

- (IBAction)newsButtonTap:(UIButton *)sender;
- (IBAction)socialMediaButtonTap:(UIButton *)sender;
- (IBAction)faqMediaButtonTap:(UIButton *)sender;

@end
