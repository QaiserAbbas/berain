//
//  SideMenuViewController.m
//  TrueLie
//
//  Created by Qaiser Abbas on 26/11/2015.
//  Copyright © 2015 Bir Al Sabia. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "AddressViewController.h"
#import "MyOrdersViewController.h"
#import "LanguageManager.h"
#import "AppDelegate.h"
#import "EditProfileViewController.h"
#import "StaticWebViewViewController.h"
#import "ShareViewController.h"

@interface SideMenuViewController ()
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.slideOutAnimationEnabled = YES;
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];
    
    if (currentUser)
    {
        [addressView setHidden:NO];
        aboutUpperContraint.constant=95;
        [signInButton setTitle:NSLocalizedString(@"Sign Out", @"") forState:UIControlStateNormal];
    }
    else
    {
        [addressView setHidden:YES];
        aboutUpperContraint.constant=0;
        [signInButton setTitle:NSLocalizedString(@"Sign In", @"") forState:UIControlStateNormal];
    }
    
    [sideMenuScroll setContentSize:CGSizeMake(188, 600)];
    sideMenuScroll.contentInset=UIEdgeInsetsMake(4.0,0.0,50,0.0);

    if ([SettingsClass isLanguageArabic])
    {
        languageButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -10);
    }
    else
    {
        languageButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{

}
-(void)updateProfile
{
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];
    
    if (currentUser)
    {
        [addressView setHidden:NO];
        aboutUpperContraint.constant=95;
        [signInButton setTitle:NSLocalizedString(@"Sign Out", @"") forState:UIControlStateNormal];
        [userNameLabel setHidden:NO];
        [editButton setHidden:NO];
        [userNameLabel setText:[NSString stringWithFormat:@"%@ %@",currentUser.frist_name,currentUser.last_name]];
    }
    else
    {
        [addressView setHidden:YES];
        aboutUpperContraint.constant=0;
        [signInButton setTitle:NSLocalizedString(@"Sign In", @"") forState:UIControlStateNormal];
        [userNameLabel setHidden:YES];
        [editButton setHidden:YES];
    }
}

- (IBAction)homeButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[HomeViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        HomeViewController* homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:homeViewController animated:YES];
        
    }

}

- (IBAction)aboutBerainButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"About Berain"];
}

- (IBAction)whyBerainButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"Why Berain?"];
}

- (IBAction)careerButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"Career"];
}

- (IBAction)contactUsButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"Contact Us"];
}

- (IBAction)newsButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"News"];
}

- (IBAction)socialMediaButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"Social Media"];
}

- (IBAction)faqMediaButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"FAQ"];
}


- (IBAction)mediaCenterButtonTap:(UIButton *)sender
{
    if (mediaCenterView.isHidden)
    {
        [mediaCenterView setHidden:NO];
        shareButtononstraint.constant=115;
    }
    else
    {
        [mediaCenterView setHidden:YES];
        shareButtononstraint.constant=5;
    }
}

- (IBAction)shareButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[ShareViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        ShareViewController* shareViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:shareViewController animated:YES];
        
    }
}

- (IBAction)addressButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[AddressViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        AddressViewController* addressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddressViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:addressViewController animated:YES];
        
    }

}

- (IBAction)myOrdersButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[MyOrdersViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        MyOrdersViewController* myOrdersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyOrdersViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:myOrdersViewController animated:YES];
        
    }

}

- (IBAction)contactUs:(UIButton *)sender
{
}

//    TermsPrivacyViewController *termsPrivacyViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"TermsPrivacyViewController"];
//    termsPrivacyViewController.viewType=@"terms";
//    [[SlideNavigationController sharedInstance] pushViewController:termsPrivacyViewController animated:YES];

- (IBAction)logOutButtonTap:(UIButton *)sender;
{
    [[DataManager sharedManager] removeUser];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"User"];
    [[SingletonClass sharedSingletonClass].settingsDictionary removeObjectForKey:@"User"];
    [SettingsClass RemoveAuth_key];
    
//    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];

    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[LoginViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        LoginViewController* loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:loginViewController animated:YES];
        
    }

}

- (IBAction)languageButtonTap:(UIButton *)sender
{
    if ([[LanguageManager currentLanguageCode] isEqualToString:@"ar"])
    {
        [sender setTitle:@" عربی" forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setObject:@"English" forKey:@"Language"];
        [LanguageManager saveLanguageByIndex:0];
        [self reloadRootViewController];
    }
    else
    {
        [sender setTitle:@"English" forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setObject:@"Arabic" forKey:@"Language"];
        [LanguageManager saveLanguageByIndex:1];
        [self reloadRootViewController];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)editButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[EditProfileViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        EditProfileViewController* editProfileViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:editProfileViewController animated:YES];
        
    }

}
- (void)reloadRootViewController
{
    [LanguageManager setupCurrentLanguage];

    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    delegate.window.rootViewController = [storyboard instantiateInitialViewController];
    [delegate changeLanguageMainStoryBoard];
}

-(void)manageStaticWebView:(NSString*)viewName
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[StaticWebViewViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                StaticWebViewViewController *staticWebViewViewController=(StaticWebViewViewController*)aViewController;
                staticWebViewViewController.viewName=viewName;
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        StaticWebViewViewController* staticWebViewViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"StaticWebViewViewController"];
        staticWebViewViewController.viewName=viewName;
        [[SlideNavigationController sharedInstance] pushViewController:staticWebViewViewController animated:YES];
    }

}

@end
