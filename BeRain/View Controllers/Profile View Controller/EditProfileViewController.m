//
//  EditProfileViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/23/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for (UIImageView*imageView in textFieldBG)
    {
        imageView.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
        imageView.layer.cornerRadius=5;
        imageView.layer.borderWidth=1.0;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderColor=[[UIColor purpleColor] CGColor];
    }
    saveButton.layer.cornerRadius=saveButton.frame.size.height/2;
    saveButton.layer.masksToBounds = YES;

    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];
    
    if (currentUser)
    {
        [firstNameTextField setText:currentUser.frist_name];
        [lastNameTextField setText:currentUser.last_name];
        [mobileTextField setText:currentUser.mobile];
        [emailTextField setText:currentUser.email];
    }
    
    if ([SettingsClass isLanguageArabic])
    {
        [firstNameTextField setTextAlignment:NSTextAlignmentRight];
        [lastNameTextField setTextAlignment:NSTextAlignmentRight];
        [emailTextField setTextAlignment:NSTextAlignmentRight];
        [passwordTextField setTextAlignment:NSTextAlignmentRight];
        [mobileTextField setTextAlignment:NSTextAlignmentRight];
        [confirmPasswordTextField setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [firstNameTextField setTextAlignment:NSTextAlignmentLeft];
        [lastNameTextField setTextAlignment:NSTextAlignmentLeft];
        [emailTextField setTextAlignment:NSTextAlignmentLeft];
        [passwordTextField setTextAlignment:NSTextAlignmentLeft];
        [mobileTextField setTextAlignment:NSTextAlignmentLeft];
        [confirmPasswordTextField setTextAlignment:NSTextAlignmentLeft];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark KeyBoardUp2


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (BOOL)textFieldShouldEndEditing:(UITextField*)textField
{
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
//    [self animateTextField:textField up:NO];
    [textField resignFirstResponder];
    
    return YES;
}


-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField!=passwordTextField || textField!=confirmPasswordTextField)
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c])
            {
                return NO;
            }
        } 
    }
    
    
//    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    int limit=0;
    if (textField==mobileTextField)
    {
        limit=9;
    }
    if (newLength>limit)
    {
        [self changeTextFields:textField hidden:YES];
    }
    else
    {
        //        [self changeTextFields:textField hidden:NO];
    }

    if (textField==mobileTextField)
    {
        if (newLength>13)
        {
            return NO;
        }
        else
        {
            return YES;
        }

    }

    return YES;
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait){
        
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
        if(up) {
            int moveUpValue = self.view.frame.size.height-temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(self.view.frame.size.height-moveUpValue-100);
        }
    }
    else
    {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(768-moveUpValue-100);
        }
        
    }
    if(animatedDis>0)
    {
        const int movementDistance = animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        if (orientation == UIInterfaceOrientationPortrait){
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationLandscapeLeft) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else {
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        
        [UIView commitAnimations];
    }
}


-(void)changeTextFields:(UITextField *)textField hidden:(BOOL)hide
{
    if (textField == firstNameTextField)
    {
        [firstNameLabel setHidden:hide];
//        [firstNameTextField setPlaceholder:@"First Name"];
    }
    else if (textField == lastNameTextField)
    {
        [lastNameLabel setHidden:hide];
//        [lastNameTextField setPlaceholder:@"Last Name"];
    }
    else if (textField == mobileTextField)
    {
        [mobileLabel setHidden:hide];
//        [mobileTextField setPlaceholder:@"Company"];
    }
    else if (textField == emailTextField)
    {
        [emailLabel setHidden:hide];
//        [emailTextField setPlaceholder:@"Email"];
    }
    else if (textField == passwordTextField)
    {
        [passwordLabel setHidden:hide];
//        [passwordTextField setPlaceholder:@"Password"];
    }
    else if (textField == confirmPasswordTextField)
    {
        [confirmLabel setHidden:hide];
//        [confirmPasswordTextField setPlaceholder:@"Confirm password"];
    }

}
-(BOOL)checkValidInfo
{
    BOOL allOK=YES;
    
    if ([firstNameTextField.text length]==0)
    {
        [firstNameLabel setHidden:NO];
//        [firstNameTextField setPlaceholder:@"First Name"];
        allOK=NO;
    }
    if ([lastNameTextField.text length]==0)
    {
        [lastNameLabel setHidden:NO];
//        [lastNameTextField setPlaceholder:@"Last Name"];
        allOK=NO;
    }
    if ([mobileTextField.text length]<10)
    {
        [mobileLabel setHidden:NO];
//        [mobileTextField setPlaceholder:@"Mobile"];
        allOK=NO;
    }
    if ([emailTextField.text length]==0)
    {
        [emailLabel setHidden:NO];
//        [emailTextField setPlaceholder:@"E-mail"];
        allOK=NO;
    }
    else
    {
        if (![SettingsClass isValidEmail:emailTextField.text])
        {
            [emailLabel setHidden:NO];
            allOK=NO;
        }
    }
    if ([passwordTextField.text length]>0)
    {
        if (![passwordTextField.text isEqualToString:confirmPasswordTextField.text])
        {
            [passwordLabel setHidden:NO];
            allOK=NO;
        }
    }
//    if ([confirmPasswordTextField.text length]==0)
//    {
//        [confirmLabel setHidden:NO];
////        [confirmPasswordTextField setPlaceholder:@"Confirm password"];
//        allOK=NO;
//    }

    
    return allOK;
}

- (IBAction)saveButtonTap:(UIButton *)sender;
{
    if ([self checkValidInfo])
    {
        [self.view endEditing:YES];
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
            [paramDict setObject:currentUser.user_id forKey:@"user_id"];
            [paramDict setObject:firstNameTextField.text forKey:@"frist_name"];
            [paramDict setObject:lastNameTextField.text forKey:@"last_name"];
            [paramDict setObject:emailTextField.text forKey:@"email"];
            [paramDict setObject:mobileTextField.text forKey:@"mobile"];
            if ([passwordTextField.text length]>0)
            {
                if ([passwordTextField.text isEqualToString:confirmPasswordTextField.text])
                {
                    [paramDict setObject:passwordTextField.text forKey:@"pass"];
                }
                else
                {
                    
                }
            }
            
            
            [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_UpdateProfile OnCompletion:^(id returnDict, NSError *error) {
                
                if (returnDict[@"rows"])
                {
                    [[DataManager sharedManager] saveUser:returnDict[@"rows"]];
                    [[DataManager sharedManager] saveAddress:returnDict[@"address"]];
                    [[DataManager sharedManager] makeDefaultAddress:currentUser.user_id];

                    User* user = [[DataManager sharedManager] getUser];
                    if (user)
                    {
                        [[SingletonClass sharedSingletonClass].settingsDictionary setObject:user forKey:@"User"];
                    }
                    UIAlertView* alert = [[UIAlertView alloc] init];
                    [alert setTitle:NSLocalizedString(@"Berain", @"")];
                    [alert setMessage:NSLocalizedString(@"Your data has been saved successfully", @"")];
                    [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                    [alert show];

                }
                else
                {
                    UIAlertView* alert = [[UIAlertView alloc] init];
                    [alert setTitle:NSLocalizedString(@"Berain", @"")];
                    [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                    [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                    [alert show];
                }
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
            }];
    }
}

@end
