//
//  EditProfileViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/23/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface EditProfileViewController : BaseViewController
{
    User *currentUser;
    CGFloat animatedDis;

    IBOutletCollection(UIImageView) NSArray *textFieldBG;
    IBOutlet UIButton *saveButton;

    IBOutlet UITextField *firstNameTextField;
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UITextField *mobileTextField;
    IBOutlet UITextField *confirmPasswordTextField;
    
    IBOutlet UILabel *firstNameLabel;
    IBOutlet UILabel *lastNameLabel;
    IBOutlet UILabel *emailLabel;
    IBOutlet UILabel *passwordLabel;
    IBOutlet UILabel *mobileLabel;
    IBOutlet UILabel *confirmLabel;
}

- (IBAction)saveButtonTap:(UIButton *)sender;

@end
