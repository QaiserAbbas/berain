//
//  ShoppingCartViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/3/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "ShoppingCartTableViewCell.h"
#import "PriceTableViewCell.h"
#import "AddressViewController.h"
#import "LoginViewController.h"
#import "HomeViewController.h"

@interface ShoppingCartViewController ()

@end

@implementation ShoppingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
    delAll=NO;
    okButton.layer.cornerRadius=okButton.frame.size.height/2;
    okButton.layer.masksToBounds = YES;

    cancelButton.layer.cornerRadius=cancelButton.frame.size.height/2;
    cancelButton.layer.masksToBounds = YES;

    if ([self.viewType isEqualToString:@"login"])
    {
        [backButton setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==cartArray.count)
    {
        return 200;
    }
    else
    {
        return 110;
    }
    return 110;
}
-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    if (cartArray.count>0)
    {
        return cartArray.count+1;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.row==cartArray.count)
    {
        static NSString* Identifier = @"PriceTableViewCell";
        
        PriceTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if( cell == nil )
        {
            NSString* cellName = @"PriceTableViewCell";
            if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                //                cellName = @"LiveEventsTableViewCell-iPad";
            }
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell.emptyCartButton addTarget:self action:@selector(deleteAllButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.homeButton addTarget:self action:@selector(homeButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.checkOutButton addTarget:self action:@selector(checkoutButtonTap:) forControlEvents:UIControlEventTouchUpInside];
//        [cell.deleteButton setTag:indexPath.row];
//        [cell.plusButton setTag:indexPath.row];
//        [cell.minusButton setTag:indexPath.row];
//        
//        [cell setdataCell:cartArray[indexPath.row]];
        [self changeCartButtons:cell];
        return cell;
    }
    else
    {
        static NSString* Identifier = @"ShoppingCartTableViewCell";
        
        ShoppingCartTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if( cell == nil )
        {
            NSString* cellName = @"ShoppingCartTableViewCell";
            if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                //                cellName = @"LiveEventsTableViewCell-iPad";
            }
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell.deleteButton addTarget:self action:@selector(deleteButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.plusButton addTarget:self action:@selector(plusButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.minusButton addTarget:self action:@selector(minusButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.deleteButton setTag:indexPath.row];
        [cell.plusButton setTag:indexPath.row];
        [cell.minusButton setTag:indexPath.row];
        
        [cell setdataCell:cartArray[indexPath.row]];
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)menuButtonTap:(UIButton *)sender
{
    [[SlideNavigationController sharedInstance] toggleMenu:[SettingsClass OpenManuWithNumber] withCompletion:nil];
}
-(void)deleteButtonTap:(UIButton*)sender
{
    currentProduct=cartArray[sender.tag];

    delAll = NO;
    [messageLabel setText:NSLocalizedString(@"Do you want to remove this product from cart", @"")];
    [popUpView setHidden:NO];

//    [[DataManager sharedManager] deleteCartOjectCompletely:cartArray[sender.tag]];
//    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
//    [cartTableView reloadData];
}
-(void)plusButtonTap:(UIButton*)sender
{
    [self changeCart:sender stat:YES];
}
-(void)minusButtonTap:(UIButton*)sender
{
    [self changeCart:sender stat:NO];
}
-(void)deleteAllButtonTap:(UIButton*)sender
{
    delAll = YES;
    [messageLabel setText:NSLocalizedString(@"Do you want to clear the shoping cart", @"")];
    [popUpView setHidden:NO];
}
-(void)checkoutButtonTap:(UIButton*)sender
{
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];
    
    if (currentUser)
    {
        if ([self totalNumberOfItems] < [[SingletonClass sharedSingletonClass].settingsDictionary[@"min_order"] intValue] )
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            [alert setTitle:NSLocalizedString(@"Berain", @"")];
            [alert setMessage:[NSString stringWithFormat:@"%@ %@ %@/%@",NSLocalizedString(@"Minimum order quantity is", @""),[SingletonClass sharedSingletonClass].settingsDictionary[@"min_order"],NSLocalizedString(@"Carton", @""),NSLocalizedString(@"Shrink", @"")]];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
            [alert show];

            return;
        }
        AddressViewController* addressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddressViewController"];
        addressViewController.viewType=@"checkout";
        addressViewController.dataDict=[NSMutableDictionary dictionary];
        [addressViewController.dataDict setObject:[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]] forKey:@"cartArray"];
        [[SlideNavigationController sharedInstance] pushViewController:addressViewController animated:YES];
    }
    else
    {
        LoginViewController* loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        loginViewController.viewType=@"payment";
        [[SlideNavigationController sharedInstance] pushViewController:loginViewController animated:YES];
    }

}
-(void)homeButtonTap:(UIButton*)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[HomeViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        HomeViewController* homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:homeViewController animated:YES];
        
    }
}
-(void)changeCart:(UIButton*)sender stat:(BOOL)add
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:cartTableView];
    NSIndexPath *indexPath = [cartTableView indexPathForRowAtPoint:buttonPosition];
    ShoppingCartTableViewCell *cell = (ShoppingCartTableViewCell *)[cartTableView cellForRowAtIndexPath:indexPath];
    currentProduct=cartArray[indexPath.row];
    if ([currentProduct.quantity intValue]==1 && !add)
    {
        delAll = NO;
        [popUpView setHidden:NO];
        return;
    }

    if (add)
    {
        [[DataManager sharedManager] updateCartByCartObject:currentProduct quantity:@""];
    }
    else
    {
        [[DataManager sharedManager] deleteCartbyCartOject:currentProduct];
    }
    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
    if ([currentProduct.quantity intValue]>0)
    {
        [cell.quantity setText:[NSString stringWithFormat:@"%@",currentProduct.quantity]];
    }
    else
    {
        [cartTableView reloadData];
    }
    [self changeCartButtons:nil];
}
-(void)changeCartButtons:(PriceTableViewCell*)priceCell
{
    if (!priceCell)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(cartArray.count) inSection:0];
        priceCell = (PriceTableViewCell *)[cartTableView cellForRowAtIndexPath:indexPath];
    }
    double vat=[[SingletonClass sharedSingletonClass].settingsDictionary[@"vat"] doubleValue];

    if (cartArray.count>0)
    {
        int totalItems=0;
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            totalItems= totalItems+[cart.quantity intValue];
        }
        [priceCell.amountLabel setText:[NSString stringWithFormat:@"%.02f %@",totalPrice,NSLocalizedString(@"SR", @"")]];
        [priceCell.priceLabel setText:[NSString stringWithFormat:@"%.02f %@",totalPrice+(totalPrice*vat)/100,NSLocalizedString(@"SR", @"")]];
        [priceCell.vatLabel setText:[NSString stringWithFormat:@"%.02f %@",(totalPrice*vat)/100,NSLocalizedString(@"SR", @"")]];
        if ([SettingsClass isLanguageArabic])
        {
            [priceCell.vatQuantity setText:[NSString stringWithFormat:@"%@ (%%%d)",NSLocalizedString(@"VAT", @""),(int)vat]];
        }
        else
        {
            [priceCell.vatQuantity setText:[NSString stringWithFormat:@"%@ (%d%%)",NSLocalizedString(@"VAT", @""),(int)vat]];
        }

    }
    else
    {
    }
}



- (IBAction)okButtonTap:(UIButton *)sender
{
    if (delAll)
    {
        [[DataManager sharedManager] deleteAllCart];
        cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
        [cartTableView reloadData];
  
    }
    else
    {
        [[DataManager sharedManager] deleteCartOjectCompletely:currentProduct];
        cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
        [cartTableView reloadData];
    }
    
    [popUpView setHidden:YES];
}

- (IBAction)cancelButtonTap:(UIButton *)sender
{
    [popUpView setHidden:YES];
}

-(int)totalNumberOfItems
{
    if (cartArray.count>0)
    {
        int totalItems=0;
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            totalItems= totalItems+[cart.quantity intValue];
        }
        return totalItems;
    }
    else
    {
        return 0;
    }
}

@end
