//
//  ShoppingCartViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/3/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface ShoppingCartViewController : BaseViewController
{
    User *currentUser;

    IBOutlet UITableView *cartTableView;
    NSMutableArray *cartArray;
    IBOutlet UILabel *messageLabel;
    IBOutlet UIButton *okButton;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIView *popUpView;
    Cart *currentProduct;
    BOOL delAll;
    IBOutlet UIButton *backButton;
}
@property (strong, nonatomic) NSString *viewType;
- (IBAction)okButtonTap:(UIButton *)sender;
- (IBAction)cancelButtonTap:(UIButton *)sender;
@end
