//
//  PriceTableViewCell.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/4/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "PriceTableViewCell.h"

@implementation PriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.checkOutButton.layer.cornerRadius=self.checkOutButton.frame.size.height/2;
    self.checkOutButton.layer.masksToBounds = YES;

    self.homeButton.layer.cornerRadius=self.homeButton.frame.size.height/2;
    self.homeButton.layer.masksToBounds = YES;

    self.emptyCartButton.layer.cornerRadius=self.emptyCartButton.frame.size.height/2;
    self.emptyCartButton.layer.masksToBounds = YES;
    self.emptyCartButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.homeButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.checkOutButton.titleLabel.adjustsFontSizeToFitWidth = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
