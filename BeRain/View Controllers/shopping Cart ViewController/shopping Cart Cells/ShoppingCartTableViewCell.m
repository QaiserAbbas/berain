//
//  ShoppingCartTableViewCell.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/3/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ShoppingCartTableViewCell.h"
#import "Cart+CoreDataClass.h"
#import "SettingsClass.h"

@implementation ShoppingCartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.plusButton.layer.cornerRadius=self.plusButton.frame.size.height/2;
    self.plusButton.layer.masksToBounds = YES;

    self.minusButton.layer.cornerRadius=self.plusButton.frame.size.height/2;
    self.minusButton.layer.masksToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setdataCell:(id)object;
{
    Cart *cart=(Cart*)object;
    
    if ([SettingsClass isLanguageArabic])
    {
        [self.productName setText:cart.dish_title_ar];
    }
    else
    {
        [self.productName setText:cart.dish_title_en];
    }
    if ([SettingsClass isLanguageArabic])
    {
        [self.productName setTextAlignment:NSTextAlignmentRight];
        [self.productPrice setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [self.productName setTextAlignment:NSTextAlignmentLeft];
        [self.productPrice setTextAlignment:NSTextAlignmentLeft];
    }


    [self.productPrice setText:[NSString stringWithFormat:@"%@ %@",cart.dish_price,NSLocalizedString(@"SR", @"")]];
    [self.quantity setText:cart.quantity];
    [self.productImageView setShowActivityIndicatorView:YES];
    [self.productImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];

    [self.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://berain.com.sa/oms/mob_app/resources/uploads/files/small/%@",cart.dish_image]] placeholderImage:[UIImage imageNamed:@"usericon"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.productImageView setShowActivityIndicatorView:NO];
    }];
}
@end
