//
//  ProductListViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductListTableViewCell.h"

@interface ProductListViewController ()

@end

@implementation ProductListViewController

//#pragma mark side menu
//
//- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
//{
//    return [SettingsClass leftMenuOption];
//}
//
//- (BOOL)slideNavigationControllerShouldDisplayRightMenu
//{
//    return [SettingsClass rightMenuOption];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    productArray=[NSMutableArray array];
    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
    [self changeCartButtons];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [GeneralWebservices webserviceMainSplashCall:nil webserviceName:Webservice_AllProducts OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            productArray =[NSMutableArray arrayWithArray:returnDict[@"rows"]];
            [productTableView reloadData];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            [alert setTitle:NSLocalizedString(@"Berain", @"")];
            [alert setMessage:NSLocalizedString(@"Please try again", @"")];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
            [alert show];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];

}

-(void)viewWillAppear:(BOOL)animated
{
    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
    [self changeCartButtons];
    [productTableView reloadData];
}

-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return productArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* Identifier = @"ProductListTableViewCell";
    
    ProductListTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if( cell == nil )
    {
        NSString* cellName = @"ProductListTableViewCell";
        if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            //                cellName = @"LiveEventsTableViewCell-iPad";
        }
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.cartButton addTarget:self action:@selector(cartButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [cell.plusButton addTarget:self action:@selector(plusButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minusButton addTarget:self action:@selector(minusButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [cell.cartButton setTag:indexPath.row];
    [cell.plusButton setTag:indexPath.row];
    [cell.minusButton setTag:indexPath.row];

    [cell setdataCell:productArray[indexPath.row] cartArray:cartArray];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)menuButtonTap:(UIButton *)sender
{
    [[SlideNavigationController sharedInstance] toggleMenu:[SettingsClass OpenManuWithNumber] withCompletion:nil];
}
-(void)cartButtonTap:(UIButton*)sender
{
    [self changeCart:sender stat:YES];
}
-(void)plusButtonTap:(UIButton*)sender
{
    [self changeCart:sender stat:YES];
}
-(void)minusButtonTap:(UIButton*)sender
{
    [self changeCart:sender stat:NO];
}
-(void)changeCart:(UIButton*)sender stat:(BOOL)add
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:productTableView];
    NSIndexPath *indexPath = [productTableView indexPathForRowAtPoint:buttonPosition];
    ProductListTableViewCell *cell = (ProductListTableViewCell *)[productTableView cellForRowAtIndexPath:indexPath];
    NSMutableDictionary *currentProduct=[NSMutableDictionary dictionaryWithDictionary:productArray[indexPath.row]];
    if (add)
    {
        [[DataManager sharedManager] saveCart:currentProduct quantity:@"1"];
    }
    else
    {
        [[DataManager sharedManager] deleteCart:currentProduct];
    }
    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"dish_id == %@",currentProduct[@"dish_id"]];
    NSArray *beginWithB = [cartArray filteredArrayUsingPredicate:bPredicate];
    if (beginWithB.count>0)
    {
//        [cell.cartonInfo setHidden:NO];
        [cell.plusMinusView setHidden:NO];
        [cell.cartButton setHidden:YES];
        Cart *currentCart=beginWithB[0];
        if ([SettingsClass isLanguageArabic])
        {
            [cell.cartonInfo setText:[NSString stringWithFormat:@"  %@ X %@",currentCart.quantity,NSLocalizedString(@"Carton", @"")]];
        }
        else
        {
            [cell.cartonInfo setText:[NSString stringWithFormat:@" X %@ %@",currentCart.quantity,NSLocalizedString(@"Carton", @"")]];
        }
        NSString *priceText = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"SR", @""),currentCart.dish_price];
        
        NSString *cartonText = [NSString stringWithFormat:@" X %@ %@",currentCart.quantity,NSLocalizedString(@"Carton", @"")];

        
        NSString *text = [NSString stringWithFormat:@"%@ %@",
                          priceText,
                          cartonText];
        
        // If attributed text is supported (iOS6+)
        if ([cell.productPrice respondsToSelector:@selector(setAttributedText:)]) {
            
            // Define general attributes for the entire text
            NSDictionary *attribs = @{
                                      NSForegroundColorAttributeName:   cell.productPrice.textColor,
                                      NSFontAttributeName: cell.productPrice.font
                                      };
            NSMutableAttributedString *attributedText =
            [[NSMutableAttributedString alloc] initWithString:text
                                                   attributes:attribs];
            
            // Red text attributes
            UIColor *redColor = [UIColor colorWithRed:2.f/255.f green:175.f/255.f blue:171.f/255.f alpha:1.f];
            NSRange redTextRange = [text rangeOfString:priceText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
            [attributedText setAttributes:@{NSForegroundColorAttributeName:redColor}
                                    range:redTextRange];
            
            // Green text attributes
            UIFont *font =[UIFont fontWithName:@"DroidArabicKufi" size:10];
            
            UIColor *greenColor = [UIColor darkGrayColor];
            NSRange greenTextRange = [text rangeOfString:cartonText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
            [attributedText setAttributes:@{NSForegroundColorAttributeName:greenColor,NSFontAttributeName:font}
                                    range:greenTextRange];
            
            cell.productPrice.attributedText = attributedText;
        }
        // If attributed text is NOT supported (iOS5-)
        else {
            cell.productPrice.text = text;
        }


    }
    else
    {
        NSString *priceText = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"SR", @""),currentProduct[@"dish_price"]];

        if ([cell.productPrice respondsToSelector:@selector(setAttributedText:)]) {
            
            // Define general attributes for the entire text
            NSDictionary *attribs = @{
                                      NSForegroundColorAttributeName:   cell.productPrice.textColor,
                                      NSFontAttributeName: cell.productPrice.font
                                      };
            NSMutableAttributedString *attributedText =
            [[NSMutableAttributedString alloc] initWithString:priceText
                                                   attributes:attribs];
            
            // Red text attributes
            UIColor *redColor = [UIColor colorWithRed:2.f/255.f green:175.f/255.f blue:171.f/255.f alpha:1.f];
            NSRange redTextRange = [priceText rangeOfString:priceText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
            [attributedText setAttributes:@{NSForegroundColorAttributeName:redColor}
                                    range:redTextRange];
            cell.productPrice.attributedText = attributedText;
        }
        // If attributed text is NOT supported (iOS5-)
        else {
            cell.productPrice.text = priceText;
        }

        [cell.cartonInfo setHidden:YES];
        [cell.plusMinusView setHidden:YES];
        [cell.cartButton setHidden:NO];
    }
    

    [self changeCartButtons];
}
-(void)changeCartButtons
{
    if (cartArray.count>0)
    {
        int totalItems=0;
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            totalItems= totalItems+[cart.quantity intValue];
        }
        [cartUpperButton setHidden:NO];
        [self configureButton:cartUpperButton withBadge:[NSString stringWithFormat:@"%d",totalItems]];
        [priceLowerLabel setText:[NSString stringWithFormat:@"%.02f",totalPrice]];
    }
    else
    {
        [cartUpperButton setHidden:YES];
        [priceLowerLabel setText:[NSString stringWithFormat:@"00.00"]];
    }
}
- (void) configureButton:(MIBadgeButton *) button withBadge:(NSString *) badgeString {
    // optional to change the default position of the badge
    if ([SettingsClass isLanguageArabic])
    {
        [button setBadgeEdgeInsets:UIEdgeInsetsMake(20, -7, 0, 8)];
    }
    else
    {
        [button setBadgeEdgeInsets:UIEdgeInsetsMake(20, -35, 0, 8)];
    }
    [button setBadgeString:badgeString];
    [button setBadgeBackgroundColor:[UIColor whiteColor]];
    [button setBadgeTextColor:[UIColor darkGrayColor]];
}

@end
