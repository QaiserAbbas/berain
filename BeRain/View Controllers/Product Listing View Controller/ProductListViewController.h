//
//  ProductListViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"
#import "MIBadgeButton.h"

@interface ProductListViewController : BaseViewController
{
    NSMutableArray *productArray;
    NSMutableArray *cartArray;
    IBOutlet UITableView *productTableView;
    IBOutlet MIBadgeButton *cartUpperButton;
    IBOutlet UILabel *priceLowerLabel;
    IBOutlet UILabel *cartAttributedLabel;
    
}
@end
