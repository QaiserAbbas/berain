//
//  ProductListTableViewCell.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ProductListTableViewCell.h"
#import "Cart+CoreDataClass.h"
#import "SettingsClass.h"

@implementation ProductListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setdataCell:(NSMutableDictionary*)object cartArray:(NSMutableArray*)cartArray;
{
    if ([SettingsClass isLanguageArabic])
    {
        [self.productName setTextAlignment:NSTextAlignmentRight];
        [self.productPrice setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [self.productName setTextAlignment:NSTextAlignmentLeft];
        [self.productPrice setTextAlignment:NSTextAlignmentLeft];
    }

    [self.cartonInfo setHidden:YES];
    [self.plusMinusView setHidden:YES];
    [self.cartButton setHidden:NO];
    
    if ([SettingsClass isLanguageArabic])
    {
        [self.productName setText:object[@"dish_title_ar"]];
    }
    else
    {
        [self.productName setText:object[@"dish_title_en"]];
    }

    NSString *cartonText = @"";

    if (object[@"ord_count"])
    {
        [self.cartonInfo setHidden:YES];
        if ([SettingsClass isLanguageArabic])
        {
            [self.cartonInfo setText:[NSString stringWithFormat:@" %@ X %@",object[@"ord_count"],NSLocalizedString(@"Carton", @"")]];
        }
        else
        {
            [self.cartonInfo setText:[NSString stringWithFormat:@" X %@ %@",object[@"ord_count"],NSLocalizedString(@"Carton", @"")]];
        }
        cartonText = self.cartonInfo.text;
        [self.productPrice setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"SR", @""),object[@"dish_price"]]];

    }
    else
    {
        [self.productPrice setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"SR", @""),object[@"dish_price"]]];
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"dish_id == %@",object[@"dish_id"]];
        NSArray *beginWithB = [cartArray filteredArrayUsingPredicate:bPredicate];
        Cart *currentCart;
        
        if (beginWithB.count>0)
        {
            //        [self.cartonInfo setHidden:NO];
            [self.plusMinusView setHidden:NO];
            [self.cartButton setHidden:YES];
            currentCart=beginWithB[0];
            //        [self.cartonInfo setText:[NSString stringWithFormat:@" X %@ %@",currentCart.quantity,NSLocalizedString(@"Carton", @"")]];
            if ([SettingsClass isLanguageArabic])
            {
                [self.cartonInfo setText:[NSString stringWithFormat:@"X %@ %@",currentCart.quantity,NSLocalizedString(@"Carton", @"")]];
            }
            else
            {
                [self.cartonInfo setText:[NSString stringWithFormat:@" X %@ %@",currentCart.quantity,NSLocalizedString(@"Carton", @"")]];
            }
            
            cartonText = self.cartonInfo.text;
        }

    }
    
    NSString *priceText = [NSString stringWithFormat:@"%@",self.productPrice.text];

    [self.productImageView setShowActivityIndicatorView:YES];
    [self.productImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];

    [self.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://berain.com.sa/oms/mob_app/resources/uploads/files/small/%@",object[@"dish_image"]]] placeholderImage:[UIImage imageNamed:@"usericon"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.productImageView setShowActivityIndicatorView:NO];
    }];
    
    
    NSString *text = [NSString stringWithFormat:@"%@ %@",
                      priceText,
                      cartonText];
    
    // If attributed text is supported (iOS6+)
    if ([self.productPrice respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName:   self.productPrice.textColor,
                                  NSFontAttributeName: self.productPrice.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        // Red text attributes
        UIColor *redColor = [UIColor colorWithRed:2.f/255.f green:175.f/255.f blue:171.f/255.f alpha:1.f];
        NSRange redTextRange = [text rangeOfString:priceText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:redColor}
                                range:redTextRange];
        
        // Green text attributes
        UIFont *font =[UIFont fontWithName:@"DroidArabicKufi" size:10];
        
        UIColor *greenColor = [UIColor darkGrayColor];
        NSRange greenTextRange = [text rangeOfString:cartonText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:greenColor,NSFontAttributeName:font}
                                range:greenTextRange];
        
        self.productPrice.attributedText = attributedText;
    }
    // If attributed text is NOT supported (iOS5-)
    else {
        self.productPrice.text = text;
    }

}
@end
