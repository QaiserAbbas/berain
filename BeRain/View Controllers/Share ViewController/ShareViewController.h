//
//  ShareViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 1/11/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface ShareViewController : BaseViewController
{
    IBOutlet UIImageView *imageBCView;
    IBOutlet UIButton *shareButton;
    IBOutlet UIView *shareView;
    
}
- (IBAction)cancelButtonTap:(UIButton *)sender;
- (IBAction)shareButtonTap:(UIButton *)sender;
@end
