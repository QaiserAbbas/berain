//
//  ShareViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 1/11/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import "ShareViewController.h"

@interface ShareViewController ()

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    imageBCView.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
    imageBCView.layer.cornerRadius=5;
    imageBCView.layer.borderWidth=1.0;
    imageBCView.layer.masksToBounds = YES;
    imageBCView.layer.borderColor=[[UIColor lightGrayColor] CGColor];

    shareButton.layer.cornerRadius=shareButton.frame.size.height/2;
    shareButton.layer.masksToBounds = YES;
    
    shareView.layer.cornerRadius=10;
    shareView.layer.masksToBounds = YES;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButtonTap:(UIButton *)sender
{
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (IBAction)shareButtonTap:(UIButton *)sender
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    [sharingItems addObject:NSLocalizedString(@"\"Wow this is the most delicious water !!\"", @"")];
//    [sharingItems addObject:[NSURL URLWithString:@"https://itunes.apple.com/us/app/glorytaxi/id1204487551?mt=8"]];
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [[SlideNavigationController sharedInstance] presentViewController:activityController animated:YES completion:nil];
}
@end
