//
//  MyOrderTableViewCell.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "MyOrderTableViewCell.h"
#import "SettingsClass.h"

@implementation MyOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setdataCell:(NSMutableDictionary*)object;
{
    if ([SettingsClass isLanguageArabic])
    {
        [self.orderNumber setTextAlignment:NSTextAlignmentRight];
        [self.amount setTextAlignment:NSTextAlignmentRight];
        [self.status setTextAlignment:NSTextAlignmentRight];
        [self.date setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [self.orderNumber setTextAlignment:NSTextAlignmentLeft];
        [self.amount setTextAlignment:NSTextAlignmentLeft];
        [self.status setTextAlignment:NSTextAlignmentLeft];
        [self.date setTextAlignment:NSTextAlignmentLeft];
    }
    
    UIFont *font=self.orderNumber.font;
    UIColor *blackColor=[UIColor blackColor];
    UIColor *megentaColor=[UIColor colorWithRed:116/255.0 green:51/255.0 blue:120/255.0 alpha:1];
    

    {
        //    [self.orderNumber setText:object[@"ord_id"]];
        NSString*orderStr=NSLocalizedString(@"Order number:", @"");
        NSString*ordernumber=object[@"ord_id"];
        NSString *orderText = [NSString stringWithFormat:@"%@ %@",
                               orderStr,
                               ordernumber];
        
        // If attributed text is supported (iOS6+)
        if ([self.orderNumber respondsToSelector:@selector(setAttributedText:)]) {
            
            // Define general attributes for the entire text
            NSDictionary *attribs = @{
                                      NSForegroundColorAttributeName:   megentaColor,
                                      NSFontAttributeName: font
                                      };
            NSMutableAttributedString *attributedText =
            [[NSMutableAttributedString alloc] initWithString:orderText
                                                   attributes:attribs];
            
            // Red text attributes
            NSRange redTextRange = [orderText rangeOfString:ordernumber];
            [attributedText setAttributes:@{NSForegroundColorAttributeName:blackColor}
                                    range:redTextRange];
            [self.orderNumber setAttributedText:attributedText];
        }
        else
        {
            [self.orderNumber setText:orderText];
        }
 
    }
    {
        
//        [self.amount setText:[NSString stringWithFormat:@"%@",object[@"ord_total_price"]]];
        NSString*amountStr=NSLocalizedString(@"Total amount:", @"");
        NSString*amountnumber=object[@"ord_total_price"];
        NSString*srText=NSLocalizedString(@"SR", @"");
        
        NSString *amountText = [NSString stringWithFormat:@"%@ %@ %@",
                               amountStr,
                               amountnumber,srText];
        
        UIColor *srColor = [UIColor colorWithRed:2.f/255.f green:175.f/255.f blue:171.f/255.f alpha:1.f];

        // If attributed text is supported (iOS6+)
        if ([self.amount respondsToSelector:@selector(setAttributedText:)]) {
            
            // Define general attributes for the entire text
            NSDictionary *attribs = @{
                                      NSForegroundColorAttributeName:   megentaColor,
                                      NSFontAttributeName: font
                                      };
            NSMutableAttributedString *attributedText =
            [[NSMutableAttributedString alloc] initWithString:amountText
                                                   attributes:attribs];
            
            // Red text attributes
            NSRange redTextRangesr = [amountText rangeOfString:srText];
            [attributedText setAttributes:@{NSForegroundColorAttributeName:srColor}
                                    range:redTextRangesr];
            
            NSRange redTextRange = [amountText rangeOfString:amountnumber];
            [attributedText setAttributes:@{NSForegroundColorAttributeName:blackColor}
                                    range:redTextRange];

            [self.amount setAttributedText:attributedText];
        }
        else
        {
            [self.amount setText:amountText];
        }
        
    }

    {
//        [self.date setText:[NSString stringWithFormat:@"%@",object[@"ord_date"]]];
        NSString*dateStr=NSLocalizedString(@"Date:", @"");
        NSString*datenumber=object[@"ord_date"];
        NSString *orderText = [NSString stringWithFormat:@"%@ %@",
                               dateStr,
                               datenumber];
        
        // If attributed text is supported (iOS6+)
        if ([self.date respondsToSelector:@selector(setAttributedText:)]) {
            
            // Define general attributes for the entire text
            NSDictionary *attribs = @{
                                      NSForegroundColorAttributeName:   megentaColor,
                                      NSFontAttributeName: font
                                      };
            NSMutableAttributedString *attributedText =
            [[NSMutableAttributedString alloc] initWithString:orderText
                                                   attributes:attribs];
            
            // Red text attributes
            NSRange redTextRange = [orderText rangeOfString:datenumber];
            [attributedText setAttributes:@{NSForegroundColorAttributeName:blackColor}
                                    range:redTextRange];
            [self.date setAttributedText:attributedText];
        }
        else
        {
            [self.date setText:orderText];
        }
        
    }
    [self.status setText:[NSString stringWithFormat:@"%@",object[@"current_status"]]];
    if ([object[@"ord_id"] intValue]==0)
    {
        [self.status setText:NSLocalizedString(@"You add new order", @"")];
    }
    else if ([object[@"current_status"] intValue]==1)
    {
        [self.status setText:NSLocalizedString(@"You add new order", @"")];
    }
    else if ([object[@"current_status"] intValue]==2)
    {
        [self.status setText:NSLocalizedString(@"Your order is confirmed", @"")];
    }
    else if ([object[@"current_status"] intValue]==3)
    {
        [self.status setText:NSLocalizedString(@"Your order is assigned to driver", @"")];
    }
    else if ([object[@"current_status"] intValue]==4)
    {
        [self.status setText:NSLocalizedString(@"Your order is delivered", @"")];
    }
    else if ([object[@"current_status"] intValue]==5)
    {
        [self.status setText:NSLocalizedString(@"Your order is pending", @"")];
    }
    else if ([object[@"current_status"] intValue]==6)
    {
        [self.status setText:NSLocalizedString(@"Your order is cancelled", @"")];
    }
}

@end
