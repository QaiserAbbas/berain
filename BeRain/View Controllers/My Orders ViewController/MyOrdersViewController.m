//
//  MyOrdersViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "MyOrdersViewController.h"
#import "MyOrderTableViewCell.h"
#import "OrderDetailViewController.h"

@interface MyOrdersViewController ()

@end

@implementation MyOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    pageNumber=0;
    ordersArray=[NSMutableArray array];
    
    [self loadorders];
    
    __weak typeof(self) weakSelfOwn = self;
    [ordersTableView addPullToRefreshWithActionHandler:^{
        pageNumber=0;
        [weakSelfOwn loadorders];
    }];
    [ordersTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelfOwn loadorders];
    }];


}

-(void)loadorders
{
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:[NSString stringWithFormat:@"%@",currentUser.user_id] forKey:@"user_id"];
    [paramDict setObject:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page"];
    
    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_AllOrders OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            NSArray *array=[NSArray arrayWithArray:returnDict[@"rows"]];
            if (array.count>0)
            {
                if (pageNumber==0)
                {
                    ordersArray=[NSMutableArray array];
                }
                pageNumber++;
                for (NSMutableDictionary *dataDict in array)
                {
                    if (![ordersArray containsObject:dataDict])
                    {
                        [ordersArray addObject:dataDict];
                    }
                }
                [ordersTableView reloadData];
            }
            
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            [alert setTitle:NSLocalizedString(@"Berain", @"")];
            [alert setMessage:NSLocalizedString(@"Please try again", @"")];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
            [alert show];
        }
        [ordersTableView.pullToRefreshView stopAnimating];
        [ordersTableView.infiniteScrollingView stopAnimating];

        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegates

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 125;
}
-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return ordersArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* Identifier = @"MyOrderTableViewCell";
    
    MyOrderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if( cell == nil )
    {
        NSString* cellName = @"MyOrderTableViewCell";
        if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            //                cellName = @"LiveEventsTableViewCell-iPad";
        }
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell setdataCell:ordersArray[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OrderDetailViewController* orderDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailViewController"];
    NSMutableDictionary *orderDict=[NSMutableDictionary dictionaryWithDictionary:ordersArray[indexPath.row]];
    orderDetailViewController.order_id=orderDict[@"ord_id"];
    [[SlideNavigationController sharedInstance] pushViewController:orderDetailViewController animated:YES];

}

@end
