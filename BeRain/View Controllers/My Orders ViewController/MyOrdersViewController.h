//
//  MyOrdersViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface MyOrdersViewController : BaseViewController
{
    IBOutlet UITableView *ordersTableView;
    NSMutableArray *ordersArray;
    User *currentUser;
    int pageNumber;
}

@end
