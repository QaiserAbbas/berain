//
//  OrderDetailViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/6/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"
#import "MIBadgeButton.h"

@interface OrderDetailViewController : BaseViewController
{
    
    IBOutlet UITableView *orderDetailTableView;
    IBOutlet UIButton *reorderButton;
    NSMutableArray *ordersArray;
    NSMutableArray *cartArray;
    IBOutlet MIBadgeButton *cartUpperButton;

}
@property (strong, nonatomic) NSString *order_id;

- (IBAction)reorderButtonTap:(UIButton *)sender;
@end
