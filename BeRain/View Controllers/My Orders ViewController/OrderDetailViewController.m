//
//  OrderDetailViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/6/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "ProductListTableViewCell.h"
#import "ShoppingCartViewController.h"

@interface OrderDetailViewController ()

@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    reorderButton.layer.cornerRadius=reorderButton.frame.size.height/2;
    reorderButton.layer.masksToBounds = YES;
    
    cartArray=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCart]];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:self.order_id forKey:@"ord_id"];
    
    
    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_OrderDetail OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            ordersArray =[NSMutableArray arrayWithArray:returnDict[@"order"][@"order_detail"]];
            [orderDetailTableView reloadData];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            [alert setTitle:NSLocalizedString(@"Berain", @"")];
            [alert setMessage:NSLocalizedString(@"Please try again", @"")];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
            [alert show];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self changeCartButtons];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
-(NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return ordersArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* Identifier = @"ProductListTableViewCell";
    
    ProductListTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if( cell == nil )
    {
        NSString* cellName = @"ProductListTableViewCell";
        if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            //                cellName = @"LiveEventsTableViewCell-iPad";
        }
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.cartButton setHidden:YES];
    
    [cell.plusButton setHidden:YES];
    [cell.minusButton setHidden:YES];
    
    [cell setdataCell:ordersArray[indexPath.row] cartArray:cartArray];
    
    [cell.cartButton setHidden:YES];
    
    [cell.plusButton setHidden:YES];
    [cell.minusButton setHidden:YES];
    [cell.plusMinusView setHidden:YES];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (IBAction)reorderButtonTap:(UIButton *)sender
{
    [[DataManager sharedManager] saveCartReOrder:ordersArray];
    
    ShoppingCartViewController* shoppingCartViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingCartViewController"];
    [[SlideNavigationController sharedInstance] pushViewController:shoppingCartViewController animated:YES];    
}
-(void)changeCartButtons
{
    if (cartArray.count>0)
    {
        int totalItems=0;
        double totalPrice=0;
        for (Cart*cart in cartArray)
        {
            totalPrice= totalPrice+([cart.quantity doubleValue]*[cart.dish_price doubleValue]);
            totalItems= totalItems+[cart.quantity intValue];
        }
        [cartUpperButton setHidden:NO];
        [self configureButton:cartUpperButton withBadge:[NSString stringWithFormat:@"%d",totalItems]];
    }
    else
    {
        [cartUpperButton setHidden:YES];
    }
}

- (void) configureButton:(MIBadgeButton *) button withBadge:(NSString *) badgeString {
    // optional to change the default position of the badge
    if ([SettingsClass isLanguageArabic])
    {
        [button setBadgeEdgeInsets:UIEdgeInsetsMake(20, -7, 0, 10)];
    }
    else
    {
        [button setBadgeEdgeInsets:UIEdgeInsetsMake(20, -35, 0, 10)];
    }
    [button setBadgeString:badgeString];
    [button setBadgeBackgroundColor:[UIColor whiteColor]];
    [button setBadgeTextColor:[UIColor darkGrayColor]];
}

@end
