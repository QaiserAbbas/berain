//
//  HomeViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 11/22/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeViewController : BaseViewController
{
    IBOutlet UIImageView *bottomTextImageView;
    IBOutlet UIButton *orderNowButton;
    IBOutlet UILabel *bottomTextLabel;
    
    NSTimer *_labelTimer;
    NSInteger labelIndex;
    NSArray *arrayOfText;
    IBOutlet UIView *swipeView;
}

- (IBAction)menuButtonTap:(UIButton *)sender;
- (IBAction)orderNowButtonTap:(UIButton *)sender;

@end
