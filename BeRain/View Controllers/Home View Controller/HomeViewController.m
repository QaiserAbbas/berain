//
//  HomeViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 11/22/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "HomeViewController.h"
#import "LJAutoScrollView.h"

static const CGFloat kAutoScrollViewHeight = 130;

@interface HomeViewController ()<LJAutoScrollViewDelegate>
@property (nonatomic, strong) LJAutoScrollView *autoScrollView;

@end

@implementation HomeViewController

//#pragma mark side menu
//
//- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
//{
//    return NO;
////    return [SettingsClass leftMenuOption];
//}
//
//- (BOOL)slideNavigationControllerShouldDisplayRightMenu
//{
//    return NO;
////    return [SettingsClass rightMenuOption];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    bottomTextImageView.layer.cornerRadius=15;
    bottomTextImageView.layer.masksToBounds = YES;
    orderNowButton.layer.cornerRadius=8;
    orderNowButton.layer.masksToBounds = YES;

    labelIndex = 0;
    NSLog(@"%@",arrayOfText);
    arrayOfText=[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayTips]];
    
    
//    if (arrayOfText.count>0)
//    {
//        Tips *tip=arrayOfText[0];
//        bottomTextLabel.text = tip.detail_en;
//    }
//    swipeView.pagingEnabled = YES;
//    
//    swipeView.alignment = SwipeViewAlignmentCenter;
//    swipeView.pagingEnabled = YES;
//    swipeView.itemsPerPage = 1;
//    swipeView.truncateFinalPage = YES;
//
//    [swipeView reloadData];
    

    // Setup a timer to run every 2 seconds and update the label
    // Increment the labelIndex so the next label is shown next time
    
    // Adding the swipe gesture on image view
//    [bottomTextImageView addGestureRecognizer:swipeLeft];
//    [bottomTextImageView addGestureRecognizer:swipeRight];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    // Stop the timer when we leave
    [_labelTimer invalidate];
    _labelTimer = nil;
}

- (void)dealloc {
    // Make sure it is stopped
    [_labelTimer invalidate];
}
-(void)viewDidAppear:(BOOL)animated
{
    for (UIView*view in self.view.subviews)
    {
        if ([view isKindOfClass:[LJAutoScrollView class]])
        {
            [view removeFromSuperview];
        }
    }
    
    self.autoScrollView = [[LJAutoScrollView alloc] initWithFrame:CGRectMake(0, swipeView.frame.origin.y, self.view.frame.size.width, kAutoScrollViewHeight)];
    self.autoScrollView.delegate = self;
    self.autoScrollView.itemSize = CGSizeMake(self.view.frame.size.width, kAutoScrollViewHeight);
    self.autoScrollView.scrollInterval = 5.0f;
    [self.autoScrollView setBackgroundColor:[UIColor clearColor]];
    self.autoScrollView.numberOfPages=arrayOfText.count;
    [self.view addSubview:self.autoScrollView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menuButtonTap:(UIButton *)sender
{
    [[SlideNavigationController sharedInstance] toggleMenu:[SettingsClass OpenManuWithNumber] withCompletion:nil];
}

- (IBAction)orderNowButtonTap:(UIButton *)sender
{
}

- (UIView *)autoScrollView:(LJAutoScrollView *)autoScrollView customViewForIndex:(NSInteger)index
{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kAutoScrollViewHeight)];
    
    
    Tips *tip=arrayOfText[index];
    UILabel *label=[[UILabel alloc] initWithFrame:bottomTextLabel.frame];


    if ([SettingsClass isLanguageArabic])
    {
        NSString* myString = [tip.detail_ar stringByReplacingOccurrencesOfString:@"\n" withString:@" "];

        label.text=myString;
    }
    else
    {
        label.text=tip.detail_en;
    }
    UIFont *font =[UIFont fontWithName:@"DroidArabicKufi" size:15];
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:font];
    label.numberOfLines=100;
    [label setMinimumFontSize:6];
    [label setClipsToBounds:YES];
    NSLog(@"%@",label.text);
    
    //don't do anything specific to the index within
    //this `if (view == nil) {...}` statement because the view will be
    //recycled and used with other index values later
    if (index == 0)
    {
        //            116,51,117,0.75
        [bottomTextImageView setBackgroundColor:[UIColor colorWithRed:116/255.0 green:51/255.0 blue:117/255.0 alpha:1]];
    }
    else if(index == 1)
    {
        //            234,197,30,0.75
        [bottomTextImageView setBackgroundColor:[UIColor colorWithRed:234/255.0 green:197/255.0 blue:30/255.0 alpha:1]];
    }
    else if(index == 2)
    {
        //            30,131,234,0.75
        [bottomTextImageView setBackgroundColor:[UIColor colorWithRed:30/255.0 green:131/255.0 blue:234/255.0 alpha:1]];
    }
    UIImageView*imageView=[[UIImageView alloc] initWithFrame:bottomTextImageView.frame];
    [imageView setBackgroundColor:bottomTextImageView.backgroundColor];
    imageView.layer.cornerRadius=15;
    imageView.layer.masksToBounds = YES;
    [imageView setAlpha:0.7];
    [view addSubview:imageView];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]];
    
    return view;
    
}

- (NSInteger)numberOfPagesInAutoScrollView:(LJAutoScrollView *)autoScrollView
{
    return arrayOfText.count;
}

- (void)autoScrollView:(LJAutoScrollView *)autoScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"select item at index:%@", @(index));
}


@end
