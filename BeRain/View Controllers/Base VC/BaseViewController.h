//
//  BaseViewController.h
//  IWasHere
//
//  Created by Qaiser Abbas on 21/10/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsClass.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "AppConstants.h"
#import "SingletonClass.h"
#import "UserObject.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "LGAlertView.h"
#import "GeneralWebservices.h"
#import "AsyncImageView.h"
#import <UIKit/UIKit.h>
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "UIImage+ImageWithView.h"
#import "UIImage+Additions.h"
#import <FXBlurView/FXBlurView.h>
#import "SlideNavigationController.h"
#import "DataManager.h"
#import "LanguageManager.h"

@interface BaseViewController : UIViewController
{
    
}


-(IBAction) backButtonSelected:(id)sender;

- (IBAction)homeButtonTap:(UIButton *)sender;
- (IBAction)productListingButtonTap:(UIButton *)sender;
- (IBAction)mainViewTap:(UIButton *)sender;
- (IBAction)notificationButtonTap:(UIButton *)sender;
- (IBAction)communityButtonTap:(UIButton *)sender;
- (IBAction)menuButtonTap:(UIButton *)sender;
- (IBAction)myOrdersHistoryButtonTap:(UIButton *)sender;



@end
