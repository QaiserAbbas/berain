//
//  BaseViewController.m
//  IWasHere
//
//  Created by Qaiser Abbas on 21/10/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeViewController.h"
//#import "JourneyViewController.h"
//#import "CommunityViewController.h"
//#import "UserProfileViewController.h"
//#import "NotificationsViewController.h"
#import "ProductListViewController.h"
#import "MyOrdersViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}


-(IBAction) backButtonSelected:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[HomeViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        HomeViewController* homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:homeViewController animated:YES];
        
    }
}

- (IBAction)productListingButtonTap:(UIButton *)sender
{
    [[SlideNavigationController sharedInstance].viewControllers enumerateObjectsUsingBlock:^( id obj, NSUInteger idx, BOOL* stop ) {
        if( [obj isKindOfClass:[ProductListViewController class]] ) {
            [[SlideNavigationController sharedInstance] popToViewController:obj animated:YES];
            *stop = YES;
        }
    }];
}

- (IBAction)mainViewTap:(UIButton *)sender
{
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^( id obj, NSUInteger idx, BOOL* stop ) {
        if( [obj isKindOfClass:[HomeViewController class]] ) {
            [self.navigationController popToViewController:obj animated:YES];
            *stop = YES;
        }
    }];
}

- (IBAction)notificationButtonTap:(UIButton *)sender
{
//    NotificationsViewController *notificationsViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationsViewController"];
//    [self.navigationController pushViewController:notificationsViewController animated:YES];
}

- (IBAction)communityButtonTap:(UIButton *)sender
{
//    CommunityViewController *communityViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"CommunityViewController"];
//    [self.navigationController pushViewController:communityViewController animated:YES];

}
- (IBAction)myOrdersHistoryButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[MyOrdersViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        MyOrdersViewController* myOrdersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyOrdersViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:myOrdersViewController animated:YES];
    }
    
}


- (IBAction)menuButtonTap:(UIButton *)sender
{
    [[SlideNavigationController sharedInstance] toggleMenu:[SettingsClass OpenManuWithNumber] withCompletion:nil];
}

@end
