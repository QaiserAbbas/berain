//
//  ForgotPasswordViewController.m
//  GoldApp
//
//  Created by Qaiser Abbas on 19/04/2016.
//  Copyright © 2016 Bir Al Sabia. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "ConfirmationViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.bgImageView.image = self.image;
    [emailTextField becomeFirstResponder];
    
    emailImageView.layer.backgroundColor=[[UIColor whiteColor] CGColor];
    emailImageView.layer.cornerRadius=5;
    emailImageView.layer.borderWidth=1.0;
    emailImageView.layer.masksToBounds = YES;
    emailImageView.layer.borderColor=[[UIColor grayColor] CGColor];

    submitButton.layer.cornerRadius=submitButton.frame.size.height/2;
    submitButton.layer.masksToBounds = YES;
    cancelButton.layer.cornerRadius=cancelButton.frame.size.height/2;
    cancelButton.layer.masksToBounds = YES;

    if ([SettingsClass isLanguageArabic])
    {
        [emailTextField setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [emailTextField setTextAlignment:NSTextAlignmentLeft];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)forgotButtonTap:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSString* email         = emailTextField.text;
    
    NSMutableArray* invalidEntries = [[NSMutableArray alloc] init];
    
    if( email.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Email", @"")];
    }
    else
    {
//        if (![SettingsClass isValidEmail:email])
//        {
//            [invalidEntries addObject:@"Email"];
//        }
    }
    
    if( invalidEntries.count > 0 )
    {
        // Atleast one has invalid data
        UIAlertView* alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"Insufficient input", @"")];
        NSMutableString* alertMsg = [[NSMutableString alloc] initWithString:NSLocalizedString(@"Please enter valid input for - ", @"")];
        {
            for( int i = 0; i < invalidEntries.count; ++i )
            {
                if( i == invalidEntries.count - 1 )
                {
                    [alertMsg appendFormat:@"%@.", invalidEntries[i]];
                }
                else
                {
                    [alertMsg appendFormat:@"%@, ", invalidEntries[i]];
                }
            }
        }
        [alert setMessage:alertMsg];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
        [alert show];
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        [paramDict setObject:@"1" forKey:@"forget_pass"];
        [paramDict setObject:emailTextField.text forKey:@"email"];
        if ([SettingsClass isLanguageArabic])
        {
            [paramDict setObject:@"ar" forKey:@"lang"];
        }
        else
        {
            [paramDict setObject:@"en" forKey:@"lang"];
        }

        [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_ForgotPassword OnCompletion:^(id returnDict, NSError *error) {
            
            if (!error)
            {
                ConfirmationViewController* confirmationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmationViewController"];
                confirmationViewController.viewType=@"";
                confirmationViewController.dataDict=[NSMutableDictionary dictionaryWithDictionary:returnDict[@"user"][0]];
                [self.navigationController pushViewController:confirmationViewController animated:YES];
                emailTextField.text=@"";
                
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }];
        
    }

}

- (IBAction)cancelButtonTap:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark KeyBoardUp2


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (BOOL)textFieldShouldEndEditing:(UITextField*)textField
{
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
//    [self animateTextField:textField up:NO];
    [textField resignFirstResponder];
    
    return YES;
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait){
        
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
        if(up) {
            int moveUpValue = self.view.frame.size.height-temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(self.view.frame.size.height-moveUpValue-100);
        }
    }
    else
    {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(768-moveUpValue-100);
        }
        
    }
    if(animatedDis>0)
    {
        const int movementDistance = animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        if (orientation == UIInterfaceOrientationPortrait){
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationLandscapeLeft) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else {
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        
        [UIView commitAnimations];
    }
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
    return YES;
}
@end
