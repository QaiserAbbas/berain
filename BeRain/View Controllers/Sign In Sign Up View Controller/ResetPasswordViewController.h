//
//  ResetPasswordViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/14/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface ResetPasswordViewController : BaseViewController
{
    IBOutlet UITextField *passwordTextfield;
    IBOutlet UITextField *rePasswordTextfield;
    IBOutlet UIButton *resetButton;
    IBOutlet UIButton *cancelButton;
    IBOutletCollection(UIImageView) NSArray *textFieldBG;

}
@property (strong, nonatomic) NSMutableDictionary *dataDict;

- (IBAction)cancelButtonTap:(UIButton *)sender;
- (IBAction)resetButtonTap:(UIButton *)sender;
@end
