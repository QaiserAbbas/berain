//
//  ForgotPasswordViewController.h
//  GoldApp
//
//  Created by Qaiser Abbas on 19/04/2016.
//  Copyright © 2016 Bir Al Sabia. All rights reserved.
//

#import "BaseViewController.h"

@interface ForgotPasswordViewController : BaseViewController
{
    CGFloat animatedDis;

    IBOutlet UITextField *emailTextField;
    IBOutlet UIImageView *emailImageView;
    IBOutlet UIButton *submitButton;
    IBOutlet UIButton *cancelButton;
}
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) UIImage *image;

- (IBAction)forgotButtonTap:(UIButton *)sender;
- (IBAction)cancelButtonTap:(UIButton *)sender;
@end
