//
//  LoginViewController.h
//  GoldApp
//
//  Created by Qaiser Abbas on 18/04/2016.
//  Copyright © 2016 Bir Al Sabia. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginViewController : BaseViewController<UITextFieldDelegate>
{
    CGFloat animatedDis;
    IBOutlet UIImageView *emailImageView;
    IBOutlet UIImageView *passwrodImageView;
    IBOutlet UIButton *loginButton;
    IBOutlet UITextField *phoneNumberTextField;
    IBOutlet UITextField *passwordTextField;
    
    IBOutlet UITextField *emailTextField;
    IBOutlet UIImageView *emailImageViewForget;
    IBOutlet UIButton *submitButton;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIView *forgotPopUpView;

    IBOutlet UIButton *skipButton;
    IBOutlet UILabel *proceedLabel;
    IBOutlet UIButton *signUpButton;
    IBOutlet UIButton *forgotPassButton;
}
@property (strong, nonatomic) NSString *viewType;

- (IBAction)loginButtonTap:(UIButton *)sender;
- (IBAction)forgotButtonTapped:(UIButton *)sender;
- (IBAction)cancelButtonTap:(UIButton *)sender;

@end
