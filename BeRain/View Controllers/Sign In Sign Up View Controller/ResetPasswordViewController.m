//
//  ResetPasswordViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/14/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "LoginViewController.h"

@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    for (UIImageView*imageView in textFieldBG)
    {
        imageView.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
        imageView.layer.cornerRadius=5;
        imageView.layer.borderWidth=1.0;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderColor=[[UIColor purpleColor] CGColor];
    }
    resetButton.layer.cornerRadius=resetButton.frame.size.height/2;
    resetButton.layer.masksToBounds = YES;
    cancelButton.layer.cornerRadius=cancelButton.frame.size.height/2;
    cancelButton.layer.masksToBounds = YES;

    if ([SettingsClass isLanguageArabic])
    {
        [passwordTextfield setTextAlignment:NSTextAlignmentRight];
        [rePasswordTextfield setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [passwordTextfield setTextAlignment:NSTextAlignmentLeft];
        [rePasswordTextfield setTextAlignment:NSTextAlignmentLeft];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButtonTap:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resetButtonTap:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSString* password         = passwordTextfield.text;
    NSString* rePassword         = rePasswordTextfield.text;
    
    NSMutableArray* invalidEntries = [[NSMutableArray alloc] init];
    
    if( password.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Password", @"")];
    }
    if( rePassword.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Re Password", @"")];
    }
    
    if( invalidEntries.count > 0 )
    {
        // Atleast one has invalid data
        UIAlertView* alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"Insufficient input", @"")];
        NSMutableString* alertMsg = [[NSMutableString alloc] initWithString:NSLocalizedString(@"Please enter valid input for - ", @"")];
        {
            for( int i = 0; i < invalidEntries.count; ++i )
            {
                if( i == invalidEntries.count - 1 )
                {
                    [alertMsg appendFormat:@"%@.", invalidEntries[i]];
                }
                else
                {
                    [alertMsg appendFormat:@"%@, ", invalidEntries[i]];
                }
            }
        }
        [alert setMessage:alertMsg];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
        [alert show];
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *lang=@"";
        
        if ([SettingsClass isLanguageArabic])
        {
            lang=@"ar";
        }
        else
        {
            lang=@"en";
        }

        NSString *url=[NSString stringWithFormat:@"activeAccountVerify/%@/%@/%@",self.dataDict[@"user_id"],passwordTextfield.text,lang];

        [GeneralWebservices webserviceMainSplashCall:nil webserviceName:url OnCompletion:^(id returnDict, NSError *error) {
            
            if (!error)
            {
                LoginViewController* loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:loginViewController animated:YES];
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }];
        
    }

}
-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
//    for (int i = 0; i < [string length]; i++)
//    {
//        unichar c = [string characterAtIndex:i];
//        if ([myCharSet characterIsMember:c])
//        {
//            return NO;
//        }
//    }
    return YES;
}

@end
