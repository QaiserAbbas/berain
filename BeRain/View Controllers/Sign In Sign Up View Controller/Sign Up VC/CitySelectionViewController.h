//
//  CitySelectionViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 11/28/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface CitySelectionViewController : BaseViewController
{
    IBOutlet UIButton *cityButton;
    IBOutlet UIButton *nextButton;
    NSMutableArray *cityArray;
    NSInteger slectedindex;
    UIImage *radioImage;
    UIImage *radioCheckImage;
}
- (IBAction)citySelectionButtonTap:(UIButton *)sender;
- (IBAction)nextButtonTap:(UIButton *)sender;
@end
