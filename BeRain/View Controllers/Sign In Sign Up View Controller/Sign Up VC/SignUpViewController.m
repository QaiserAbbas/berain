//
//  SignUpViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 11/28/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "SignUpViewController.h"
#import "LoginViewController.h"
#import "ConfirmationViewController.h"

@interface SignUpViewController ()<UITextFieldDelegate>


@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    for (UIImageView*imageView in textFieldBG)
    {
        imageView.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
        imageView.layer.cornerRadius=5;
        imageView.layer.borderWidth=1.0;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderColor=[[UIColor purpleColor] CGColor];
    }
    nextButton.layer.backgroundColor=[[UIColor clearColor] CGColor];
    nextButton.layer.cornerRadius=5;
    nextButton.layer.borderWidth=2.0;
    nextButton.layer.masksToBounds = YES;
    nextButton.layer.borderColor=[[UIColor purpleColor] CGColor];
    
    if ([SettingsClass isLanguageArabic])
    {
        [firstNameTextField setTextAlignment:NSTextAlignmentRight];
        [lastNameTextField setTextAlignment:NSTextAlignmentRight];
        [emailTextField setTextAlignment:NSTextAlignmentRight];
        [passwordTextField setTextAlignment:NSTextAlignmentRight];
        [mobileTextField setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [firstNameTextField setTextAlignment:NSTextAlignmentLeft];
        [lastNameTextField setTextAlignment:NSTextAlignmentLeft];
        [emailTextField setTextAlignment:NSTextAlignmentLeft];
        [passwordTextField setTextAlignment:NSTextAlignmentLeft];
        [mobileTextField setTextAlignment:NSTextAlignmentLeft];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark KeyBoardUp2


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (BOOL)textFieldShouldEndEditing:(UITextField*)textField
{
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
//    [self animateTextField:textField up:NO];
    [textField resignFirstResponder];
    
    return YES;
}


-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    int limit=0;
    if (textField==passwordTextField)
    {
        limit=5;
    }
    if (newLength>limit)
    {
        [self changeTextFields:textField hidden:YES];
    }
    else
    {
//        [self changeTextFields:textField hidden:NO];
    }
    return YES;
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait){
        
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
        if(up) {
            int moveUpValue = self.view.frame.size.height-temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(self.view.frame.size.height-moveUpValue-100);
        }
    }
    else
    {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(768-moveUpValue-100);
        }
        
    }
    if(animatedDis>0)
    {
        const int movementDistance = animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        if (orientation == UIInterfaceOrientationPortrait){
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationLandscapeLeft) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else {
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        
        [UIView commitAnimations];
    }
}


-(void)changeTextFields:(UITextField *)textField hidden:(BOOL)hide
{
        if (textField == firstNameTextField)
        {
            [firstNameLabel setHidden:hide];
//            [firstNameTextField setPlaceholder:@"First Name"];
        }
        else if (textField == lastNameTextField)
        {
            [lastNameLabel setHidden:hide];
//            [lastNameTextField setPlaceholder:@"Last Name"];
        }
        else if (textField == mobileTextField)
        {
            [mobileLabel setHidden:hide];
//            [mobileTextField setPlaceholder:@"Company"];
        }
        else if (textField == emailTextField)
        {
            [emailLabel setHidden:hide];
//            [emailTextField setPlaceholder:@"Email"];
        }
        else if (textField == passwordTextField)
        {
            [passwordLabel setHidden:hide];
//            [passwordTextField setPlaceholder:@"Password"];
        }
}
-(BOOL)checkValidInfo
{
    BOOL allOK=YES;
    
    if ([firstNameTextField.text length]==0)
    {
        [firstNameLabel setHidden:NO];
//        [firstNameTextField setPlaceholder:@"First Name"];
        allOK=NO;
    }
    if ([lastNameTextField.text length]==0)
    {
        [lastNameLabel setHidden:NO];
//        [lastNameTextField setPlaceholder:@"Last Name"];
        allOK=NO;
    }
    if ([mobileTextField.text length]==0)
    {
        [mobileLabel setHidden:NO];
//        [mobileTextField setPlaceholder:@"Mobile"];
        allOK=NO;
    }
    if ([emailTextField.text length]==0)
    {
        [emailLabel setHidden:NO];
//        [emailTextField setPlaceholder:@"E-mail"];
        allOK=NO;
    }
    if ([passwordTextField.text length]==0)
    {
        [passwordLabel setHidden:NO];
//        [passwordTextField setPlaceholder:@"Password"];
        allOK=NO;
    }

    return allOK;
}
- (IBAction)registerButtonTap:(UIButton *)sender
{
    if ([self checkValidInfo])
    {
        if (![SettingsClass isValidEmail:emailTextField.text])
        {
            
        }

        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSString *today=[dateFormat stringFromDate:[NSDate date]];

//        NSMutableDictionary*cityDict=[NSMutableDictionary dictionaryWithDictionary:self.signUpDictFinal[@"city"][@"city"]];
        City *city=self.signUpDictFinal[@"city"][@"city"];
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        [paramDict setObject:firstNameTextField.text forKey:@"frist_name"];
        [paramDict setObject:lastNameTextField.text forKey:@"last_name"];
        [paramDict setObject:emailTextField.text forKey:@"email"];
        [paramDict setObject:mobileTextField.text forKey:@"mobile"];
        [paramDict setObject:passwordTextField.text forKey:@"pass"];
        [paramDict setObject:self.signUpDictFinal[@"channel"] forKey:@"client_type"];
        [paramDict setObject:@"" forKey:@"company_name"];
        [paramDict setObject:self.signUpDictFinal[@"subChannel"] forKey:@"client_sub_type"];
        [paramDict setObject:@"" forKey:@"user_id"];
        [paramDict setObject:today forKey:@"register_date"];
        [paramDict setObject:city.city_id forKey:@"city_id"];

        [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_SignUp OnCompletion:^(id returnDict, NSError *error) {
            
            if (!error && returnDict)
            {
                ConfirmationViewController* confirmationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmationViewController"];
                confirmationViewController.viewType=@"signup";
                confirmationViewController.dataDict=[NSMutableDictionary dictionaryWithDictionary:returnDict];
                 [self.navigationController pushViewController:confirmationViewController animated:YES];
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }];
    }
}
@end
