//
//  ChanellSelectionViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 11/28/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ChanellSelectionViewController.h"
#import "SignUpViewController.h"

@interface ChanellSelectionViewController ()<LGAlertViewDelegate>

@end

@implementation ChanellSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    channelButton.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
    channelButton.layer.cornerRadius=5;
    channelButton.layer.borderWidth=1.0;
    channelButton.layer.masksToBounds = YES;
    channelButton.layer.borderColor=[[UIColor purpleColor] CGColor];

    subChannelButton.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
    subChannelButton.layer.cornerRadius=5;
    subChannelButton.layer.borderWidth=1.0;
    subChannelButton.layer.masksToBounds = YES;
    subChannelButton.layer.borderColor=[[UIColor purpleColor] CGColor];
    
    nextButton.layer.backgroundColor=[[UIColor clearColor] CGColor];
    nextButton.layer.cornerRadius=5;
    nextButton.layer.borderWidth=2.0;
    nextButton.layer.masksToBounds = YES;
    nextButton.layer.borderColor=[[UIColor purpleColor] CGColor];

    channelArray=[NSMutableArray array];
//    [channelArray addObject:@"Home Delivery"];
    slectedindexChannel=0;
//    [channelButton setTitle:channelArray[slectedindexChannel][@"title_en"] forState:UIControlStateNormal];
    
    subChannelArray=[NSMutableArray array];
//    [subChannelArray addObject:@"House"];
//    [subChannelArray addObject:@"School"];
//    [subChannelArray addObject:@"Mosque"];
//    [subChannelArray addObject:@"Office (Ofc)"];

    slectedindexSubChannel=0;
//    [subChannelButton setTitle:subChannelArray[slectedindexSubChannel] forState:UIControlStateNormal];

    radioImage=[UIImage imageNamed:@"radio"];
    radioCheckImage=[UIImage imageNamed:@"radio-check"];
    channelArray =[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayChannel]];
    subChannelArray =[NSMutableArray arrayWithArray:[[DataManager sharedManager] arraySubChannel]];
    if (channelArray.count>0)
    {
        Channel *channel=channelArray[slectedindexChannel];
        
        if ([SettingsClass isLanguageArabic])
        {
            [channelButton setTitle:channel.title_ar forState:UIControlStateNormal];
        }
        else
        {
            [channelButton setTitle:channel.title_en forState:UIControlStateNormal];
        }
  
    }
    if (subChannelArray.count>0)
    {
        SubChannel *subChannel=subChannelArray[slectedindexSubChannel];
        
        if ([SettingsClass isLanguageArabic])
        {
            [subChannelButton setTitle:subChannel.title_ar forState:UIControlStateNormal];
        }
        else
        {
            [subChannelButton setTitle:subChannel.title_en forState:UIControlStateNormal];
        }

    }
    
    if ([SettingsClass isLanguageArabic])
    {
        [channelButton.titleLabel setTextAlignment:NSTextAlignmentRight];
        channelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        channelButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        
        [subChannelButton.titleLabel setTextAlignment:NSTextAlignmentRight];
        subChannelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        subChannelButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);

    }
    else
    {
        [channelButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
        channelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        channelButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        
        [subChannelButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
        subChannelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        subChannelButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);

    }


//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
//    [paramDict setObject:@"1" forKey:@"type_id"];
//    
//    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_GetChannels OnCompletion:^(id returnDict, NSError *error) {
//        
//        if (!error)
//        {
//            channelArray =[NSMutableArray arrayWithArray:returnDict[@"rows"]];
//            if ([SettingsClass isLanguageArabic])
//            {
//                [channelButton setTitle:channelArray[slectedindexChannel][@"title_ar"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [channelButton setTitle:channelArray[slectedindexChannel][@"title_en"] forState:UIControlStateNormal];
//            }
//
//        }
//        else
//        {
//            UIAlertView* alert = [[UIAlertView alloc] init];
//            [alert setTitle:@"Berain"];
//            [alert setMessage:returnDict[@"mMessage"]];
//            [alert addButtonWithTitle:@"OK"];
//            [alert show];
//        }
//        if (bothDone)
//        {
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        }
//        bothDone=YES;
//    }];
////
//    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_GetSubChannels OnCompletion:^(id returnDict, NSError *error) {
//        
//        if (!error)
//        {
//            subChannelArray =[NSMutableArray arrayWithArray:returnDict[@"rows"]];
//            if ([SettingsClass isLanguageArabic])
//            {
//                [subChannelButton setTitle:subChannelArray[slectedindexSubChannel][@"title_ar"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [subChannelButton setTitle:subChannelArray[slectedindexSubChannel][@"title_en"] forState:UIControlStateNormal];
//            }
//
//        }
//        else
//        {
//            UIAlertView* alert = [[UIAlertView alloc] init];
//            [alert setTitle:@"Berain"];
//            [alert setMessage:returnDict[@"mMessage"]];
//            [alert addButtonWithTitle:@"OK"];
//            [alert show];
//        }
//        if (bothDone)
//        {
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        }
//        bothDone=YES;
//    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)subChannelButtonTap:(UIButton *)sender
{
    NSMutableArray *imagesArray=[NSMutableArray array];
    NSMutableArray *namesArray=[NSMutableArray array];

    for (int i=0; i<subChannelArray.count; i++)
    {
        SubChannel *subChannel=subChannelArray[i];

        if (i==slectedindexSubChannel)
        {
            [imagesArray addObject:radioCheckImage];
        }
        else
        {
            [imagesArray addObject:radioImage];
        }
        if ([SettingsClass isLanguageArabic])
        {
            [namesArray addObject:subChannel.title_ar];
        }
        else
        {
            [namesArray addObject:subChannel.title_en];
        }

    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Select Sub Channel", @"")
                                                        message:@""
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:namesArray
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    alertView.buttonsIconImages = imagesArray;
    alertView.buttonsIconImagesHighlighted = imagesArray;
    alertView.buttonsIconPosition = LGAlertViewButtonIconPositionRight;
    if ([SettingsClass isLanguageArabic])
    {
        alertView.buttonsTextAlignment = NSTextAlignmentRight;
    }
    else
    {
        alertView.buttonsTextAlignment = NSTextAlignmentLeft;
    }
    
    alertView.cancelButtonTextAlignment = NSTextAlignmentCenter;
    alertView.tag=2;
    [alertView showAnimated:YES completionHandler:nil];

}

- (IBAction)channelButtonTap:(UIButton *)sender
{
    NSMutableArray *imagesArray=[NSMutableArray array];
    NSMutableArray *namesArray=[NSMutableArray array];

    for (int i=0; i<channelArray.count; i++)
    {
        Channel *channel=channelArray[i];

        if (i==slectedindexChannel)
        {
            [imagesArray addObject:radioCheckImage];
        }
        else
        {
            [imagesArray addObject:radioImage];
        }
        if ([SettingsClass isLanguageArabic])
        {
            [namesArray addObject:channel.title_ar];
        }
        else
        {
            [namesArray addObject:channel.title_en];
        }

    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Select Channel", @"")
                                                        message:@""
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:namesArray
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    alertView.buttonsIconImages = imagesArray;
    alertView.buttonsIconImagesHighlighted = imagesArray;
    alertView.buttonsIconPosition = LGAlertViewButtonIconPositionRight;
    if ([SettingsClass isLanguageArabic])
    {
        alertView.buttonsTextAlignment = NSTextAlignmentRight;
    }
    else
    {
        alertView.buttonsTextAlignment = NSTextAlignmentLeft;
    }
    
    alertView.cancelButtonTextAlignment = NSTextAlignmentCenter;
    alertView.tag=1;
    [alertView showAnimated:YES completionHandler:nil];

}

- (IBAction)nextButtonTap:(UIButton *)sender
{
    SignUpViewController* signUpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    signUpViewController.signUpDictFinal=[NSMutableDictionary dictionary];
    SubChannel *subChannel=subChannelArray[slectedindexSubChannel];
    Channel *channel=channelArray[slectedindexChannel];

    [signUpViewController.signUpDictFinal setObject:self.signUpDict forKey:@"city"];
    [signUpViewController.signUpDictFinal setObject:channel.channel_id forKey:@"channel"];
    [signUpViewController.signUpDictFinal setObject:subChannel.subChannel_id forKey:@"subChannel"];
    
    [self.navigationController pushViewController:signUpViewController animated:YES];
}
-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    if (alertView.tag==1)
    {
        slectedindexChannel=index;
        Channel *channel=channelArray[slectedindexChannel];

        if ([SettingsClass isLanguageArabic])
        {
            [channelButton setTitle:channel.title_ar forState:UIControlStateNormal];
        }
        else
        {
            [channelButton setTitle:channel.title_en forState:UIControlStateNormal];
        }

    }
    else if (alertView.tag==2)
    {
        slectedindexSubChannel=index;
        SubChannel *subChannel=subChannelArray[slectedindexSubChannel];

        if ([SettingsClass isLanguageArabic])
        {
            [subChannelButton setTitle:subChannel.title_ar forState:UIControlStateNormal];
        }
        else
        {
            [subChannelButton setTitle:subChannel.title_en forState:UIControlStateNormal];
        }
    }
}
@end
