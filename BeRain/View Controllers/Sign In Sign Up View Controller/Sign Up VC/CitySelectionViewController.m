//
//  CitySelectionViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 11/28/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "CitySelectionViewController.h"
#import "ChanellSelectionViewController.h"

@interface CitySelectionViewController ()<LGAlertViewDelegate>

@end

@implementation CitySelectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    cityButton.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
    cityButton.layer.cornerRadius=5;
    cityButton.layer.borderWidth=1.0;
    cityButton.layer.masksToBounds = YES;
    cityButton.layer.borderColor=[[UIColor purpleColor] CGColor];

    nextButton.layer.backgroundColor=[[UIColor clearColor] CGColor];
    nextButton.layer.cornerRadius=5;
    nextButton.layer.borderWidth=2.0;
    nextButton.layer.masksToBounds = YES;
    nextButton.layer.borderColor=[[UIColor purpleColor] CGColor];

    cityArray=[NSMutableArray array];
    slectedindex=0;
    radioImage=[UIImage imageNamed:@"radio"];
    radioCheckImage=[UIImage imageNamed:@"radio-check"];
    cityArray =[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCity]];
    if (cityArray.count>0)
    {
        City *city=cityArray[slectedindex];
        
        if ([SettingsClass isLanguageArabic])
        {
            [cityButton setTitle:city.city_title_ar forState:UIControlStateNormal];
        }
        else
        {
            [cityButton setTitle:city.city_title_en forState:UIControlStateNormal];
        }
    }
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        
//    [GeneralWebservices webserviceMainSplashCall:nil webserviceName:Webservice_AllCities OnCompletion:^(id returnDict, NSError *error) {
//        
//        if (!error)
//        {
//            cityArray =[NSMutableArray arrayWithArray:returnDict[@"rows"]];
//            if ([SettingsClass isLanguageArabic])
//            {
//                [cityButton setTitle:cityArray[slectedindex][@"city_title_ar"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [cityButton setTitle:cityArray[slectedindex][@"city_title_en"] forState:UIControlStateNormal];
//            }
//
//        }
//        else
//        {
//            UIAlertView* alert = [[UIAlertView alloc] init];
//            [alert setTitle:@"Berain"];
//            [alert setMessage:@"Please check server"];
//            [alert addButtonWithTitle:@"OK"];
//            [alert show];
//        }
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        
//    }];
    if ([SettingsClass isLanguageArabic])
    {
        [cityButton.titleLabel setTextAlignment:NSTextAlignmentRight];
        cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        cityButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);

    }
    else
    {
        [cityButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
        cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        cityButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)citySelectionButtonTap:(UIButton *)sender
{
    NSMutableArray *imagesArray=[NSMutableArray array];
    NSMutableArray *namesArray=[NSMutableArray array];
    for (int i=0; i<cityArray.count; i++)
    {
        City *city=cityArray[i];

        if (i==slectedindex)
        {
            [imagesArray addObject:radioCheckImage];
        }
        else
        {
            [imagesArray addObject:radioImage];
        }
        if ([SettingsClass isLanguageArabic])
        {
            [namesArray addObject:city.city_title_ar];
        }
        else
        {
            [namesArray addObject:city.city_title_en];
        }

    }

    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Select City", @"")
                                                        message:@""
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:namesArray
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    alertView.buttonsIconImages = imagesArray;
    alertView.buttonsIconImagesHighlighted = imagesArray;
    alertView.buttonsIconPosition = LGAlertViewButtonIconPositionRight;
    
    if ([SettingsClass isLanguageArabic])
    {
        alertView.buttonsTextAlignment = NSTextAlignmentRight;
    }
    else
    {
        alertView.buttonsTextAlignment = NSTextAlignmentLeft;
    }

    alertView.cancelButtonTextAlignment = NSTextAlignmentCenter;
    
    [alertView showAnimated:YES completionHandler:nil];
}

- (IBAction)nextButtonTap:(UIButton *)sender
{
    ChanellSelectionViewController* chanellSelectionViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChanellSelectionViewController"];
    chanellSelectionViewController.signUpDict=[NSMutableDictionary dictionary];

    [chanellSelectionViewController.signUpDict setObject:cityArray[slectedindex] forKey:@"city"];
    [self.navigationController pushViewController:chanellSelectionViewController animated:YES];
}
-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    slectedindex=index;
    City *city=cityArray[slectedindex];

    if ([SettingsClass isLanguageArabic])
    {
        [cityButton setTitle:city.city_title_ar forState:UIControlStateNormal];
    }
    else
    {
        [cityButton setTitle:city.city_title_en forState:UIControlStateNormal];
    }

}
@end
