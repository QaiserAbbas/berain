//
//  ChanellSelectionViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 11/28/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface ChanellSelectionViewController : BaseViewController
{
    IBOutlet UIButton *channelButton;
    IBOutlet UIButton *subChannelButton;
    IBOutlet UIButton *nextButton;

    NSMutableArray *channelArray;
    NSInteger slectedindexChannel;
    NSMutableArray *subChannelArray;
    NSInteger slectedindexSubChannel;

    UIImage *radioImage;
    UIImage *radioCheckImage;

    BOOL bothDone;
}
@property (strong, nonatomic) NSMutableDictionary *signUpDict;

- (IBAction)subChannelButtonTap:(UIButton *)sender;
- (IBAction)channelButtonTap:(UIButton *)sender;
- (IBAction)nextButtonTap:(UIButton *)sender;


@end
