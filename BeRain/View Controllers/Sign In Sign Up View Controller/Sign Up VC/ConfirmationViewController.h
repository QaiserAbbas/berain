//
//  ConfirmationViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/12/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface ConfirmationViewController : BaseViewController
{
    IBOutlet UIButton *verifyButton;
    IBOutlet UITextField *codeTextField;
    IBOutlet UIButton *resendButton;
}
@property (strong, nonatomic) NSString *viewType;
@property (strong, nonatomic) NSMutableDictionary *dataDict;

- (IBAction)resendButtonTap:(UIButton *)sender;
- (IBAction)verifyButtonTap:(UIButton *)sender;
@end
