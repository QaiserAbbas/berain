//
//  ConfirmationViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/12/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "ConfirmationViewController.h"
#import "LoginViewController.h"
#import "ResetPasswordViewController.h"

@interface ConfirmationViewController ()

@end

@implementation ConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    verifyButton.layer.backgroundColor=[[UIColor clearColor] CGColor];
    verifyButton.layer.cornerRadius=5;
    verifyButton.layer.borderWidth=2.0;
    verifyButton.layer.masksToBounds = YES;
    verifyButton.layer.borderColor=[[UIColor purpleColor] CGColor];
    if ([SettingsClass isLanguageArabic])
    {
        [codeTextField setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [codeTextField setTextAlignment:NSTextAlignmentLeft];
    }
    [verifyButton setEnabled:NO];

    UIFont *font =[UIFont fontWithName:@"DroidArabicKufi" size:15];
    
    UIColor *greenColor = [UIColor darkGrayColor];

    {
        //    [self.orderNumber setText:object[@"ord_id"]];
        NSString*resendFirstStr=NSLocalizedString(@"Didn't get it?", @"");
        NSString*resendSecondStr=NSLocalizedString(@"Click here to resend", @"");
        NSString *orderText = [NSString stringWithFormat:@"%@    %@",
                               resendFirstStr,
                               resendSecondStr];
        
        // If attributed text is supported (iOS6+)
        if ([resendButton.titleLabel respondsToSelector:@selector(setAttributedText:)]) {
            
            // Define general attributes for the entire text
            NSDictionary *attribs = @{
                                      NSForegroundColorAttributeName:   greenColor,
                                      NSFontAttributeName: font
                                      };
            NSMutableAttributedString *attributedText =
            [[NSMutableAttributedString alloc] initWithString:orderText
                                                   attributes:attribs];
            
            NSRange redTextRange = [orderText rangeOfString:resendSecondStr];
            [attributedText addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:redTextRange];

            [resendButton setAttributedTitle:attributedText forState:UIControlStateNormal];
        }
        else
        {
            [resendButton setTitle:orderText forState:UIControlStateNormal];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)resendButtonTap:(UIButton *)sender
{
    if ([self.viewType isEqualToString:@"signup"])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        NSString *url=[NSString stringWithFormat:@"resend_code/%@/en",self.dataDict[@"id"]];
        
        [GeneralWebservices webserviceMainSplashCallGET:nil webserviceName:url OnCompletion:^(id returnDict, NSError *error) {
            if (!error)
            {
                
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
            }
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        }];
    }
    else
    {
        
    }
}

- (IBAction)verifyButtonTap:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSString* code         = codeTextField.text;
    
    NSMutableArray* invalidEntries = [[NSMutableArray alloc] init];
    
    if( code.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Code", @"")];
    }
    
    if( invalidEntries.count > 0 )
    {
        // Atleast one has invalid data
        UIAlertView* alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"Insufficient input", @"")];
        NSMutableString* alertMsg = [[NSMutableString alloc] initWithString:NSLocalizedString(@"Please enter valid input for - ", @"")];
        {
            for( int i = 0; i < invalidEntries.count; ++i )
            {
                if( i == invalidEntries.count - 1 )
                {
                    [alertMsg appendFormat:@"%@.", invalidEntries[i]];
                }
                else
                {
                    [alertMsg appendFormat:@"%@, ", invalidEntries[i]];
                }
            }
        }
        [alert setMessage:NSLocalizedString(alertMsg, @"")];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
        [alert show];
    }
    else
    {
            if ([self.viewType isEqualToString:@"signup"])
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                NSString *lang=@"";
                
                if ([SettingsClass isLanguageArabic])
                {
                    lang=@"ar";
                }
                else
                {
                    lang=@"en";
                }

                NSString *url=[NSString stringWithFormat:@"activeAccountVerify/%@/%@/%@",self.dataDict[@"id"],codeTextField.text,lang];
                
                [GeneralWebservices webserviceMainSplashCall:nil webserviceName:url OnCompletion:^(id returnDict, NSError *error) {
                    if (!error)
                    {
                        NSLog(@"%@",returnDict);
                        LoginViewController* loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                        [self.navigationController pushViewController:loginViewController animated:YES];
                    }
                    else
                    {
                        UIAlertView* alert = [[UIAlertView alloc] init];
                        [alert setTitle:NSLocalizedString(@"Berain", @"")];
                        [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                        [alert show];
                    }

                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                }];
            }
            else
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
                [paramDict setObject:[NSString stringWithFormat:@"%@",self.dataDict[@"email"]] forKey:@"email"];
                [paramDict setObject:codeTextField.text forKey:@"verify_code"];
                if ([SettingsClass isLanguageArabic])
                {
                    [paramDict setObject:@"ar" forKey:@"lang"];
                }
                else
                {
                    [paramDict setObject:@"en" forKey:@"lang"];
                }
                [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_ForgotPassword OnCompletion:^(id returnDict, NSError *error) {
                    
                    if (!error)
                    {
                        ResetPasswordViewController* resetPasswordViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordViewController"];
                        resetPasswordViewController.dataDict=self.dataDict;

                        [self.navigationController pushViewController:resetPasswordViewController animated:YES];
                    }
                    else
                    {
                        UIAlertView* alert = [[UIAlertView alloc] init];
                        [alert setTitle:NSLocalizedString(@"Berain", @"")];
                        [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                        [alert show];
                    }
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    
                }];
            }
    }
}


-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
    for (int i = 0; i < [string length]; i++)
    {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c])
        {
            return NO;
        }
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    if (newLength>5)
    {
        [verifyButton setEnabled:YES];
    }
    else
    {
        [verifyButton setEnabled:NO];
    }
    if (newLength>6)
    {
        return NO;
    }
    else
    {
        return YES;
    }


    return YES;
}

@end
