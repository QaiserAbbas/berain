//
//  SignUpViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 11/28/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"

@interface SignUpViewController : BaseViewController
{
    CGFloat animatedDis;

    IBOutlet UIButton *nextButton;
    IBOutletCollection(UIImageView) NSArray *textFieldBG;
    
    IBOutlet UITextField *firstNameTextField;
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UITextField *mobileTextField;
    
    IBOutlet UILabel *firstNameLabel;
    IBOutlet UILabel *lastNameLabel;
    IBOutlet UILabel *emailLabel;
    IBOutlet UILabel *passwordLabel;
    IBOutlet UILabel *mobileLabel;
}
@property (strong, nonatomic) NSMutableDictionary *signUpDictFinal;
- (IBAction)registerButtonTap:(UIButton *)sender;

@end
