//
//  LoginViewController.m
//  GoldApp
//
//  Created by Qaiser Abbas on 18/04/2016.
//  Copyright © 2016 Bir Al Sabia. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "ForgotPasswordViewController.h"
#import "ConfirmationViewController.h"
#import "ShoppingCartViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    emailImageView.layer.backgroundColor=[[UIColor whiteColor] CGColor];
    emailImageView.layer.cornerRadius=5;
    emailImageView.layer.borderWidth=1.0;
    emailImageView.layer.masksToBounds = YES;
    emailImageView.layer.borderColor=[[UIColor purpleColor] CGColor];
    
    passwrodImageView.layer.backgroundColor=[[UIColor whiteColor] CGColor];
    passwrodImageView.layer.cornerRadius=5;
    passwrodImageView.layer.borderWidth=1.0;
    passwrodImageView.layer.masksToBounds = YES;
    passwrodImageView.layer.borderColor=[[UIColor purpleColor] CGColor];

    loginButton.layer.backgroundColor=[[UIColor clearColor] CGColor];
    loginButton.layer.cornerRadius=5;
    loginButton.layer.borderWidth=1.0;
    loginButton.layer.masksToBounds = YES;
    loginButton.layer.borderColor=[[UIColor purpleColor] CGColor];

    
    emailImageViewForget.layer.backgroundColor=[[UIColor whiteColor] CGColor];
    emailImageViewForget.layer.cornerRadius=5;
    emailImageViewForget.layer.borderWidth=1.0;
    emailImageViewForget.layer.masksToBounds = YES;
    emailImageViewForget.layer.borderColor=[[UIColor grayColor] CGColor];
    
    submitButton.layer.cornerRadius=submitButton.frame.size.height/2;
    submitButton.layer.masksToBounds = YES;
    cancelButton.layer.cornerRadius=cancelButton.frame.size.height/2;
    cancelButton.layer.masksToBounds = YES;
    
//    skipButton.underlinePosition = 1;
    
    NSString *text = skipButton.titleLabel.text;
    
    // If attributed text is supported (iOS6+)
    if ([skipButton.titleLabel respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName:   skipButton.titleLabel.textColor,
                                  NSFontAttributeName: skipButton.titleLabel.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(text, @"")
                                               attributes:attribs];
        [attributedText addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [attributedText length])];

        
        skipButton.titleLabel.attributedText =attributedText;

    }
    
    
    NSString *textSignUp = signUpButton.titleLabel.text;
    
    // If attributed text is supported (iOS6+)
    if ([signUpButton.titleLabel respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName:   signUpButton.titleLabel.textColor,
                                  NSFontAttributeName: signUpButton.titleLabel.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(textSignUp, @"")
                                               attributes:attribs];
        [attributedText addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [attributedText length])];
        
        
        signUpButton.titleLabel.attributedText =attributedText;
        
    }

    
    NSString *textForgetPass = forgotPassButton.titleLabel.text;
    
    // If attributed text is supported (iOS6+)
    if ([forgotPassButton.titleLabel respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName:   forgotPassButton.titleLabel.textColor,
                                  NSFontAttributeName: forgotPassButton.titleLabel.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(textForgetPass, @"")
                                               attributes:attribs];
        [attributedText addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [attributedText length])];
        
        
        forgotPassButton.titleLabel.attributedText =attributedText;
        
    }
    if ([SettingsClass isLanguageArabic])
    {
        [emailTextField setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [emailTextField setTextAlignment:NSTextAlignmentLeft];
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self.viewType isEqualToString:@"payment"])
    {
        [skipButton setHidden:YES];
        [proceedLabel setHidden:NO];
    }
    else
    {
        [skipButton setHidden:NO];
        [proceedLabel setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonTap:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSString* email         = phoneNumberTextField.text;
    NSString* password      = passwordTextField.text;
    
    password    = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableArray* invalidEntries = [[NSMutableArray alloc] init];
    
    if( email.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Phone Number", @"")];
    }

    if( password.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Password", @"")];
    }
    
    if( invalidEntries.count > 0 )
    {
        // Atleast one has invalid data
        UIAlertView* alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"Insufficient input", @"")];
        NSMutableString* alertMsg = [[NSMutableString alloc] initWithString:NSLocalizedString(@"Please enter valid input for - ", @"")];
        {
            for( int i = 0; i < invalidEntries.count; ++i )
            {
                if( i == invalidEntries.count - 1 )
                {
                    [alertMsg appendFormat:@"%@.", invalidEntries[i]];
                }
                else
                {
                    [alertMsg appendFormat:@"%@, ", invalidEntries[i]];
                }
            }
        }
        [alert setMessage:alertMsg];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
        [alert show];
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        [paramDict setObject:phoneNumberTextField.text forKey:@"email"];
        [paramDict setObject:passwordTextField.text forKey:@"pass"];
        
        [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_SignIn OnCompletion:^(id returnDict, NSError *error) {
            
            if (returnDict[@"user"])
            {
                NSLog(@"%@",[returnDict[@"user"] class]);
                NSArray *weather = returnDict[@"user"];
                if (weather && weather.count > 0)
                {
                    [[DataManager sharedManager] saveUser:returnDict[@"user"][0]];
                    [[DataManager sharedManager] saveAddress:returnDict[@"address"]];
                    User* user = [[DataManager sharedManager] getUser];


                    if (user)
                    {
                        [[DataManager sharedManager] makeDefaultAddress:user.user_id];

                        [[SingletonClass sharedSingletonClass].settingsDictionary setObject:user forKey:@"User"];
                    }
                    if ([self.viewType isEqualToString:@"payment"])
                    {
                        ShoppingCartViewController* homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingCartViewController"];
                        homeVC.viewType=@"login";
                        [self.navigationController pushViewController:homeVC animated:YES];
                    }
                    else
                    {
                        HomeViewController* homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                        [self.navigationController pushViewController:homeVC animated:YES];
                    }
                }

            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        }];

    }
}

- (IBAction)forgotButtonTap:(UIButton *)sender
{
//    ForgotPasswordViewController*forgotPasswordViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
//    
//    forgotPasswordViewController.modalPresentationStyle = UIModalPresentationCustom;
//    [forgotPasswordViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    
//    UIImage *viewImage = [UIImage imageWithView:self.view];
//    UIImage *blurredImage = [viewImage blurredImageWithRadius:20.0 iterations:2 tintColor:[UIColor clearColor]];
//    forgotPasswordViewController.image = blurredImage;
//    
//    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];

    [forgotPopUpView setHidden:NO];
}

#pragma mark KeyBoardUp2


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (BOOL)textFieldShouldEndEditing:(UITextField*)textField
{
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
//    [self animateTextField:textField up:NO];
    [textField resignFirstResponder];

    return YES;
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait){
        
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
        if(up) {
            int moveUpValue = self.view.frame.size.height-temp.y+textField.frame.size.height;
            animatedDis = 264-(self.view.frame.size.height-moveUpValue-35);
        }
    }
    else if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(self.view.frame.size.height-moveUpValue-100);
        }
    }
    else
    {
        if(up) {
            int moveUpValue = temp.y+textField.frame.size.height;
            animatedDis = 352-(768-moveUpValue-100);
        }
        
    }
    if(animatedDis>0)
    {
        const int movementDistance = animatedDis;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        if (orientation == UIInterfaceOrientationPortrait){
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else if(orientation == UIInterfaceOrientationLandscapeLeft) {
            
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        else {
            self.view.frame = CGRectOffset( self.view.frame, 0, movement);
        }
        
        [UIView commitAnimations];
    }
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField!=passwordTextField)
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        return YES;
 
    }
    return YES;
}

- (IBAction)forgotButtonTapped:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSString* email         = emailTextField.text;
    
    NSMutableArray* invalidEntries = [[NSMutableArray alloc] init];
    
    if( email.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Email", @"")];
    }
    else
    {
//        if (![SettingsClass isValidEmail:email])
//        {
//            [invalidEntries addObject:@"Email"];
//        }
    }
    
    if( invalidEntries.count > 0 )
    {
        // Atleast one has invalid data
        UIAlertView* alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"Insufficient input", @"")];
        NSMutableString* alertMsg = [[NSMutableString alloc] initWithString:NSLocalizedString(@"Please enter valid input for - ", @"")];
        {
            for( int i = 0; i < invalidEntries.count; ++i )
            {
                if( i == invalidEntries.count - 1 )
                {
                    [alertMsg appendFormat:@"%@.", invalidEntries[i]];
                }
                else
                {
                    [alertMsg appendFormat:@"%@, ", invalidEntries[i]];
                }
            }
        }
        [alert setMessage:alertMsg];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
        [alert show];
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        [paramDict setObject:@"1" forKey:@"forget_pass"];
        [paramDict setObject:emailTextField.text forKey:@"email"];
        if ([SettingsClass isLanguageArabic])
        {
            [paramDict setObject:@"ar" forKey:@"lang"];
        }
        else
        {
            [paramDict setObject:@"en" forKey:@"lang"];
        }
        
        [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_ForgotPassword OnCompletion:^(id returnDict, NSError *error) {
            
            if (!error)
            {
                ConfirmationViewController* confirmationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmationViewController"];
                confirmationViewController.viewType=@"";
                confirmationViewController.dataDict=[NSMutableDictionary dictionaryWithDictionary:returnDict[@"user"][0]];
                [self.navigationController pushViewController:confirmationViewController animated:YES];
                emailTextField.text=@"";
                
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc] init];
                [alert setTitle:NSLocalizedString(@"Berain", @"")];
                [alert setMessage:NSLocalizedString(@"Please try again", @"")];
                [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
                [alert show];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }];
        
    }
    
}

- (IBAction)cancelButtonTap:(UIButton *)sender
{
    [forgotPopUpView setHidden:YES];
}

@end
