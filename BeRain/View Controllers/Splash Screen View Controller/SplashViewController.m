//
//  SplashViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/2/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "SplashViewController.h"
#import "LoginViewController.h"
#import "HomeViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentUser = [[DataManager sharedManager] getUser];
    if (currentUser)
    {
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        [paramDict setObject:@"headerinfo" forKey:@"check"];
        
        [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_TokenReferesh OnCompletion:^(id returnDict, NSError *error) {
            if (!error)
            {
                [SettingsClass setAuth_key:returnDict[@"auth"]];

                [self loadAll];
            }
        }];
    }
    else
    {
        [self loadAll];
    }

}
-(void)loadAll
{
    [GeneralWebservices webserviceMainSplashCall:nil webserviceName:Webservice_AllTips OnCompletion:^(id returnDict, NSError *error) {
        if (!error)
        {
            [[DataManager sharedManager] saveTips:returnDict[@"tips"]];
        }
        
        User* user = [[DataManager sharedManager] getUser];
        if (user)
        {
            [[SingletonClass sharedSingletonClass].settingsDictionary setObject:user forKey:@"User"];
            HomeViewController* homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [[SlideNavigationController sharedInstance] pushViewController:homeVC animated:YES];
        }
        else
        {
            LoginViewController* tableSelectionViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [[SlideNavigationController sharedInstance] pushViewController:tableSelectionViewController animated:YES];
            
        }
        
        
    }];
    [self downloadAllSettings];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)downloadAllSettings
{
    [GeneralWebservices webserviceMainSplashCall:nil webserviceName:Webservice_Settings OnCompletion:^(id returnDict, NSError *error) {
        if (returnDict)
        {
            [[SingletonClass sharedSingletonClass].settingsDictionary setObject:returnDict[@"rows"][0][@"min_order"] forKey:@"min_order"];
            [[SingletonClass sharedSingletonClass].settingsDictionary setObject:returnDict[@"rows"][0][@"vat"] forKey:@"vat"];
            [[SingletonClass sharedSingletonClass].settingsDictionary setObject:returnDict[@"rows"][0][@"is_credit_card_enable"] forKey:@"is_credit_card_enable"];
            [[SingletonClass sharedSingletonClass].settingsDictionary setObject:returnDict[@"rows"][0][@"is_cash_delivery_enable"] forKey:@"is_cash_delivery_enable"];
            [[SingletonClass sharedSingletonClass].settingsDictionary setObject:returnDict[@"rows"][0][@"is_sadad_enable"] forKey:@"is_sadad_enable"];
        }
    }];

    [GeneralWebservices webserviceMainSplashCall:nil webserviceName:Webservice_AllCities OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            [[DataManager sharedManager] saveCity:returnDict[@"rows"]];
        }
    }];
    
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:@"1" forKey:@"type_id"];
    
    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_GetChannels OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            [[DataManager sharedManager] saveChannel:returnDict[@"rows"]];
        }
    }];
    
    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_GetSubChannels OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            [[DataManager sharedManager] saveSubChannel:returnDict[@"rows"]];
        }
    }];
    
    NSMutableDictionary *paramDictArea=[NSMutableDictionary dictionary];
    [paramDict setObject:@"1" forKey:@"city_id"];
    
    [GeneralWebservices webserviceMainJsonResponse:paramDictArea webserviceName:Webservice_GetArea OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            [[DataManager sharedManager] saveArea:returnDict[@"rows"]];
        }
    }];
}

@end
