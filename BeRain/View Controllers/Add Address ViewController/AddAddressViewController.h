//
//  AddAddressViewController.h
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"
#import <MapKit/MapKit.h>
@import GoogleMaps;
@import GooglePlaces;

@interface AddAddressViewController : BaseViewController
{
    User *currentUser;

    IBOutlet UIScrollView *mainScrollView;
    IBOutlet UIView *innerView;
    IBOutlet UIButton *currentLocationButton;
    IBOutletCollection(UIImageView) NSArray *bgImageViews;
    IBOutlet UIButton *cityButton;
    IBOutlet UIButton *areaButton;
    IBOutlet UITextField *addressName;
    IBOutlet UITextField *buildingNumber;
    IBOutlet UITextField *floorNumber;
    IBOutlet UIButton *saveButton;
    IBOutlet UITextView *addressTextView;
    IBOutlet UIView *addressView;
    IBOutlet UIView *cityView;
    IBOutlet UIView *mapViewOnly;
    
    NSMutableArray *cityArray;
    NSInteger slectedindex;
    UIImage *radioImage;
    UIImage *radioCheckImage;
    
    NSMutableArray *areaArray;
    NSInteger slectedindexArea;

    CLLocation* currentLocationSaved;

    GMSPlace *selectedPlace;
    IBOutlet NSLayoutConstraint *mapViewHeightConstraint;
    
    UIImage*fullScreenImage;
    UIImage*halfScreenImage;
    
}


- (IBAction)currentLocationButtonTap:(UIButton *)sender;
- (IBAction)cityButtonTap:(UIButton *)sender;
- (IBAction)areaButtonTap:(UIButton *)sender;
- (IBAction)saveButtonTap:(UIButton *)sender;



/**
 Reference to UILabel holds address
 */
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

/**
 Reference to GMSMapView shows location on google map
 */
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapView;

/**
 Reference to UIView shows address
 */
@property (weak, nonatomic) IBOutlet UIView *viewAddressContainer;

/**
 Reference to set address button
 */
@property (weak, nonatomic) IBOutlet UIButton *buttonSetAddress;

/**
 Reference to NSString holds address
 */
@property (strong, nonatomic) NSString *address;

/**
 Reference to NSString holds latitude
 */
@property (strong, nonatomic) NSString *lat;

/**
 Reference to NSString holds longitude
 */
@property (strong, nonatomic) NSString *lng;

/**
 Reference to NSString holds city
 */
@property (strong, nonatomic) NSString *city;

/**
 Reference to NSString holds country
 */
@property (strong, nonatomic) NSString *country;


/**
 Used to set selected address
 
 @param sender Set Address button
 */
- (IBAction)setAddressButtonAction:(id)sender;

/**
 Used to set current address
 
 @param sender GPS button
 */
- (IBAction)locateCurrentLocation:(id)sender;
- (IBAction)fullScreenButtonTap:(UIButton *)sender;


@end
