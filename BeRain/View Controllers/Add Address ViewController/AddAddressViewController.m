//
//  AddAddressViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/5/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "AddAddressViewController.h"

static NSString *const kHNKDemoSearchResultsCellIdentifier = @"HNKDemoSearchResultsCellIdentifier";

@interface AddAddressViewController ()<GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate,LGAlertViewDelegate>

@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentUser = [SingletonClass sharedSingletonClass].settingsDictionary[@"User"];
    self.lat=[SingletonClass sharedSingletonClass].settingsDictionary[@"latitude"];
    self.lng=[SingletonClass sharedSingletonClass].settingsDictionary[@"longitude"];
    self.address=[SingletonClass sharedSingletonClass].settingsDictionary[@"currentAddress"];
    
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, 1200)];
    mainScrollView.contentInset=UIEdgeInsetsMake(4.0,0.0,1100-self.view.frame.size.height,0.0);

    
    currentLocationButton.layer.cornerRadius=currentLocationButton.frame.size.height/2;
    currentLocationButton.layer.masksToBounds = YES;
    
    saveButton.layer.cornerRadius=saveButton.frame.size.height/2;
    saveButton.layer.masksToBounds = YES;

    
    for (UIImageView*imageView in bgImageViews)
    {
        imageView.layer.backgroundColor=[[UIColor groupTableViewBackgroundColor] CGColor];
        imageView.layer.cornerRadius=5;
        imageView.layer.borderWidth=1.0;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderColor=[[UIColor purpleColor] CGColor];
    }
    fullScreenImage=[UIImage imageNamed:@"ic_full_screen"];
    halfScreenImage=[UIImage imageNamed:@"ic_exit_full_screen"];

    
    radioImage=[UIImage imageNamed:@"radio"];
    radioCheckImage=[UIImage imageNamed:@"radio-check"];
    
    cityArray=[NSMutableArray array];
    slectedindex=0;
    
    areaArray=[NSMutableArray array];
    slectedindexArea=0;
    [self getAllCities];
    
    [self setUpMapView];
    

    currentLocationSaved = [[CLLocation alloc] initWithLatitude:[[SingletonClass sharedSingletonClass].settingsDictionary[@"latitude"] floatValue] longitude:[[SingletonClass sharedSingletonClass].settingsDictionary[@"longitude"] floatValue]];
    
    if ([SettingsClass isLanguageArabic])
    {
        [addressName setTextAlignment:NSTextAlignmentRight];
        [buildingNumber setTextAlignment:NSTextAlignmentRight];
        [addressTextView setTextAlignment:NSTextAlignmentRight];
        [floorNumber setTextAlignment:NSTextAlignmentRight];
        cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        cityButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        areaButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        areaButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);

    }
    else
    {
        [addressName setTextAlignment:NSTextAlignmentLeft];
        [buildingNumber setTextAlignment:NSTextAlignmentLeft];
        [addressTextView setTextAlignment:NSTextAlignmentLeft];
        [floorNumber setTextAlignment:NSTextAlignmentLeft];
        cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        cityButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        areaButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        areaButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateAddress:(NSString*)countryName completeAddress:(NSString*)address
{
//    [addressTextView setText:address];
    if ([countryName isEqualToString:@"Saudi Arabia"] || [countryName isEqualToString:@"السعودية"])
    {
        [addressView setHidden:NO];
        [cityView setHidden:YES];
    }
    else
    {
        [addressView setHidden:YES];
        [cityView setHidden:NO];
    }
}
- (IBAction)currentLocationButtonTap:(UIButton *)sender
{
//    [self getCurrentLocation];
}

- (IBAction)cityButtonTap:(UIButton *)sender
{
    NSMutableArray *imagesArray=[NSMutableArray array];
    NSMutableArray *namesArray=[NSMutableArray array];
    
    for (int i=0; i<cityArray.count; i++)
    {
        City *city=cityArray[i];
        
        if (i==slectedindex)
        {
            [imagesArray addObject:radioCheckImage];
        }
        else
        {
            [imagesArray addObject:radioImage];
        }
        if ([SettingsClass isLanguageArabic])
        {
            [namesArray addObject:city.city_title_ar];
        }
        else
        {
            [namesArray addObject:city.city_title_en];
        }
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Select City", @"")
                                                        message:@""
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:namesArray
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    alertView.buttonsIconImages = imagesArray;
    alertView.buttonsIconImagesHighlighted = imagesArray;
    alertView.buttonsIconPosition = LGAlertViewButtonIconPositionRight;
    if ([SettingsClass isLanguageArabic])
    {
        alertView.buttonsTextAlignment = NSTextAlignmentRight;
    }
    else
    {
        alertView.buttonsTextAlignment = NSTextAlignmentLeft;
    }
    
    alertView.cancelButtonTextAlignment = NSTextAlignmentCenter;
    alertView.tag=1;
    [alertView showAnimated:YES completionHandler:nil];

}

- (IBAction)areaButtonTap:(UIButton *)sender
{
    NSMutableArray *imagesArray=[NSMutableArray array];
    NSMutableArray *namesArray=[NSMutableArray array];
    
    for (int i=0; i<areaArray.count; i++)
    {
        Area *area=areaArray[i];
        
        if (i==slectedindexArea)
        {
            [imagesArray addObject:radioCheckImage];
        }
        else
        {
            [imagesArray addObject:radioImage];
        }
        if ([SettingsClass isLanguageArabic])
        {
            [namesArray addObject:area.area_title_ar];
        }
        else
        {
            [namesArray addObject:area.area_title_en];
        }
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:NSLocalizedString(@"Select Area", @"")
                                                        message:@""
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:namesArray
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                         destructiveButtonTitle:nil
                                                       delegate:self];
    alertView.buttonsIconImages = imagesArray;
    alertView.buttonsIconImagesHighlighted = imagesArray;
    alertView.buttonsIconPosition = LGAlertViewButtonIconPositionRight;
    if ([SettingsClass isLanguageArabic])
    {
        alertView.buttonsTextAlignment = NSTextAlignmentRight;
    }
    else
    {
        alertView.buttonsTextAlignment = NSTextAlignmentLeft;
    }
    
    alertView.cancelButtonTextAlignment = NSTextAlignmentCenter;
    alertView.tag=2;
    [alertView showAnimated:YES completionHandler:nil];

}

- (IBAction)saveButtonTap:(UIButton *)sender
{
//NewAddress: ( Parameters: user_id, add_area, add_name, add_detail, add_latitude, add_longitude, add_block, add_floor ,add_street_name)
    
    NSMutableArray* invalidEntries = [[NSMutableArray alloc] init];
    
    if( addressName.text.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Address Name", @"")];
    }
    if( buildingNumber.text.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Building number", @"")];
    }
    if( floorNumber.text.length <= 0 )
    {
        [invalidEntries addObject:NSLocalizedString(@"Floor number", @"")];
    }

    if ([cityButton.titleLabel.text isEqualToString:NSLocalizedString(@"Select City", @"")])
    {
        [invalidEntries addObject:NSLocalizedString(@"Select Area", @"")];
    }
    if ([areaButton.titleLabel.text isEqualToString:NSLocalizedString(@"Select Area", @"")])
    {
        [invalidEntries addObject:NSLocalizedString(@"Select Area", @"")];
    }

    if( invalidEntries.count > 0 )
    {
        // Atleast one has invalid data
        UIAlertView* alert = [[UIAlertView alloc] init];
        [alert setTitle:NSLocalizedString(@"Insufficient input", @"")];
        NSMutableString* alertMsg = [[NSMutableString alloc] initWithString:NSLocalizedString(@"Please enter valid input for - ", @"")];
        {
            for( int i = 0; i < invalidEntries.count; ++i )
            {
                if( i == invalidEntries.count - 1 )
                {
                    [alertMsg appendFormat:@"%@.", invalidEntries[i]];
                }
                else
                {
                    [alertMsg appendFormat:@"%@, ", invalidEntries[i]];
                }
            }
        }
        [alert setMessage:alertMsg];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
        [alert show];
    }
    else
    {

    City *city=cityArray[slectedindex];
    Area *area=areaArray[slectedindexArea];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:currentUser.user_id forKey:@"user_id"];
    [paramDict setObject:[NSString stringWithFormat:@"%f",currentLocationSaved.coordinate.latitude] forKey:@"add_latitude"];
    [paramDict setObject:[NSString stringWithFormat:@"%f",currentLocationSaved.coordinate.longitude] forKey:@"add_longitude"];
    [paramDict setObject:area.area_id forKey:@"add_area"];
    [paramDict setObject:addressName.text forKey:@"add_name"];
    [paramDict setObject:addressTextView.text forKey:@"add_detail"];
    [paramDict setObject:buildingNumber.text forKey:@"add_block"];
    [paramDict setObject:floorNumber.text forKey:@"add_floor"];
    [paramDict setObject:area.area_id forKey:@"add_street_name"];

    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_SaveArea OnCompletion:^(id returnDict, NSError *error) {
        
        if (!error)
        {
            [[DataManager sharedManager] saveAddress:returnDict[@"rows"]];
            [[DataManager sharedManager] makeDefaultAddress:currentUser.user_id];
            
            [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];

//            areaArray =[NSMutableArray arrayWithArray:returnDict[@"rows"]];
//            [areaButton setTitle:areaArray[slectedindexArea][@"area_title_en"] forState:UIControlStateNormal];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] init];
            [alert setTitle:NSLocalizedString(@"Berain", @"")];
            [alert setMessage:NSLocalizedString(@"Please try again", @"")];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"")];
            [alert show];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
    }
}
-(void)selectCity:(NSString*)cityName
{
    if (cityArray.count>0)
    {
        BOOL selected=NO;
        
        for (int i=0; i<cityArray.count; i++)
        {
            City *city=cityArray[i];
            NSString *cityNameSaved=city.city_title_en;
            
            if ([SettingsClass isLanguageArabic])
            {
                cityNameSaved=city.city_title_ar;
            }
            if ([cityNameSaved isEqualToString:cityName])
            {
                slectedindex=i;
                if ([SettingsClass isLanguageArabic])
                {
                    [cityButton setTitle:city.city_title_ar forState:UIControlStateNormal];
                }
                else
                {
                    [cityButton setTitle:city.city_title_en forState:UIControlStateNormal];
                }
                [self getArea:city.city_id];
                selected=YES;
                break;
            }
        }
        if (!selected)
        {
            [cityButton setTitle:NSLocalizedString(@"Select City", @"") forState:UIControlStateNormal];
        }
        
    }
    
}
-(void)getAllCities
{
    cityArray =[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayCity]];
    [cityButton setTitle:NSLocalizedString(@"Select City", @"") forState:UIControlStateNormal];
    
//    if (cityArray.count>0)
//    {
//        City *city=cityArray[slectedindex];
//        
//        if ([SettingsClass isLanguageArabic])
//        {
//            [cityButton setTitle:city.city_title_ar forState:UIControlStateNormal];
//        }
//        else
//        {
//            [cityButton setTitle:city.city_title_en forState:UIControlStateNormal];
//        }
//        [self getArea:city.city_id];
//    }
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
//    [GeneralWebservices webserviceMainSplashCall:nil webserviceName:Webservice_AllCities OnCompletion:^(id returnDict, NSError *error) {
//        
//        if (!error)
//        {
//            cityArray =[NSMutableArray arrayWithArray:returnDict[@"rows"]];
//            if ([SettingsClass isLanguageArabic])
//            {
//                [cityButton setTitle:cityArray[slectedindex][@"city_title_ar"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [cityButton setTitle:cityArray[slectedindex][@"city_title_en"] forState:UIControlStateNormal];
//            }
//
//            [self getArea:cityArray[slectedindex][@"city_id"]];
//        }
//        else
//        {
//            UIAlertView* alert = [[UIAlertView alloc] init];
//            [alert setTitle:@"Berain"];
//            [alert setMessage:@"Please check server"];
//            [alert addButtonWithTitle:@"OK"];
//            [alert show];
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        }
//        
//    }];
}

-(void)getArea:(NSString*)city_id
{
    areaArray=[NSMutableArray array];
    slectedindexArea=0;
    areaArray =[NSMutableArray arrayWithArray:[[DataManager sharedManager] arrayarea:city_id]];
    [areaButton setTitle:NSLocalizedString(@"Select Area", @"") forState:UIControlStateNormal];

//    if (areaArray.count>0)
//    {
//        Area *area=areaArray[slectedindexArea];
//        
//        if ([SettingsClass isLanguageArabic])
//        {
//            [areaButton setTitle:area.area_title_ar forState:UIControlStateNormal];
//        }
//        else
//        {
//            [areaButton setTitle:area.area_title_en forState:UIControlStateNormal];
//        }
//    }
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
//    [paramDict setObject:city_id forKey:@"city_id"];
//
//    [GeneralWebservices webserviceMainJsonResponse:paramDict webserviceName:Webservice_GetArea OnCompletion:^(id returnDict, NSError *error) {
//        
//        if (!error)
//        {
//            areaArray =[NSMutableArray arrayWithArray:returnDict[@"rows"]];
//            if ([SettingsClass isLanguageArabic])
//            {
//                [areaButton setTitle:areaArray[slectedindexArea][@"area_title_ar"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [areaButton setTitle:areaArray[slectedindexArea][@"area_title_en"] forState:UIControlStateNormal];
//            }
//
//        }
//        else
//        {
//            UIAlertView* alert = [[UIAlertView alloc] init];
//            [alert setTitle:@"Berain"];
//            [alert setMessage:@"Please check server"];
//            [alert addButtonWithTitle:@"OK"];
//            [alert show];
//        }
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        
//    }];
 
}

-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    if (alertView.tag==1)
    {
        slectedindex=index;
        City *city=cityArray[slectedindex];

        if ([SettingsClass isLanguageArabic])
        {
            [cityButton setTitle:city.city_title_ar forState:UIControlStateNormal];
        }
        else
        {
            [cityButton setTitle:city.city_title_en forState:UIControlStateNormal];
        }

        [self getArea:city.city_id];
    }
    else if (alertView.tag==2)
    {
        slectedindexArea=index;
        Area *area=areaArray[slectedindexArea];
        
        if ([SettingsClass isLanguageArabic])
        {
            [areaButton setTitle:area.area_title_ar forState:UIControlStateNormal];
        }
        else
        {
            [areaButton setTitle:area.area_title_en forState:UIControlStateNormal];
        }

    }
}

-(void)setUpMapView
{
    self.city = @"";
    self.country = @"";
    
    self.addressLabel.text = self.address;
    [addressTextView setText:self.addressLabel.text];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue]  longitude:[self.lng doubleValue] zoom:16];
    [_googleMapView setCamera:camera];
    _googleMapView.myLocationEnabled = YES;
    _googleMapView.delegate = self;
    //_googleMapView.padding = UIEdgeInsetsMake(0.0, 0.0, 0.0, 10.0);
    [self.viewAddressContainer.layer setCornerRadius:25.f];
    self.viewAddressContainer.layer.borderWidth = 1.f;
    self.viewAddressContainer.layer.borderColor = [UIColor colorWithRed:28.f/255.f green:181.f/255.f blue:234.f/255.f alpha:1.f].CGColor;
    [self.viewAddressContainer.layer masksToBounds];
}

- (IBAction)searchButtonTap:(UIButton *)sender
{
    selectedPlace = nil;
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}
- (void) mapView: (GMSMapView *)mapView didChangeCameraPosition: (GMSCameraPosition *)position {
    
    if (selectedPlace != nil) {
        selectedPlace = nil;
        return;
    }
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    [self setAddressForMap:latitude longitude:longitude];
    currentLocationSaved = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

    }

-(void)setAddressForMap:(double)latitude longitude:(double)longitude
{
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(latitude,longitude);
    GMSGeocoder* coder = [[GMSGeocoder alloc] init];
    [coder reverseGeocodeCoordinate:addressCoordinates completionHandler:^(GMSReverseGeocodeResponse *results, NSError *error) {
        if (error) {
            // NSLog(@"Error %@", error.description);
            self.country = @"";
            self.city = @"";
        } else {
            GMSAddress* address = [results firstResult];
            self.city = address.locality ? address.locality : @"";
            self.country = address.country ? address.country : @"";
            [self updateAddress:self.country completeAddress:@""];
            NSArray *arr = [address valueForKey:@"lines"];
            

            NSString *str1 = [NSString stringWithFormat:@"%lu",(unsigned long)[arr count]];
            if ([str1 isEqualToString:@"0"]) {
                self.addressLabel.text = @"";
            }
            else if ([str1 isEqualToString:@"1"]) {
                NSString *str2 = [arr objectAtIndex:0];
                self.addressLabel.text = str2;
            }
            else if ([str1 isEqualToString:@"2"]) {
                NSString *str2 = [arr objectAtIndex:0];
                NSString *str3 = [arr objectAtIndex:1];
                if (str2.length > 1 ) {
                    self.addressLabel.text = [NSString stringWithFormat:@"%@,%@",str2,str3];
                }
                else {
                    
                    self.addressLabel.text = [NSString stringWithFormat:@"%@",str3];
                }
            }
        }
        [addressTextView setText:self.addressLabel.text];
        
    }];

}
- (IBAction)setAddressButtonAction:(id)sender {
    if (self.addressLabel.text.length == 0) {
        return;
    }
    self.address = self.addressLabel.text;
    self.lat = [NSString stringWithFormat:@"%f", self.googleMapView.camera.target.latitude];
    self.lng = [NSString stringWithFormat:@"%f", self.googleMapView.camera.target.longitude];
    [self performSegueWithIdentifier:@"unwindFromLocationPickerView" sender:nil];
}

- (IBAction)locateCurrentLocation:(id)sender {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue]  longitude:[self.lng doubleValue] zoom:16];
    [_googleMapView setCamera:camera];
    [self setAddressForMap:[self.lat doubleValue] longitude:[self.lng doubleValue]];

}

- (IBAction)fullScreenButtonTap:(UIButton *)sender
{
    if ([sender.imageView.image isEqual:fullScreenImage])
    {
        [sender setImage:halfScreenImage forState:UIControlStateNormal];
//        [sender setTitle:@"Half" forState:UIControlStateNormal];
//        [mapViewOnly setFrame:CGRectMake(mapViewOnly.frame.origin.x, mapViewOnly.frame.origin.y, mapViewOnly.frame.size.width, self.view.frame.size.height-45)];
        [mainScrollView setScrollEnabled:NO];
        mapViewHeightConstraint.constant=self.view.frame.size.height-45;

    }
    else
    {
        [sender setImage:fullScreenImage forState:UIControlStateNormal];

//        [sender setTitle:@"Full" forState:UIControlStateNormal];
//        [mapViewOnly setFrame:CGRectMake(mapViewOnly.frame.origin.x, mapViewOnly.frame.origin.y, mapViewOnly.frame.size.width, 357)];
        [mainScrollView setScrollEnabled:YES];
        mapViewHeightConstraint.constant=357;
        
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    self.country = @"";
    self.city = @"";
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    selectedPlace = place;
    double latitude = place.coordinate.latitude;
    double longitude = place.coordinate.longitude;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude  longitude:longitude zoom:16];
    [_googleMapView setCamera:camera];
    self.addressLabel.text = place.formattedAddress;
    [addressTextView setText:self.addressLabel.text];

    currentLocationSaved = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

    for (GMSAddressComponent *compCity in place.addressComponents) {
        if ([compCity.type isEqualToString:@"locality"]) {
            self.city = compCity.name ? compCity.name : @"";
        }
        else if ([compCity.type isEqualToString:@"country"]) {
            self.country = compCity.name ? compCity.name : @"";
        }
        [self updateAddress:self.country completeAddress:@""];
        [self selectCity:self.city];
        
    }
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"[\'^£$%&*()}{#~?<>>,|=+-'\"]"];
    for (int i = 0; i < [string length]; i++)
    {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c])
        {
            return NO;
        }
    }
    return YES;
}

@end
